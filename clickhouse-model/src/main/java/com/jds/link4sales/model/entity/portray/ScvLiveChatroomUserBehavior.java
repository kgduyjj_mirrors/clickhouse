package com.jds.link4sales.model.entity.portray;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 报告会、直播、微课用户行为数据
 * </p>
 *
 * @author JACKSON G
 * @since 2020-11-06
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ScvLiveChatroomUserBehavior extends Model<ScvLiveChatroomUserBehavior> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private Integer roomId;
    /**
     * 来源id
     */
    private Integer sourceId;
    /**
     * jds用户id
     */
    private String jdsUserId;
    /**
     * 类型，1-进入时间。2-发言
     */
    private Integer type;
    /**
     * 进入时间
     */
    private Date enterTime;
    /**
     * 消息类型 1文本、2图片、3音频、4视频
     */
    private Integer msgType;
    /**
     * 消息内容
     */
    private String messgeContent;
    /**
     * 发送消息时间
     */
    private Date messgeTime;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
