package com.jds.link4sales.model.dto;

import com.jds.link4sales.model.entity.CommonEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author: zhixiang.peng
 * @date: 2021/2/2
 * @description: 已配置的链接dto
 */
@EqualsAndHashCode(callSuper = false)
@Data
public class SourceLinkConfigDto extends CommonEntity {

    /**
     * 工号
     */
    private String  account;

    /**
     * 资源类型
     */
    private Integer  sourceType;

    /**
     * 资源名称
     */
    private String  roomName;

    /**
     * 资源名称
     */
    private Integer  sourceLinkId;

}
