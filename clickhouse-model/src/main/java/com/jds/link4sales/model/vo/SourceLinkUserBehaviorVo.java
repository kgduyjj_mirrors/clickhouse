package com.jds.link4sales.model.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * @author: zhixiang.peng
 * @date: 2021/2/8
 * @description: 行为vo
 */
@Data
public class SourceLinkUserBehaviorVo {


    /**
     * 工号
     */
    private String account;

    private String sourceLinkId;

    /**
     * source_exclusive_link主键id
     */
    private Integer exclusiveLinkId;
    /**
     * 用户id
     */
    private String userId;

    /**
     * 用户昵称
     */
    private String nickname;

    /**
     * 真实姓名
     */
    private String realname;
    /**
     * 头像
     */
    private String headimgurl;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date accessTime;

    /**
     * 访问次数
     */
    private   Integer accessCount;

    /**
     * 用户标签
     */
    private   String tags;


}
