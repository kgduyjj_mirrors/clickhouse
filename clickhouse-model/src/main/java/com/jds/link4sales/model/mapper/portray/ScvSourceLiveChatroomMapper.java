package com.jds.link4sales.model.mapper.portray;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jds.link4sales.model.entity.portray.ScvSourceLiveChatroom;

/**
 * <p>
 * 直播室 Mapper 接口
 * </p>
 *
 * @author JACKSON G
 * @since 2020-11-27
 */
@DS("slave")
public interface ScvSourceLiveChatroomMapper extends BaseMapper<ScvSourceLiveChatroom> {

}
