package com.jds.link4sales.model.entity.portray;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 * wechat_user_portrait
 * </p>
 *
 * @author wzy
 * @since 2020-11-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class WechatUserPortrait extends Model<WechatUserPortrait> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * followId
     */
    private String followId;
    /**
     * externalId
     */
    private String externalId;
    /**
     * 企业id
     */
    private Integer qyId;
    /**
     * 手机号
     */
    private String phoneNumber;
    /**
     * 家庭结构
     */
    private String familyStructure;
    /**
     * 资产结构
     */
    private String assetStructure;
    /**
     * 年龄
     */
    private Integer age;
    /**
     * 家庭住址
     */
    private String location;
    /**
     * 微信号
     */
    private String wxNumber;
    /**
     * 生日
     */
    private String birth;
    /**
     * 交易编码
     */
    private String transactionNumber;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 更新时间
     */
    private Date updateTime;
    /**
     * aes  微信对称加密
     */
    private String hashWxNumber;
    /**
     * hash 手机号
     */
    private String hashPhoneNumber;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
