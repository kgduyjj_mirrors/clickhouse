package com.jds.link4sales.model.dto;

import com.jds.link4sales.model.entity.sys.SysRole;
import lombok.Data;

import java.util.List;

/**   新增更新 批量新增dto
 * @author: zhixiang.peng
 * @date: 2020/11/9
 * @description: 
 */
@Data
public class UserRoleDto {
    
    /**
     * 员工工号
     */
    private  String  userCode;
    
    /**
     * 角色ids
     */
    private List<Integer> roleIds;
    
}
