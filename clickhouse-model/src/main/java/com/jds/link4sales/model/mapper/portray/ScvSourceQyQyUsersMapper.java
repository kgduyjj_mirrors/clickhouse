package com.jds.link4sales.model.mapper.portray;


import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jds.link4sales.model.entity.portray.ScvSourceQyQyUsers;

/**
 * <p>
 * 企业微信用户 Mapper 接口
 * </p>
 *
 * @author JACKSON G
 * @since 2020-11-26
 */
@DS("slave")
public interface ScvSourceQyQyUsersMapper extends BaseMapper<ScvSourceQyQyUsers> {

}
