package com.jds.link4sales.model.dto;

import com.jds.link4sales.model.entity.CommonEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 微信用户属性查询vo
 *
 * @author zhixiang.peng
 * @date 2020/12/08
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WxUserPropertyQueryDto extends CommonEntity {

    /**
     * 员工工号
     */
    private String userCode;

    /**
     * 客户微信号
     */
    private String customerWxid;


    /**
     * 所属微信号
     */
    private String belongWxid;


    /**
     * 客户状态 //1:在通讯录 2:在群 3:通讯录和群都在
     */
    private String relationStatus;


    /**
     * 微信标签
     */
    private String  tag;


    /**
     * 开始时间
     */
    private String startTime;

    /**
     * 结束时间
     */
    private String stopTime;

    /**
     * 老师ip
     */
    private String  teacherIp;

    /**
     * count统计数读取缓存的标记
     */
    private Integer  readCachCountFlag;


}
