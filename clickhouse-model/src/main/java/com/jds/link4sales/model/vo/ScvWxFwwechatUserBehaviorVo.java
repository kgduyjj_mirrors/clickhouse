package com.jds.link4sales.model.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * scv fwwechat mapper vo服务号用户行为
 *
 * @author kun.gao
 * @date 2020/11/06
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ScvWxFwwechatUserBehaviorVo {

    /**
     * oneId
     */
    private String oneId;

    /**
     * 事件时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date operateTime;
    /**
     * 头像
     */
    private String avatar;
    /**
     * 昵称
     */
    private String nickName;

    /**
     * 员工工号
     */
    private String employeeId;


    /**
     * 员工姓名
     */
    private String employeeName;

    /**
     * 客户微信号
     */
    private String customerWxid;

    /**
     * 公司微信号
     */
    private String companyWxid;

    /**
     * 所属微信号
     */
    private String belongWxid;


    /**
     * 客户状态  ,//1:在通讯录 2:在群 3:通讯录和群都在
     */
    private String relationStatus;

    /**
     * 添加时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date addTime;

    /**
     * 客户备注
     */
    private String  conRemark;

    /**
     * 服务号id
     */
    private Integer fwid;
    /**
     * 服务号名称
     */
    private String fwName;
    /**
     * 行为类型，1-普通发言， 2-点击菜单，3-关注，4-取消关注，5-阅读文章
     */
    private Integer type;

    /**
     * 菜单名称或者文章标题
     */
    private String operateObjName;
    /**
     * 用户行为内容
     */
    private String content;

}
