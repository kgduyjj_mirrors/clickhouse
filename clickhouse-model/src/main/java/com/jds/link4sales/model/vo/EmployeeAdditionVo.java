package com.jds.link4sales.model.vo;

import lombok.Data;

/**
 * 员工除了签证官
 *
 * @author kun.gao
 * @date 2020/12/10
 */
@Data
public class EmployeeAdditionVo {

    private Long id;
    private String companyWxid;
    private String employeeId;
    private String customerWxid;
    private String customerName;
    private String customerPhone;
    private String customerAddress;
    private String familyStruct;
    private String customerAsset;
    private String userId;
    private Integer paramType;
}
