package com.jds.link4sales.model.mapper.portray;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jds.link4sales.model.entity.portray.QySecretMapping;

/**
 * qy秘密映射映射器
 *
 * @author kun.gao
 * @date 2020/11/05
 */
public interface QySecretMappingMapper extends BaseMapper<QySecretMapping> {

}
