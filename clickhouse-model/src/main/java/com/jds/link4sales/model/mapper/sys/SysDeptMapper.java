package com.jds.link4sales.model.mapper.sys;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jds.link4sales.model.vo.SysDeptTreeVo;
import com.jds.link4sales.model.entity.sys.SysDept;

/**
 * <p>
 * sys_dept  Mapper 接口
 * </p>
 *
 * @author wzy
 * @since 2020-11-03
 */
public interface SysDeptMapper extends BaseMapper<SysDept> {

    List<SysDeptTreeVo> selectTreeInfo();

}
