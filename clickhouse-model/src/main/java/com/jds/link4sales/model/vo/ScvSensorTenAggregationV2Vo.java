package com.jds.link4sales.model.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * scv fwwechat mapper vo服务号用户行为
 *
 * @author kun.gao
 * @date 2020/11/06
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ScvSensorTenAggregationV2Vo {
    
    /**
     * 企业id
     */
    private String qyId;
    /**
     * 员工id
     */
    private String followUserId;
    /**
     * externalId
     */
    private Integer exId;
    /**
     * oneId
     */
    private String oneId;
    /**
     * 员工工号
     */
    private String userCode;

    /**
     * 头像
     */
    private String avatar;
    /**
     * 备注
     */
    private String mark;
    /**
     * 昵称
     */
    private String nickName;

    /**
     * 员工
     * @param 
     * @return 
     */
    private String followUserName;

    private String mobile;
    
    /**
     * 用户行为时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date eventTime;

    /**
     * 停留时间 (单位是秒)
     */
    private Integer stayTime;

    /**
     * 进入房间时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date intoRoomTime;

    /**
     * 用户行为类型
     */
    private Integer eventType;

    /**
     *   用户发言类容
     */
     private String content;

    /**
     *   删除状态
     */
     private Integer deleteFlag;

     /**
      * 所属员工姓名
      */
     private  String  name;

    /**
     *   场次标题
     */
    private String title;

    /**
     *   场次标题
     */
    private String titleId;

}
