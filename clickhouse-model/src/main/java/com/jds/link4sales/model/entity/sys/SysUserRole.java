package com.jds.link4sales.model.entity.sys;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *   用户与角色关系
 * </p>
 *
 * @author zhixiang.peng
 * @since 2020-11-03
 */
@Data
@EqualsAndHashCode(callSuper = false)

public class SysUserRole extends Model<SysUserRole> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 用户工号，业务唯一标识
     */
    @NotBlank(message = "用户code不能为空")
    private String userCode;
    /**
     * 角色id
     */
    @NotNull(message = "角色id不能为空")
    private Integer roleId;
    /**
     * 是否主角色 0:否 1:是
     */
    private Integer isMaster;
    /**
     * 创建人
     */
    private String createdBy;
    /**
     * 创建时间
     */
    private Date createdTime;
    /**
     * 更新人
     */
    private String updatedBy;
    /**
     * 更新时间
     */
    private Date updatedTime;
    /**
     * 角色名称
     */
    @TableField(exist = false)
    private  String  roleName;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "SysUserRole{" +
        ", id=" + id +
        ", userCode=" + userCode +
        ", roleId=" + roleId +
        ", isMaster=" + isMaster +
        ", createdBy=" + createdBy +
        ", createdTime=" + createdTime +
        ", updatedBy=" + updatedBy +
        ", updatedTime=" + updatedTime +
        "}";
    }
}
