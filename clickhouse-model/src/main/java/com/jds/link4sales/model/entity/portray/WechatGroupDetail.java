package com.jds.link4sales.model.entity.portray;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 微信群信息详情表
 * </p>
 *
 * @author JACKSON G
 * @since 2020-12-01
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class WechatGroupDetail extends Model<WechatGroupDetail> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "group_id", type = IdType.AUTO)
    private Long groupId;
    /**
     * 群主工号
     */
    private String roomOwnerId;
    /**
     * 群主姓名
     */
    private String roomOwnerName;
    /**
     * 群主微信
     */
    private String roomOwner;
    private String roomName;
    private Integer roomId;
    /**
     * 群头像地址
     */
    private String roomHead;
    /**
     * 群成员
     */
    private String roomMember;
    /**
     * 建群时间
     */
    private Date groupTime;
    /**
     * 进群时间
     */
    private Date enterTime;
    /**
     * 退群时间
     */
    private Date outTime;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 更新时间
     */
    private Date updateTime;
    private Integer oneId;


    @Override
    protected Serializable pkVal() {
        return this.groupId;
    }

}
