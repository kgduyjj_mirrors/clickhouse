package com.jds.link4sales.model.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 微信用户属性VO（初始化列表展示用）
 *
 * @author zhixiang.peng
 * @date 2020/12/08
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ScvWxUserPropertyVo {
    
    /**
     * oneId
     */
    private String oneId;
    /**
     * 头像
     */
    private String avatar;
    /**
     * 昵称
     */
    private String nickName;

    /**
     * 员工工号
     */
    private String employeeId;


    /**
     * 员工姓名
     */
    private String employeeName;

    /**
     * 客户微信号
     */
    private String customerWxid;

    /**
     * 公司微信号
     */
    private String companyWxid;

    /**
     * 所属微信号
     */
    private String belongWxid;


    /**
     * 客户状态  ,//1:在通讯录 2:在群 3:通讯录和群都在
     */
    private String relationStatus;

    /**
     * 添加时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date addTime;

    /**
     * 客户备注
     */
    private String  conRemark;

    /**
     * 风险标识
     */
    private Integer  riskStatus;

    /**
     * 激活状态
     */
    private Integer  activeStatus;

    /**
     * 同行状态
     */
    private Integer  peerStatus;


    /**
     * 微信标签
     */
    private String  labels;


    /**
     * 客户来源
     */
    private String  addWay;

    /**
     *  老师ip
     */
    private String  teacherIp;

}
