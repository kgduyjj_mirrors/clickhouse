package com.jds.link4sales.model.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jds.link4sales.model.entity.sys.SysUserRole;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * sys_user_role  Mapper 接口
 * </p>
 *
 * @author zhixiang.peng
 * @since 2020-11-03
 */
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {


    /**
     *   判断用户是否有当前url的访问权限
     * @param userId 用户id
     * @param url url
     * @return
     */
    public   Integer selectAuthCountByUserId(@Param("userId")String userId , @Param("url")String url);

    /**
     * 根据用户cod获取用户角色一览
     * @param 
     * @return 
     */
    List<SysUserRole>  selectRoleInfoByUserCode(String  userCode);
    
}
