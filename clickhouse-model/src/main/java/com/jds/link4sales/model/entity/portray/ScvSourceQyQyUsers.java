package com.jds.link4sales.model.entity.portray;

import java.util.Date;

import java.io.Serializable;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 企业微信用户
 * </p>
 *
 * @author JACKSON G
 * @since 2020-11-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ScvSourceQyQyUsers extends Model<ScvSourceQyQyUsers> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 企业微信账号ID
     */
    private Integer qyId;
    /**
     * 账号
     */
    private String userid;
    /**
     * 名称
     */
    private String name;
    /**
     * 手机号
     */
    private String mobile;
    /**
     * 邮箱
     */
    private String email;
    /**
     * 性别：1表示男性，2表示女性
     */
    private Integer gender;
    /**
     * 头像
     */
    private String avatar;
    /**
     * 企业微信的部门id用逗号分隔
     */
    private String departments;
    /**
     * 状态：0未导入，1已激活，2已禁用，4未激活
     */
    private Integer status;
    /**
     * 是否关注
     */
    private Integer isSubscribe;
    /**
     * 同步状态：0失败，1成功
     */
    private Integer syncStatus;
    /**
     * 同步异常信息
     */
    private String syncError;
    private Date createdAt;
    private Date updatedAt;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
