package com.jds.link4sales.model.entity.sys;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.extension.activerecord.Model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * <p>
 * 事业部表
 * </p>
 *
 * @author wzy
 * @since 2020-11-03
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SysDivision extends Model<SysDivision> {

    private static final long serialVersionUID = 1L;

    private Integer divisionId;
    /**
     * 用户来源
     */
    private String source;
    /**
     * 来源描述
     */
    private String description;
    private Integer status;
    /**
     * 备注信息
     */
    private String remark;
    private Date createTime;
    private Date updateTime;

    @Override
    protected Serializable pkVal() {
        return this.divisionId;
    }

}
