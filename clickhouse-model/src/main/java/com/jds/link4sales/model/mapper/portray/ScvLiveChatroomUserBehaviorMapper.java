package com.jds.link4sales.model.mapper.portray;


import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jds.link4sales.model.dto.ActionsDto;
import com.jds.link4sales.model.entity.portray.ScvLiveChatroomUserBehavior;
import com.jds.link4sales.model.vo.ScvUserLiveRoomBehaviorVo;

import java.util.List;

/**
 * <p>
 * 报告会、直播、微课用户行为数据 Mapper 接口
 * </p>
 *
 * @author JACKSON G
 * @since 2020-11-06
 */
@DS("slave")
public interface ScvLiveChatroomUserBehaviorMapper extends BaseMapper<ScvLiveChatroomUserBehavior> {

    /**
     * 查询其他动作列表
     *
     * @param actionsDto 行动dto
     * @return {@link List<ScvUserLiveRoomBehaviorVo>}
     */
    List<ScvUserLiveRoomBehaviorVo> queryOtherActionList(ActionsDto actionsDto);

    /**
     * 查询其他动作列表
     *
     * @param actionsDto 行动dto
     * @return {@link List<ScvUserLiveRoomBehaviorVo>}
     */
    List<ScvUserLiveRoomBehaviorVo> queryUserOtherActionList(ActionsDto actionsDto);

    /**
     * 查询其他动作列表
     *
     * @param actionsDto 行动dto
     * @return {@link List<ScvUserLiveRoomBehaviorVo>}
     */
    List<ScvUserLiveRoomBehaviorVo> queryUserOtherActionList1(ActionsDto actionsDto);
    
    List<ScvUserLiveRoomBehaviorVo> queryWxUserOtherActionList1(ActionsDto actionsDto);

    List<String> roomList(Integer type);


}
