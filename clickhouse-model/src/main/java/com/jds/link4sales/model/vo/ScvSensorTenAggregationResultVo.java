package com.jds.link4sales.model.vo;

import com.jds.link4sales.model.entity.portray.ScvSensorTenAggregationV2;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 财商直播单个房间单个场次返回的结果vo
 *
 * @author zhixiang.peng
 * @date 2020/11/06
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ScvSensorTenAggregationResultVo {

    /**
     * 场次
     */
    private String title;

    /**
     * 场次id
     */
    private String titleId;
    
    /**
     * 唯一人id
     */
    private String oneId;
    
    /**
     * 房间id
     */
    private String roomId;
    
    /**
     * 房间场次事件明细
     */
    private List<ScvSensorTenAggregationV2> detail;


}
