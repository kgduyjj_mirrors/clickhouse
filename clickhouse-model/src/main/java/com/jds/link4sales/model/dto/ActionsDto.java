package com.jds.link4sales.model.dto;

import com.jds.link4sales.model.entity.CommonEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;
import java.util.List;

/**
 * 用户行为dto
 *
 * @author JACKSON G
 * @date 2020/11/06
 */
@EqualsAndHashCode(callSuper = false)
@Data
public class ActionsDto extends CommonEntity{

    private Integer id;
    /**
     * 员工ID
     */
    private String followId;
    /**
     * 员工工号
     */
    private String userCode;
    /**
     * 外部联系人ID
     */
    private String externalId;
    /**
     * 企业id
     */
    private Integer qyId;
    /**
     * 名字
     */
    private String name;
    /**
     * 1 进一天  7近七天  30 近三十天
     */
    private Integer dayType;

    /**
     * 0 服务号  1直播   2微课  3报告会  4 开户入金
     */
    private Integer queryType;

    /**
     * tag
     */
    private Integer tag;

    private Integer theme;

    private Date startTime;

    private Date stopTime;

    private Long orderCompleted;

    /**
     * 支付开始时间
     */
    private Long orderCompletedStartTime;

    /**
     * 支付结束时间
     */
    private Long orderCompletedStopTime;

    private Integer answerFlag;

    private String oneId;
    
    /**
     * 关联外部联系人时会用到
     */
    private List<String> oneIdList;

    private Integer exId;
    
    private String unionId;

    /**
     * 客户微信号
     */
    private String customerWxId;

    private String roomId;

    private String title;

    private String titleId;

    private String mobile;

    private String employeeId;

    
}
