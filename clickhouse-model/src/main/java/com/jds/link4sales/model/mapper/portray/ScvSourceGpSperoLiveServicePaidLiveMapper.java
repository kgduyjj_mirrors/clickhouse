package com.jds.link4sales.model.mapper.portray;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jds.link4sales.model.entity.portray.ScvSourceGpSperoLiveServicePaidLive;

/**
 * <p>
 * 付费直播场次表 Mapper 接口
 * </p>
 *
 * @author zhixiang.peng
 * @since 2021-01-22
 */
@DS("slave")
public interface ScvSourceGpSperoLiveServicePaidLiveMapper extends BaseMapper<ScvSourceGpSperoLiveServicePaidLive> {

}
