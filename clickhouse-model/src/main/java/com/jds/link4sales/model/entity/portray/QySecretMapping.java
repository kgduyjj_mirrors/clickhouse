package com.jds.link4sales.model.entity.portray;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * qy秘密映射
 *
 * @author ＪＡＣＫＳＯＮ G
 * @date 2020/11/05
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class QySecretMapping implements Serializable {

    private Integer id;

    private Integer qyId;

    private String secret;

    private String clientId;

    private String agentId;

    private String qyName;

    private Date updateTime;

    private Date createTime;

}
