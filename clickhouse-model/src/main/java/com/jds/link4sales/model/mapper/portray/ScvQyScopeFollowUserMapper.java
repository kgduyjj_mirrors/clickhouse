package com.jds.link4sales.model.mapper.portray;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jds.link4sales.model.entity.portray.ScvQyScopeFollowUser;

/**
 * <p>
 * 配置了客户联系功能的企业员工表 Mapper 接口
 * </p>
 *
 * @author wzy
 * @since 2020-11-04
 */
@DS("slave")
public interface ScvQyScopeFollowUserMapper extends BaseMapper<ScvQyScopeFollowUser> {

}
