package com.jds.link4sales.model.vo;

import lombok.Data;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * scv tdcs vo用户行为
 *
 * @author JACKSON G
 * @date 2020/11/09
 */
@Data
public class ScvTdcsUserBehaviorVo {
    
    /**
     * 企业id
     */
    private String qyId;
    /**
     * 员工id
     */
    private String followUserId;

    private String followUserName;

    private String mobile;
    /**
     * exId
     */
    private Integer exId;
    /**
     * externalId
     */
    private String externalId;
    /**
     * oneID
     */
    private String oneId;
    /**
     * 注册时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date registryTime;
    /**
     * 开户银行（1-浦发，2-工行）
     */
    private Integer accountBankType;
    /**
     * 行为类型（101-点击开户链接，102-完成短信验证，103-完成基本信息，104-完成黄金签约，105-首次入金时间，106-首次发起交易时间；201-首次申请工行开户，202-非工行卡风险测评，203-工行卡开户风险测评，204-工行卡一类户开户开黄金户，205-非工行卡开户开黄金户，206-黄金交易编码开户成功，207-绑定cas交易编码，208-工行电子账户开户申请，209工行影像上传）
     */
    private Integer type;
    /**
     * 操作时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date operateTime;
    /**
     * 头像
     */
    private String avatar;
    /**
     * 备注
     */
    private String mark;
    /**
     * 昵称
     */
    private String nickName;
    /**
     * 流失状态（1：正常，2：删除）
     */
    private int deleteFlag;
    
    /**
     * 员工工号
     */
    private String userCode;

    /**
     * 员工姓名
     */
    private String name;
    
}
