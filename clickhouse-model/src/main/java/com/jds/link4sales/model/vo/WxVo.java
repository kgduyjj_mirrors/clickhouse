package com.jds.link4sales.model.vo;

import lombok.Data;

import java.util.Date;

/**
 * @author kun.gao
 */
@Data
public class WxVo {
    private Long userId;
    private String customerWxid;
    private String alias;
    private String wechatWxacc;
    private String nickname;
    private String pyInitial;
    private String quanPin;
    private String wechatHeadUrl;
    private String domainList;
    private String showHeadUrl;
    private Long type;
    private Integer isChatRoom;
    private Integer riskStatus;
    private Integer activeStatus;
    private String labels;
    private Integer genderFlag;
    private String conRemark;
    private Integer isRelation;
    private Date addTime;
}
