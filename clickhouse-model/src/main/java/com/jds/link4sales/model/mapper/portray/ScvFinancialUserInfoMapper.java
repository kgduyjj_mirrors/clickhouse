package com.jds.link4sales.model.mapper.portray;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jds.link4sales.model.entity.portray.ScvFinancialUserInfo;

/**
 * <p>
 * 财商课用户基本信息 Mapper 接口
 * </p>
 *
 * @author wzy
 * @since 2020-12-16
 */
@DS("slave")
public interface ScvFinancialUserInfoMapper extends BaseMapper<ScvFinancialUserInfo> {

}
