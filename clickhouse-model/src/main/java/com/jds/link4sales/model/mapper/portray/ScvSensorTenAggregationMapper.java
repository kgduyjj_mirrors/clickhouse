package com.jds.link4sales.model.mapper.portray;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jds.link4sales.model.dto.ActionsDto;
import com.jds.link4sales.model.entity.portray.ScvSensorTenAggregation;
import com.jds.link4sales.model.vo.ScvSensorTenAggregationV2Vo;
import com.jds.link4sales.model.vo.ScvWxSensorTenAggregationV2Vo;


import java.util.List;

/**
 * <p>
 * 神策埋点10分钟聚合数据 Mapper 接口
 * </p>
 *
 * @author JUSTIN G
 * @since 2021-01-07
 */
@DS("slave")
public interface ScvSensorTenAggregationMapper extends BaseMapper<ScvSensorTenAggregation> {

    /**
     * 通过oneid 获取股拍直播行为
     * @param
     * @return
     */
    List<ScvSensorTenAggregation> getGpLiveList(ActionsDto actionsDto);

    /**
     * 获取股拍直播行为(外部联系人)
     *
     * @param
     * @return
     */
    List<ScvSensorTenAggregationV2Vo> getWebWeChatGpLiveList(ActionsDto actionsDto);

    /**
     * 获取股拍直播行为（个人微信）
     *
     * @param
     * @return
     */
    List<ScvWxSensorTenAggregationV2Vo> getWebWxGpLiveList(ActionsDto actionsDto);

}
