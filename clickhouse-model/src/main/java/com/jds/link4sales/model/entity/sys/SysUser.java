package com.jds.link4sales.model.entity.sys;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.extension.activerecord.Model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author wzy
 * @since 2020-11-03
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SysUser extends Model<SysUser> {

    private static final long serialVersionUID = 1L;

    private Integer userId;
    /**
     * 工号
     */
    private String userLogin;
    /**
     * 密码
     */
    private String userPass;
    /**
     * 昵称
     */
    private String userNicename;
    /**
     * 邮箱
     */
    private String userEmail;
    /**
     * 注册时间
     */
    private Date userRegistered;
    /**
     * 用户状态：1正常，0删除
     */
    private Integer userStatus;
    /**
     * 手机号
     */
    private String mobile;
    /**
     * 头像
     */
    private String headPortrait;
    /**
     * 身份证号
     */
    private String idCard;
    /**
     * 姓名
     */
    private String realname;
    /**
     * 微信昵称
     */
    private String wxNickname;
    /**
     * 微信号
     */
    private String wxNumber;
    /**
     * 用户来源
     */
    private String source;
    /**
     * 是否绑定mfa 0未绑定 1 绑定
     */
    private Integer mfaBind;
    /**
     * 是否打开mfa 0关闭 1 打开
     */
    private Integer mfaOpen;
    private Integer userType;
    /**
     * 部门Id
     */
    private Integer deptId;
    private Integer roleId;
    private Date lastModifyPasswordTime;
    private Date updateTime;

    /**
     * link4sales的状态 0：禁用 1：启用
     */
    private Integer link4salesStatus;

    @Override
    protected Serializable pkVal() {
        return this.userId;
    }

}
