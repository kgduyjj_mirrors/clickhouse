package com.jds.link4sales.model.vo;

import java.util.List;

import com.baomidou.mybatisplus.extension.activerecord.Model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author wzy
 * @since 2020-11-03
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SysDeptTreeVo extends Model<SysDeptTreeVo> {

    private static final long serialVersionUID = 1L;

    /**
     * 节点id（部门id或员工工号）
     */
    private String id;

    /**
     * 节点名称（部门名称或员工名称）
     */
    private String name;
    
    /**
     * 节点类型（1：部门，2：员工）
     */
    private Integer type;

    /**
     * 父节点id
     */
    private String parentId;

    /**
     * 子节点
     */
    private List<SysDeptTreeVo> list;

}
