package com.jds.link4sales.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: zhixiang.peng
 * @date: 2020/11/6
 * @description: 公用实体
 */
@Data
public class CommonEntity {

    /** 公共数据参数 */
    private Map<String, Object> params=  new HashMap<>();

    private static final long serialVersionUID = -813066887270640503L;
    private Integer pageNum;
    private Integer pageSize;
    
    public Integer getIndex() {
        return (getPageNum() - 1) * getPageSize();
    }

    public Integer getPageNum() {
        if (pageNum == null || pageNum <= 0) {
            return 1;
        } else {
            return pageNum;
        }
    }

    public Integer getPageSize() {
        if (pageSize == null || pageSize <= 0 || pageSize > 50) {
            return 20;
        } else {
            return pageSize;
        }
    }

}
