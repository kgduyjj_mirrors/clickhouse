package com.jds.link4sales.model.vo;

import lombok.Data;

/**
 * scv金融用户信息签证官
 *
 * @author kun.gao
 * @date 2021/01/15
 */
@Data
public class ScvFinancialUserInfoVo {

    private Integer id;

    private String avatar;

    private String name;

    private String qyName;

    private Integer qyId;

    private Integer externalId;

}
