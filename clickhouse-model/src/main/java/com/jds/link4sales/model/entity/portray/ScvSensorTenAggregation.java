package com.jds.link4sales.model.entity.portray;

import java.util.Date;
import java.io.Serializable;
import java.util.List;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 神策埋点10分钟聚合数据
 * </p>
 *
 * @author JUSTIN G
 * @since 2021-01-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ScvSensorTenAggregation extends Model<ScvSensorTenAggregation> {

    private static final long serialVersionUID = 1L;

    private String oneid;
    private String distinctid;
    @TableField("intoRoomTime")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date intoRoomTime;
    private String title;
    private String stayTime;
    private Integer roomid;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;

    /**
     * 界面初始化的前3条进入房间时间
     */
    @TableField(exist = false)
    private List<ScvSensorTenAggregation> detail;

    /**
     * 界面初始化的前3条评论
     */
    @TableField(exist = false)
    private List<ScvSourceGpSperoCommentServiceLiveComment> commentDetail;

    @Override
    protected Serializable pkVal() {
        return null;
    }

}
