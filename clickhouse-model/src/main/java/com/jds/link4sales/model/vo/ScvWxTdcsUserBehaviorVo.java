package com.jds.link4sales.model.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 微信用户行为VO（初始化列表展示用 开户如今）
 *
 * @author zhixiang.peng
 * @date 2020/12/08
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ScvWxTdcsUserBehaviorVo {

    /**
     * oneId
     */
    private String oneId;

    /**
     * 事件时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date operateTime;
    /**
     * 头像
     */
    private String avatar;
    /**
     * 昵称
     */
    private String nickName;

    /**
     * 员工工号
     */
    private String employeeId;


    /**
     * 员工姓名
     */
    private String employeeName;

    /**
     * 客户微信号
     */
    private String customerWxid;

    /**
     * 公司微信号
     */
    private String companyWxid;

    /**
     * 所属微信号
     */
    private String belongWxid;

    /**
     * 客户状态  ,//1:在通讯录 2:在群 3:通讯录和群都在
     */
    private String relationStatus;

    /**
     * 添加时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date addTime;

    /**
     * 客户备注
     */
    private String  conRemark;


    /**
     * 开户银行（1-浦发，2-工行）
     */
    private Integer accountBankType;
    /**
     * 行为类型（101-点击开户链接，102-完成短信验证，103-完成基本信息，104-完成黄金签约，105-首次入金时间，106-首次发起交易时间；201-首次申请工行开户，202-非工行卡风险测评，203-工行卡开户风险测评，204-工行卡一类户开户开黄金户，205-非工行卡开户开黄金户，206-黄金交易编码开户成功，207-绑定cas交易编码，208-工行电子账户开户申请，209工行影像上传）
     */
    private Integer type;

}
