package com.jds.link4sales.model.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jds.link4sales.model.entity.sys.SysMenu;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * sys_menu  Mapper 接口
 * </p>
 *
 * @author zhixiang.peng
 * @since 2020-11-03
 */
public interface SysMenuMapper extends BaseMapper<SysMenu> {

    /**
     * 根据用户ID查询菜单
     *
     * @param userCode 用户code
     * @return 菜单列表
     */
    public List<SysMenu> selectMenuTreeByUserCode(String userCode);

    /**
     * 查询全部
     *
     * @return 菜单列表
     */
    public List<SysMenu> selectAllMenuTree();

    /**
     * 根据当前请求的功能判断当前请求的功能数据权限
     *
     * @param url  请求的url
     * @param userCode  请求的userCode
     * @return
     */
    public List<Integer> selectDataAuthByUrlAndUserCode(@Param("url")String url,@Param("userCode") String userCode);

}
