package com.jds.link4sales.model.mapper.portray;


import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jds.link4sales.model.dto.ActionsDto;
import com.jds.link4sales.model.entity.portray.ScvReportUserBehavior;
import com.jds.link4sales.model.vo.ScvUserLiveRoomBehaviorVo;
import com.jds.link4sales.model.vo.ScvWxUserBehaviorVo;
import com.jds.link4sales.model.vo.ScvWxUserLiveRoomBehaviorVo;

import java.util.List;

/**
 * <p>
 * 报告会客户行为表 Mapper 接口
 * </p>
 *
 * @author JACKSON G
 * @since 2020-11-27
 */
@DS("slave")
public interface ScvReportUserBehaviorMapper extends BaseMapper<ScvReportUserBehavior> {
    /**
     * 查询报告动作列表
     *
     * @param actionsDto 行动dto
     * @return {@link List < ScvUserLiveRoomBehaviorVo >}
     */
    List<ScvUserLiveRoomBehaviorVo> queryReportActionList(ActionsDto actionsDto);
    /**
     * 查询报告动作列表（针对微信）
     *
     * @param actionsDto 行动dto
     * @return {@link List < ScvWxUserBehaviorVo >}
     */
    List<ScvWxUserLiveRoomBehaviorVo> queryWxReportActionList(ActionsDto actionsDto);

}
