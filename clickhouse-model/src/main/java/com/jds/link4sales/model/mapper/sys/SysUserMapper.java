package com.jds.link4sales.model.mapper.sys;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jds.link4sales.model.vo.SysDeptTreeVo;
import com.jds.link4sales.model.dto.SysUserListDto;
import com.jds.link4sales.model.vo.SysUserListVo;
import com.jds.link4sales.model.entity.sys.SysUser;

/**
 * <p>
 * sys_user 系统用户表 Mapper 接口
 * </p>
 *
 * @author wzy
 * @since 2020-11-03
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

    List<SysDeptTreeVo> selectTreeInfo();
    
    List<SysUser> selectListByIdList();

    List<SysUserListVo> selectList1(SysUserListDto dto);

    /**
     *   根据员工工号获取同部门下面所有员工
     * @param  userId  员工工号
     * @return
     */
    List<SysUser> selectSameDeptUserList(String userId);

    /**
     * 根据用户工号查询用户信息
     * @param  userLogin  员工工号
     * @return
     */
    SysUserListVo  selectUserInfoByUserLogin(String userLogin);

    List<String> selectUserLoginList(String userTag);

}
