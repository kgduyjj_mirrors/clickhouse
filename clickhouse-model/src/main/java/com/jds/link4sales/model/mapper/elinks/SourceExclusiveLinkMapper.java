package com.jds.link4sales.model.mapper.elinks;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jds.link4sales.model.entity.elinks.SourceExclusiveLink;

/**
 * <p>
 * 资源专属链接表 Mapper 接口
 * </p>
 *
 * @author JACKSON G
 * @since 2021-01-27
 */
public interface SourceExclusiveLinkMapper extends BaseMapper<SourceExclusiveLink> {

}
