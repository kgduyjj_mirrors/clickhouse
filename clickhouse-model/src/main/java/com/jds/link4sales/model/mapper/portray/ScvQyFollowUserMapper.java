package com.jds.link4sales.model.mapper.portray;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jds.link4sales.model.entity.portray.ScvQyFollowUser;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wzy
 * @since 2020-11-04
 */
@DS("slave")
public interface ScvQyFollowUserMapper extends BaseMapper<ScvQyFollowUser> {

}
