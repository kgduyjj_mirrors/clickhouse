package com.jds.link4sales.model.entity.portray;

import java.util.Date;
import java.io.Serializable;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 员工客户关联关系表
 * </p>
 *
 * @author JACKSON G
 * @since 2020-11-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ScvQyStaffCustommerRelation extends Model<ScvQyStaffCustommerRelation> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private Integer qyId;
    /**
     * 员工工号
     */
    private String userId;
    /**
     * 唯一人id
     */
    private String oneId;
    /**
     * 手机号
     */
    private String mobile;
    /**
     * 配置了客户联系功能的企业员工id
     */
    private String followUserId;
    /**
     * 外部联系人的主键id
     */
    private Integer externalId;
    /**
     * 微信号
     */
    private String wxCode;
    /**
     * 0未知 1 老师号 2 助理号 3 互助号 4 员工工号 
     */
    private Integer wxType;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 更新时间
     */
    private Date updateTime;
    /**
     * 1-正常，2-删除
     */
    private Integer deleteFlag;
    private Date deleteTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
