package com.jds.link4sales.model.mapper.portray;


import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jds.link4sales.model.dto.ScvQyExternalUserListDto;
import com.jds.link4sales.model.dto.WechatUserPortraitDto;
import com.jds.link4sales.model.entity.portray.FollowWxRelation;
import com.jds.link4sales.model.entity.portray.WechatUserPortrait;
import com.jds.link4sales.model.vo.ScvQyExternalUserListVo;

import java.util.List;

/**
 * <p>
 * FollowWxRelation Mapper 接口
 * </p>
 *
 * @author cheng.zhang
 * @since 2020-11-16
 */
public interface FollowWxRelationMapper extends BaseMapper<FollowWxRelation> {

    List<ScvQyExternalUserListVo> list(ScvQyExternalUserListDto scvQyExternalUserListDto);


}
