package com.jds.link4sales.model.entity.portray;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 客户进线的渠道说明
 * </p>
 *
 * @author JACKSON G
 * @since 2020-12-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ComeDetail extends Model<ComeDetail> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    private String oneId;
    private String employeeId;
    private String addWay;
    private String serverEmployee;
    private String customerWxid;
    private String teacherIp;
    private String teacherWxid;
    private String channelName;
    private String channelSource;
    private Date comeTime;
    private Date addTime;
    private Date createTime;
    private Date updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
