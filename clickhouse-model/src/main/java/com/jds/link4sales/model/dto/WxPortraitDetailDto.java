package com.jds.link4sales.model.dto;

import com.jds.link4sales.model.entity.CommonEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * 微信用户画像dto
 *
 * @author cheng.zhang
 * @date 2020/11/26
 */
@EqualsAndHashCode(callSuper = false)
@Data
public class WxPortraitDetailDto extends CommonEntity implements Serializable {
    private static final long serialVersionUID = 8116423438782150505L;

    private Integer id;

    private String followId;

    private String externalId;
    /**
     * 企业id
     */
    private Integer qyId;
    /**
     * 名字
     */
    private String name;
    /**
     * 手机号
     */
    private String phoneNumber;
    /**
     * 年龄
     */
    private Integer age;
    /**
     * 生日
     */
    private String birth;
    /**
     * 家庭结构
     */
    private String familyStructure;
    /**
     * 收入结构
     */
    private String assetStructure;
    /**
     * 家庭地址
     */
    private String location;
    /**
     * 家庭结构
     */
    private String wxNumber;

    private Date createAt;

    private Date stopAt;

    private String qyName;

    private Integer followGender;

    private String followAvatar;

    private Integer totalCount;

    private Integer tag;

    private Integer exId;

    private String owner;

    /**
     * 0 普通  1 查询跨企业号
     */
    private Integer queryType;

    private String unionId;

    private Long createTime;
}