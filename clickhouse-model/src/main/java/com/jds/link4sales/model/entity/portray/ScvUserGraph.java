package com.jds.link4sales.model.entity.portray;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author JACKSON G
 * @since 2020-11-06
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ScvUserGraph extends Model<ScvUserGraph> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 唯一物理人Id
     */
    private String oneId;
    /**
     * 1-手机号,
     * 2-交易编码,
     * 3-头像,
     * 4-邮箱,
     * 5-openid,
     * 6-UNIONID,
     * 7-头条openid,
     * 8-WXID,
     * 9-企业微信的external_user_id,
     * 10-企业微信的unionid,
     * 11-悟空手机企业用户id,
     * 12-设备号,
     * 100-jince_sso的注册用户主键Id,
     * 101-期货注册用户主键ID,
     * 102-西瓜智投的注册用户主键ID,
     * 103-方德用户主键ID,
     * 104-芝士律动用户主键id,
     * 105-投资易课用户主键id,
     * 106-解码直播用户主键Id,
     * 107-股市智投宝主键id,
     * 108-悟空采集企业微信用户信息主键ID,
     * 109-悟空采集企业微信用户信息主键ID,
     * 110-悟空采集微信用户信息主键ID,
     * 111-悟空好友微信用户信息主键ID,
     * 112-公司微信信息,
     * 113-公众号用户信息,
     * 114-股拍用户信息
     */
    private Integer type;
    /**
     * 对应的值
     */
    private String value;
    /**
     * 来源表，与type>=100相同
     */
    private Integer source;
    /**
     * 来源表唯一标识
     */
    private String sourceId;
    /**
     * 创建时间
     */
    private Date createdAt;
    /**
     * 更新时间
     */
    private Date updatedAt;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
