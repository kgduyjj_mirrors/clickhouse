package com.jds.link4sales.model.entity.portray;

import java.util.Date;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 微信群信息详情表
 * </p>
 *
 * @author JACKSON G
 * @since 2020-12-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class WechatGroupMember extends Model<WechatGroupMember> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "member_id", type = IdType.AUTO)
    private Long memberId;
    private String roomId;
    private String oneId;
    private String customerWxid;
    private Date enterTime;
    private Date outTime;
    private Date createTime;
    private Date updateTime;


    @Override
    protected Serializable pkVal() {
        return this.memberId;
    }

}
