package com.jds.link4sales.model.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ScvQyExternalUserListVo {

    /**
     * 企业号id
     */
    private Integer qyId;

    /**
     * 客户头像
     */
    private String externalUserAvatar;

    /**
     * 员工对客户的备注
     */
    private String externalUserRemark;

    /**
     * 客户姓名
     */
    private String externalUserName;
    
    /**
     * exId
     */
    private Integer exId;
    
    /**
     * oneId
     */
    private String oneId;

    /**
     * 员工id
     */
    private String followUserId;

    /**
     * 员工姓名
     */
    private String followUserName;

    /**
     * 员工对客户的备注手机号
     */
    private String remarkMobiles;

    /**
     * 添加时间
     */
    private long createtime;

    /**
     * 标签
     */
    private String tags;

    /**
     * 渠道（客户来源）
     */
    private String state;

    /**
     * 流失状态（1：正常，2：删除）
     */
    private int deleteFlag;
    
    /**
     * 员工工号
     */
    private String userCode;

    /**
     * 员工姓名
     */
    private String name;
    
    private String mobile;

    private String qyName;
    private Integer isBind;

    /**
     * 客户来源
     */
    private Integer addWay;

    /**
     * 同行状态
     */
    private Integer competitorStatus;

    /**
     * 风险状态
     */
    private Integer riskStatus;

    /**
     * 激活状态
     */
    private Integer activeStatus;

}
