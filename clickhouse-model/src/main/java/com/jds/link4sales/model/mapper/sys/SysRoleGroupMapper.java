package com.jds.link4sales.model.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jds.link4sales.model.entity.sys.SysRoleGroup;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author JSCKSON G
 * @since 2020-11-03
 */
public interface SysRoleGroupMapper extends BaseMapper<SysRoleGroup> {

}
