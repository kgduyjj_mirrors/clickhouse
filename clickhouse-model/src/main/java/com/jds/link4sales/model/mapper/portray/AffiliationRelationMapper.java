package com.jds.link4sales.model.mapper.portray;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jds.link4sales.model.dto.ActionsDto;
import com.jds.link4sales.model.dto.WxUserPropertyQueryDto;
import com.jds.link4sales.model.entity.portray.AffiliationRelation;
import com.jds.link4sales.model.vo.WxVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import com.jds.link4sales.model.vo.ScvWxUserPropertyVo;


/**
 * <p>
 * 客户群体与对应员工的所属关系 Mapper 接口
 * </p>
 *
 * @author zhixiang.peng
 * @since 2020-12-01
 */
@DS("slave")
public interface AffiliationRelationMapper extends BaseMapper<AffiliationRelation> {


    /**
     * 微信客户用户属性一览(全部)
     * @param
     * @return
     */
    List<ScvWxUserPropertyVo> getAllWxUserPropertyList(WxUserPropertyQueryDto actionsDto);

    /**
     * 微信客户用户属性一览(全部-针对于发展部主任等缓存count查询)
     * @param actionsDto
     * @return
     */
    Integer getCacheAllWxUserPropertyList(WxUserPropertyQueryDto actionsDto);

    /**
     * 微信客户用户属性一览（我的）
     * @param
     * @return
     */
    List<ScvWxUserPropertyVo> getMyWxUserPropertyList(WxUserPropertyQueryDto actionsDto);


    /**
     * 通用电气wx好友信息
     *
     * @param customerWxId 客户wx id
     * @return {@link List<WxVo>}
     */
    List<WxVo> getWxBuddyInfo(@Param("customerWxId") String customerWxId);


}
