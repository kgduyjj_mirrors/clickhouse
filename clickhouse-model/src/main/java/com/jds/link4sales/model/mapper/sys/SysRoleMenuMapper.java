package com.jds.link4sales.model.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jds.link4sales.model.entity.sys.SysRoleMenu;

/**
 * <p>
 * sys_role_menu  Mapper 接口
 * </p>
 *
 * @author JACKSON G
 * @since 2020-11-09
 */
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu> {

}
