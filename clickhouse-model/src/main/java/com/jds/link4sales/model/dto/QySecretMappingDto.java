package com.jds.link4sales.model.dto;

import com.jds.link4sales.model.entity.CommonEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * qy秘密映射dto
 *
 * @author JACKSON G
 * @date 2020/11/03
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class QySecretMappingDto {

    private Integer qyId;

    private String secret;

    private String clientId;

    private String agentId;

    private String qyName;

}
