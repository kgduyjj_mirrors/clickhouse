package com.jds.link4sales.model.entity.portray;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * <p>
 * 配置了客户联系功能的企业员工表
 * </p>
 *
 * @author wzy
 * @since 2020-11-04
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ScvQyScopeFollowUser extends Model<ScvQyScopeFollowUser> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 企业号id
     */
    private Integer qyId;
    /**
     * 配置了客户联系功能的企业员工id
     */
    private String followUserId;
    /**
     * 企业员工姓名
     */
    private String followName;
    /**
     * 企业员工头像
     */
    private String followAvatar;
    /**
     * 企业员工性别 1-男 2-女
     */
    private Integer followGender;
    /**
     * 企业员工手机号
     */
    private String followMobile;
    /**
     * 创建时间
     */
    private Date createdAt;
    /**
     * 更新时间
     */
    private Date updatedAt;
    /**
     * 删除时间
     */
    private Date deletedAt;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
