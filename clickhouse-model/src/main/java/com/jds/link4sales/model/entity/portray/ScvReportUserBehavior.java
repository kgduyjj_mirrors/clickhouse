package com.jds.link4sales.model.entity.portray;

import java.util.Date;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 报告会客户行为表
 * </p>
 *
 * @author JACKSON G
 * @since 2020-11-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ScvReportUserBehavior extends Model<ScvReportUserBehavior> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 进入时间
     */
    private Date eventTime;
    /**
     * 消息类型 1文本、2图片、3音频、4视频
     */
    private Integer msgType;
    /**
     * 消息内容
     */
    private String messgeContent;
    /**
     * 类型，1-进入时间。2-发言
     */
    private Integer type;
    /**
     * 房间号
     */
    private Integer roomId;
    /**
     * 客户唯一标志
     */
    private String oneId;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 更新时间
     */
    private Date updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
