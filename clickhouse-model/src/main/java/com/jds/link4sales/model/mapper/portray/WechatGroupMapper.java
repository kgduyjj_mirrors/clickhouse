package com.jds.link4sales.model.mapper.portray;


import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jds.link4sales.model.entity.portray.WechatGroup;
import com.jds.link4sales.model.vo.WxGroupVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 微信群信息详情表 Mapper 接口
 * </p>
 *
 * @author JACKSON G
 * @since 2020-12-08
 */
@DS("slave")
public interface WechatGroupMapper extends BaseMapper<WechatGroup> {

    /**
     * 获取组信息
     *
     * @param customerWxId 客户wx id
     * @param companyWxid  公司wxid
     * @param type         类型
     * @return {@link List<WxGroupVo>}
     */
    List<WxGroupVo>  getGroupInfo(@Param("customerWxId")String customerWxId ,@Param("companyWxid")String companyWxid,@Param("type")Integer type );

}
