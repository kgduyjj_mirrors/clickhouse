package com.jds.link4sales.model.mapper.portray;

import java.util.List;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jds.link4sales.model.dto.ScvQyExternalUserListDto;
import com.jds.link4sales.model.vo.ScvQyExternalUserListVo;
import com.jds.link4sales.model.entity.portray.ScvQyExternalUser;

/**
 * <p>
 * 企业外部联系人表 Mapper 接口
 * </p>
 *
 * @author wzy
 * @since 2020-11-04
 */
@DS("slave")
public interface ScvQyExternalUserMapper extends BaseMapper<ScvQyExternalUser> {

    List<ScvQyExternalUserListVo> list(ScvQyExternalUserListDto scvQyExternalUserListDto);


    String getOneId(Integer externalId);



}
