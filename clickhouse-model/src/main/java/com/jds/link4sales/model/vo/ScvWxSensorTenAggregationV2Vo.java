package com.jds.link4sales.model.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * scv fwwechat mapper vo服务号用户行为
 *
 * @author kun.gao
 * @date 2020/11/06
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ScvWxSensorTenAggregationV2Vo {

    /**
     * oneId
     */
    private String oneId;

    /**
     * 头像
     */
    private String avatar;
    /**
     * 昵称
     */
    private String nickName;

    /**
     * 员工工号
     */
    private String employeeId;


    /**
     * 员工姓名
     */
    private String employeeName;

    /**
     * 客户微信号
     */
    private String customerWxid;

    /**
     * 公司微信号
     */
    private String companyWxid;

    /**
     * 所属微信号
     */
    private String belongWxid;


    /**
     * 客户状态  ,//1:在通讯录 2:在群 3:通讯录和群都在
     */
    private String relationStatus;
    
    /**
     * 客户备注
     */
    private  String  conRemark;

    /**
     * 添加时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date addTime;

    /**
     * 用户行为时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date eventTime;

    /**
     * 停留时间 (单位是秒)
     */
    private Integer stayTime;;

    /**
     * 进入房间时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date intoRoomTime;

    /**
     * 用户行为类型
     */
    private Integer eventType;

    /**
     *   用户发言类容
     */
    private String content;

    /**
     *   场次标题
     */
    private String title;

    /**
     *   场次标题id
     */
    private String titleId;

}
