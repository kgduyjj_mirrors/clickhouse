package com.jds.link4sales.model.entity.sys;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *   菜单
 * </p>
 *
 * @author zhixiang.peng
 * @since 2020-11-03
 */
@Data
public class SysMenu extends Model<SysMenu> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 菜单名称
     */
    @NotBlank(message = "菜单名称不能为空")
    private String menuName;
    /**
     * 菜单url
     */
    private String url;
    /**
     * 权限标识编码
     */
    @NotBlank(message = "权限标识编码不能为空")
    @TableField("permsCode")
    private String permsCode;
    /**
     * 父级菜单id
     */
    private Integer parentId;
    /**
     * 菜单类型:1:目录 2:页面 3:功能  4：接口
     */
    @NotNull(message = "菜单类型不能为空")
    private Integer menuType;
    /**
     * 显示顺序
     */
    private Integer orderNum;
    /**
     * 菜单图标
     */
    private String icon;
    /**
     * 数据权限
     */
    private String dataAuthType;
    /**
     * 菜单图标
     */
    private String remark;
    /**
     * 状态
     */
    private Integer status;
    /**
     * 创建人
     */
    private String createdBy;
    /**
     * 创建时间
     */
    private Date createdTime;
    /**
     * 更新人
     */
    private String updatedBy;
    /**
     * 更新时间
     */
    private Date updatedTime;

    @TableField(exist = false)
    /** 子菜单 */
    private List<SysMenu> children = new ArrayList<SysMenu>();

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "SysMenu{" +
        ", id=" + id +
        ", menuName=" + menuName +
        ", url=" + url +
        ", permsCode=" + permsCode +
        ", parentId=" + parentId +
        ", menuType=" + menuType +
        ", orderNum=" + orderNum +
        ", icon=" + icon +
        ", dataAuthType=" + dataAuthType +
        ", remark=" + remark +
        ", status=" + status +
        ", createdBy=" + createdBy +
        ", createdTime=" + createdTime +
        ", updatedBy=" + updatedBy +
        ", updatedTime=" + updatedTime +
        "}";
    }
}
