package com.jds.link4sales.model.entity.sys;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *   角色组
 * </p>
 *
 * @author zhixiang.peng
 * @since 2020-11-03
 */
@Data
public class SysRoleGroup extends Model<SysRoleGroup> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 组名
     */
    @NotBlank(message = "角色组名称不能为空")
    private String groupName;
    /**
     * 组编码
     */
    private String groupCode;
    /**
     * 创建时间
     */
    private Date createdTime;
    /**
     * 创建人
     */
    private String createdBy;
    /**
     * 更新时间
     */
    private Date updatedTime;
    /**
     * 更新人
     */
    private String updatedBy;

    /**
     * 角色组下面的角色集合
     */
    @TableField(exist = false)
    private List<SysRole> roles= new ArrayList<>();

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "SysRoleGroup{" +
                ", id=" + id +
                ", groupName=" + groupName +
                ", groupCode=" + groupCode +
                ", createdTime=" + createdTime +
                ", createdBy=" + createdBy +
                ", updatedTime=" + updatedTime +
                ", updatedBy=" + updatedBy +
                "}";
    }
}
