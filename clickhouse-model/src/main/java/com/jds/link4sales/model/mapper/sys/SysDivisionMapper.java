package com.jds.link4sales.model.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jds.link4sales.model.entity.sys.SysDivision;

/**
 * <p>
 * 事业部表 Mapper 接口
 * </p>
 *
 * @author wzy
 * @since 2020-11-03
 */
public interface SysDivisionMapper extends BaseMapper<SysDivision> {

}
