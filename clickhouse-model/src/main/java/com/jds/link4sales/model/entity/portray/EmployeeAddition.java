package com.jds.link4sales.model.entity.portray;

import java.util.Date;
import java.io.Serializable;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 员工补充的客户信息
 * </p>
 *
 * @author JACKSON G
 * @since 2020-12-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class EmployeeAddition extends Model<EmployeeAddition> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    private String companyWxid;
    private String employeeId;
    private String customerWxid;
    private String customerName;
    private String customerPhone;
    private String customerAddress;
    private String familyStruct;
    private String customerAsset;
    private Date createTime;
    private Date updateTime;
    private String hashPhone;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
