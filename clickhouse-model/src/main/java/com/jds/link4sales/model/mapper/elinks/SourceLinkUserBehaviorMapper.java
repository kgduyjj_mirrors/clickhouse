package com.jds.link4sales.model.mapper.elinks;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jds.link4sales.model.entity.elinks.SourceLinkUserBehavior;
import com.jds.link4sales.model.vo.SourceLinkUserBehaviorVo;

import java.util.List;

/**
 * <p>
 * 资源链接用户行为 Mapper 接口
 * </p>
 *
 * @author JACKSON G
 * @since 2021-01-28
 */
public interface SourceLinkUserBehaviorMapper extends BaseMapper<SourceLinkUserBehavior> {


    /**
     * 获取用户行为
     *
     * @param exclusiveLinkId exclusiveLinkId
     * @return
     */
    List<SourceLinkUserBehaviorVo> getUserBehavior(Integer exclusiveLinkId);

    
    /**
     * 获取15分钟之内的用户行为数据
     * @param 
     * @return 
     */
    List<SourceLinkUserBehaviorVo> get15MinuteData();
}
