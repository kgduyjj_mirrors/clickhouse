package com.jds.link4sales.model.vo;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * scv fwwechat mapper vo服务号用户行为
 *
 * @author kun.gao
 * @date 2020/11/06
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ScvFwwechatUserBehaviorVo {
    
    /**
     * 企业id
     */
    private String qyId;
    /**
     * 员工id
     */
    private String followUserId;
    /**
     * externalId
     */
    private Integer exId;
    /**
     * oneId
     */
    private String oneId;
    /**
     * 服务号id
     */
    private Integer fwid;
    /**
     * 服务号名称
     */
    private String fwName;
    /**
     * 行为类型，1-普通发言， 2-点击菜单，3-关注，4-取消关注，5-阅读文章
     */
    private Integer type;
    /**
     * 用户行为时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date operateTime;
    /**
     * 菜单名称或者文章标题
     */
    private String operateObjName;
    /**
     * 用户行为内容
     */
    private String content;
    /**
     * 文章id
     */
    private Integer articleId;
    /**
     * 文章阅读次数
     */
    private Integer readCount;
    /**
     * 头像
     */
    private String avatar;
    /**
     * 备注
     */
    private String mark;
    /**
     * 昵称
     */
    private String nickName;
    /**
     * 流失状态（1：正常，2：删除）
     */
    private int deleteFlag;

    private String followUserName;

    private String mobile;
    
    /**
     * 员工工号
     */
    private String userCode;

    /**
     * 员工姓名
     */
    private String name;

}
