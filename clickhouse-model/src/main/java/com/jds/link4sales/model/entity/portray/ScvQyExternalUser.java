package com.jds.link4sales.model.entity.portray;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * <p>
 * 企业外部联系人表
 * </p>
 *
 * @author wzy
 * @since 2020-11-04
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName(value = "scv_source_qy_qy_external_user")
public class ScvQyExternalUser extends Model<ScvQyExternalUser> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 外部联系人id
     */
    private String externalUserId;
    /**
     * 外部联系人姓名
     */
    private String name;
    /**
     * 外部联系人头像
     */
    private String avatar;
    /**
     * 外部联系人类型,1微信用户，2企业微信用户
     */
    private Integer type;
    /**
     * 外部联系人性别 0-未知 1-男性 2-女性
     */
    private Integer gender;
    /**
     * 外部联系人在微信开放平台的唯一身份标识
     */
    private String unionid;
    /**
     * 外部联系人的职位
     */
    private String position;
    /**
     * 外部联系人所在企业的简称，仅当联系人类型是企业微信用户时有此字段
     */
    private String corpName;
    /**
     * 外部联系人所在企业的主体名称，仅当联系人类型是企业微信用户时有此字段
     */
    private String corpFullName;
    /**
     * 外部联系人的自定义展示信息，可以有多个字段和多种类型，包括文本，网页和小程序，仅当联系人类型是企业微信用户时有此字段
     */
    private String externalProfile;
    /**
     * 同行状态 0-正常 1-同行
     */
    private Integer competitorStatus;
    /**
     * 风险状态 0-正常 1-风险
     */
    private Integer riskStatus;
    /**
     * 激活状态 0-正常 1-激活
     */
    private Integer activeStatus;
    /**
     * 创建时间
     */
    private Date createdAt;
    /**
     * 更新时间
     */
    private Date updatedAt;
    /**
     * 删除时间
     */
    private Date deletedAt;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
