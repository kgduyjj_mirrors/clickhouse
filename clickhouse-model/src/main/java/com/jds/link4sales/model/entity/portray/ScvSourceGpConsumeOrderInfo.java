package com.jds.link4sales.model.entity.portray;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author JACKSON G
 * @since 2021-01-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ScvSourceGpConsumeOrderInfo extends Model<ScvSourceGpConsumeOrderInfo> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 唯一人id
     */
    private String oneId;
    /**
     * 用户id
     */
    @TableField("userId")
    private String userId;
    /**
     * 消费订单号
     */
    @TableField("consumeNo")
    private String consumeNo;
    /**
     * 商品id
     */
    @TableField("productId")
    private Integer productId;
    /**
     * 商品名称
     */
    @TableField("productName")
    private String productName;
    /**
     * 博主昵称
     */
    private String nickname;
    /**
     * 商品类型 0:专辑课程 1:未使用 2:直播打赏 3:圈子打赏 4:圈子动态 5:加入粉丝团 6:付费入圈 (固定加入一年) 7:付费入圈 (支持不同周期) 8:付费直播 9:付费专栏文章 10:付费专栏周期 11:付费系列直播 12:直播抽奖播主消费
     */
    @TableField("productType")
    private Integer productType;
    /**
     * 附加id（圈子打赏对应的动态id）
     */
    @TableField("subProductId")
    private Integer subProductId;
    /**
     * 商品数量
     */
    @TableField("productCount")
    private Integer productCount;
    /**
     * 状态 0:未支付 1:已支付 2:已取消 3:已完成 4:已退款
     */
    private Integer status;
    /**
     * 订单价格
     */
    private Long amount;
    /**
     * 订单原价
     */
    @TableField("originAmount")
    private Long originAmount;
    /**
     * 是否开发票
     */
    @TableField("isBilled")
    private Integer isBilled;
    /**
     * 支付时间
     */
    @TableField("paidTime")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date paidTime;
    /**
     * 完成时间
     */
    @TableField("doneTime")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date doneTime;
    /**
     * 订单商品信息
     */
    private String products;
    @TableField("createdAt")
    private Date createdAt;
    @TableField("updatedAt")
    private Date updatedAt;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getAmount() {
        if(amount==null){
            return 0L;
        }
        return amount/100;
    }

    public Long getOriginAmount() {
        if(originAmount==null){
            return 0L;
        }
        return originAmount/100;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
