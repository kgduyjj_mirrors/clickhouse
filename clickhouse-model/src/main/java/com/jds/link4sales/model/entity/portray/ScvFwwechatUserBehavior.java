package com.jds.link4sales.model.entity.portray;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author JACKSON G
 * @since 2020-11-06
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ScvFwwechatUserBehavior extends Model<ScvFwwechatUserBehavior> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 服务号id
     */
    private Integer fwid;
    /**
     * 服务号名称
     */
    private String fwName;
    /**
     * openid
     */
    private String openid;
    /**
     * 行为类型，1-普通发言， 2-点击菜单，3-关注，4-取消关注，5-阅读文章
     */
    private Integer type;
    /**
     * 用户行为时间
     */
    private Date operateTime;
    /**
     * 菜单名称或者文章标题
     */
    private String operateObjName;
    /**
     * 用户行为内容
     */
    private String content;
    /**
     * 文章id
     */
    private Integer articleId;
    /**
     * 文章阅读次数
     */
    private Integer readCount;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
