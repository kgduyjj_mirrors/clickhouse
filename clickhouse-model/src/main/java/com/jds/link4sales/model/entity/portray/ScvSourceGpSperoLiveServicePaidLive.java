package com.jds.link4sales.model.entity.portray;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 付费直播场次表
 * </p>
 *
 * @author zhixiang.peng
 * @since 2021-01-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ScvSourceGpSperoLiveServicePaidLive extends Model<ScvSourceGpSperoLiveServicePaidLive> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 付费直播标题
     */
    private String title;
    /**
     * 直播类型（1付费直播 2权限直播）
     */
    private Integer type;
    /**
     * 亮点
     */
    @TableField("highLightImg")
    private String highLightImg;
    /**
     * 封面
     */
    private String cover;
    /**
     * 头图
     */
    @TableField("headCover")
    private String headCover;
    /**
     * 已购人数
     */
    @TableField("paidCount")
    private Integer paidCount;
    /**
     * 价格
     */
    private Integer price;
    /**
     * 原价(划线价)
     */
    @TableField("originPrice")
    private Integer originPrice;
    /**
     * 商品ID
     */
    @TableField("productId")
    private Integer productId;
    /**
     * 直播间ID
     */
    @TableField("roomId")
    private Integer roomId;
    /**
     * 播主ID
     */
    @TableField("userId")
    private String userId;
    /**
     * 付费直播状态1直播中，2未开始3已结束
     */
    private Integer status;
    /**
     * 运营审核结果
     */
    @TableField("auditStatus")
    private String auditStatus;
    /**
     * 审核人
     */
    private String operator;
    /**
     * 审核时间
     */
    @TableField("auditTime")
    private Date auditTime;
    /**
     * 开始时间
     */
    @TableField("startTime")
    private Date startTime;
    /**
     * 结束时间
     */
    @TableField("endTime")
    private Date endTime;
    @TableField("createdAt")
    private Date createdAt;
    @TableField("updatedAt")
    private Date updatedAt;
    /**
     * 放宽后的开始时间
     */
    @TableField("relaxStartTime")
    private Date relaxStartTime;
    /**
     * 放宽后的结束时间
     */
    @TableField("relaxEndTime")
    private Date relaxEndTime;
    /**
     * 该场付费直播是否有过记录
     */
    @TableField("hasPlayed")
    private Integer hasPlayed;
    /**
     * 是否审核通过过
     */
    @TableField("hasAuditSuccess")
    private Integer hasAuditSuccess;
    /**
     * 权限说明
     */
    @TableField("rightsDesc")
    private String rightsDesc;
    /**
     * 引导按钮跳转类型: 1.私信 2.圈子 3.粉丝团 4.付费直播
     */
    @TableField("guideType")
    private Integer guideType;
    /**
     * 引导按钮内容id
     */
    @TableField("guideId")
    private Integer guideId;
    /**
     * 圈子直播对应的id
     */
    @TableField("qId")
    private Integer qId;
    /**
     * 粉丝组id数组
     */
    @TableField("fansGroupIds")
    private String fansGroupIds;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
