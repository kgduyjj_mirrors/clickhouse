package com.jds.link4sales.model.mapper.portray;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jds.link4sales.model.entity.portray.ScvSourceQyWechatRegistration;

/**
 * <p>
 * 公司微信号登记表 Mapper 接口
 * </p>
 *
 * @author JACKSON G
 * @since 2021-01-29
 */
@DS("slave")
public interface ScvSourceQyWechatRegistrationMapper extends BaseMapper<ScvSourceQyWechatRegistration> {

}
