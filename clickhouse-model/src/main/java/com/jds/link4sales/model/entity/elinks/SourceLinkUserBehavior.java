package com.jds.link4sales.model.entity.elinks;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 资源链接用户行为
 * </p>
 *
 * @author JACKSON G
 * @since 2021-01-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SourceLinkUserBehavior extends Model<SourceLinkUserBehavior> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 工号
     */
    @NotBlank(message = "员工工号不能为空")
    private String account;

    @NotBlank(message = "资源链接id不能为空")
    private String sourceLinkId;

    /**
     * source_exclusive_link主键id
     */
    private Integer exclusiveLinkId;
    /**
     * 用户id
     */
    @NotBlank(message = "用户id不能为空")
    private String userId;
    /**
     * 用户昵称
     */
    @NotBlank(message = "用户昵称不能为空")
    private String nickname;
    /**
     * 真实姓名
     */
    private String realname;
    /**
     * 头像
     */
    private String headimgurl;
    /**
     * 类型：1-访问，2-访问5分钟，3-支付
     */
    private Integer type;
    /**
     * 创建时间
     */
    private Date createTime;
    
    /**
     * 访问次数
     */
    @TableField(exist = false)
    private transient  Integer accessCount;

    /**
     * 用户标签
     */
    @TableField(exist = false)
    private transient  String tags;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
