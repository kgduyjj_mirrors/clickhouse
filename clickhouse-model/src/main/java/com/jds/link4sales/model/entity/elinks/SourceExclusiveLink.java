package com.jds.link4sales.model.entity.elinks;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 资源专属链接表
 * </p>
 *
 * @author JACKSON G
 * @since 2021-01-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SourceExclusiveLink extends Model<SourceExclusiveLink> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * source_link主键id
     */
    @NotNull(message = "资源链接id不能为空")
    private Integer sourceLinkId;
    /**
     * 资源名称
     */
    private String sourceName;
    /**
     * 链接地址(拼接后的个人专属连接地址)
     */
    private String sourceUrl;
    /**
     * 1:非自生产连接 2：自生产连接
     */
    private Integer type;
    /**
     * 工号
     */
    private String account;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 更新时间
     */
    private Date updateTime;
    
    /**
     * 企业followUserId
     */
    @TableField(exist = false)
    private String  followUserId;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
