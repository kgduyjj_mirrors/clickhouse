package com.jds.link4sales.model.dto;

import com.jds.link4sales.model.entity.elinks.SourceLinkConfig;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 源链接dto
 *
 * @author kun.gao
 * @date 2021/01/28
 */
@EqualsAndHashCode(callSuper = false)
@Data
public class SourceLinkDto extends SourceLinkConfig {
    /**
     * 当前页
     */
    private Integer page;
    /**
     * 显示条数
     */
    private Integer limit;

    /**
     * 用户账号
     */
    private String account;

    private String sourceName;

}
