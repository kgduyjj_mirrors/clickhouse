package com.jds.link4sales.model.mapper.portray;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jds.link4sales.model.entity.portray.ScvUserGraph;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author JACKSON G
 * @since 2020-11-06
 */
public interface ScvUserGraphMapper extends BaseMapper<ScvUserGraph> {

}
