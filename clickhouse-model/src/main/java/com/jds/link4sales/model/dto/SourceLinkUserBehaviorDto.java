package com.jds.link4sales.model.dto;

import com.jds.link4sales.model.entity.elinks.SourceLinkUserBehavior;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 源链接dto
 *
 * @author kun.gao
 * @date 2021/01/28
 */
@EqualsAndHashCode(callSuper = false)
@Data
public class SourceLinkUserBehaviorDto extends SourceLinkUserBehavior {
    /**
     * 当前页
     */
    private Integer pageNum;
    /**
     * 显示条数
     */
    private Integer pageSize;

    /**
     * followUserId
     */
    private String  followUserId;

    /**
     * sign
     */
    private String  sign;

}
