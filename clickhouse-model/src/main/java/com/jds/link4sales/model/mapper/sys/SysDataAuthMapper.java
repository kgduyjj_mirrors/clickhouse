package com.jds.link4sales.model.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jds.link4sales.model.entity.sys.SysDataAuth;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zhixiang.peng
 * @since 2020-11-12
 */
public interface SysDataAuthMapper extends BaseMapper<SysDataAuth> {

}
