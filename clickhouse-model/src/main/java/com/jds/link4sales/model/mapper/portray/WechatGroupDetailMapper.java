package com.jds.link4sales.model.mapper.portray;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jds.link4sales.model.entity.portray.WechatGroupDetail;

/**
 * <p>
 * 微信群信息详情表 Mapper 接口
 * </p>
 *
 * @author JACKSON G
 * @since 2020-12-01
 */
public interface WechatGroupDetailMapper extends BaseMapper<WechatGroupDetail> {

}
