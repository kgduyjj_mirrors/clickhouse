package com.jds.link4sales.model.entity.portray;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 财商用户订单信息
 * </p>
 *
 * @author wzy
 * @since 2020-12-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ScvFinancialOrderInfo extends Model<ScvFinancialOrderInfo> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 唯一物理人Id
     */
    private String oneId;
    /**
     * 订单编号
     */
    private Long orderId;
    /**
     * 商品表
     */
    private Integer productId;
    /**
     * 手机号
     */
    private String mobile;
    /**
     * 订单金额
     */
    private BigDecimal amount;
    /**
     * 实付金额
     */
    private BigDecimal payAmount;
    /**
     * 订单状态；-1：废单 -2: 取消订单 -3: 过期订单 -4. 已退款 0:下单；9:订单完成；-9:订单服务到期
     */
    private Integer status;
    /**
     * 是否已生成电子发票 0：未生成， 1：生成中，2：已生成
     */
    private Integer isGeneratedElectronicInvoice;
    /**
     * 订单服务延长天数
     */
    private Integer extendDays;
    /**
     * 下单员工号
     */
    private String employeeId;
    /**
     * 订单销售渠道, 1为股拍,2为抖音,不传默认为芝士律动 值为0
     */
    private Integer sellChannel;
    /**
     * 购买渠道, 1为公众号支付, 2为H5支付
     */
    private Integer buyChannel;
    /**
     * 推广微信号
     */
    private String wxNumber;
    /**
     * 完成时间
     */
    private Date orderCompleted;
    /**
     * 创建时间
     */
    private Date orderCreated;
    /**
     * 修改时间
     */
    private Date orderUpdated;
    /**
     * 创建时间
     */
    private Date createdAt;
    /**
     * 更新时间
     */
    private Date updatedAt;

    /**
     * 是否报名；-1：报名 -0: 未报名
     */
    private Integer isApplication;

    /**
     * 是否学习；-1：学习 -0: 未学习
     */
    private Integer isStudy;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
