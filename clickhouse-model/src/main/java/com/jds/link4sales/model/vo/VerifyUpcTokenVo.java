package com.jds.link4sales.model.vo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @Author zhixiang.peng
 * @Description
 * @Param 
 * @return 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class VerifyUpcTokenVo  {

    public static final VerifyUpcTokenVo SUCCESS = new VerifyUpcTokenVo(true,null,null );

    /**
     * token是否有效
     */
    private boolean result;
    /**
     *  用户名
     */
    private String username ;
    /**
     *  如果不为空则为刷新的token  需重新添加到cookie中
     */
    private String token ;

}
