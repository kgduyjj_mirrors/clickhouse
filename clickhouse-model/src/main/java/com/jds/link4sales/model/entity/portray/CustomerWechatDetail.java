package com.jds.link4sales.model.entity.portray;

import java.util.Date;

import java.io.Serializable;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 客户微信详情表
 * </p>
 *
 * @author JACKSON G
 * @since 2020-12-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class CustomerWechatDetail extends Model<CustomerWechatDetail> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    private String oneId;
    private Long userId;
    private String customerWxid;
    private String alias;
    private String wechatWxacc;
    private String nickname;
    private String pyInitial;
    private String quanPin;
    private String wechatHeadUrl;
    private Integer riskStatus;
    private Integer activeStatus;
    private Integer peerStatus;
    private Integer genderFlag;
    private Date createTime;
    private Date updateTime;
    private Date delTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
