package com.jds.link4sales.model.entity.portray;

import java.util.Date;
import java.io.Serializable;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 直播评论表
 * </p>
 *
 * @author JUSTIN G
 * @since 2021-01-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ScvSourceGpSperoCommentServiceLiveComment extends Model<ScvSourceGpSperoCommentServiceLiveComment> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    @TableField("createdAt")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createdAt;
    @TableField("updatedAt")
    private Date updatedAt;
    @TableField("userId")
    private String userId;
    @TableField("roomId")
    private Integer roomId;
    private String content;
    /**
     * 评论长度
     */
    @TableField("contentLength")
    private Integer contentLength;
    private String status;
    @TableField("checkUser")
    private String checkUser;
    @TableField("checkTime")
    private Date checkTime;
    @TableField("isFake")
    private Integer isFake;
    private String platform;
    @TableField("isDeleted")
    private Integer isDeleted;
    /**
     * 评论来源.0-真实用户评论;1-大数据;2:运营评论库;3-爬虫;4-运营后台评论
     */
    private Integer source;
    /**
     * 是否已审核
     */
    @TableField("hasAudit")
    private Integer hasAudit;
    
    @TableField("oneID")
    private String oneId;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
