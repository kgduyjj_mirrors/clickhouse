package com.jds.link4sales.model.vo;

import lombok.Data;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 签证官scv用户直播室行为
 *
 * @author JACKSON G
 * @date 2020/11/09
 */
@Data
public class ScvUserLiveRoomBehaviorVo {

    private Integer roomId;
    /**
     * 房间名称
     */
    private String roomName;
    /**
     * 来源id
     */
    private Integer sourceId;
    /**
     * 类型，1-进入时间。2-发言
     */
    private Integer type;
    /**
     * 内容类型：1-视频(日常直播)；2-文字(微课)；3视频加文字（报告会）
     */
    private Integer contentType;
    /**
     * 行为时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date operateTime;
    /**
     * 消息类型 1文本、2图片、3音频、4视频
     */
    private Integer msgType;
    /**
     * 消息内容
     */
    private String messgeContent;

    private Integer exId;
    
    private Integer qyId;

    private String followUserId;

    private String followUserName;

    private String mobile;
    
    private String externalId;

    /**
     * oneId
     */
    private String oneId;

    /**
     * 头像
     */
    private String avatar;
    /**
     * 备注
     */
    private String mark;
    /**
     * 昵称
     */
    private String nickName;
    /**
     * 流失状态（1：正常，2：删除）
     */
    private Integer deleteFlag;
    
    /**
     * 员工工号
     */
    private String userCode;

    /**
     * 员工姓名
     */
    private String name;

}
