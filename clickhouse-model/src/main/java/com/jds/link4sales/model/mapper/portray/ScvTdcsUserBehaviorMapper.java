package com.jds.link4sales.model.mapper.portray;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jds.link4sales.model.dto.ActionsDto;
import com.jds.link4sales.model.entity.portray.ScvTdcsUserBehavior;
import com.jds.link4sales.model.vo.ScvTdcsUserBehaviorVo;
import com.jds.link4sales.model.vo.ScvWxTdcsUserBehaviorVo;
import com.jds.link4sales.model.vo.ScvWxUserBehaviorVo;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author JACKSON G
 * @since 2020-11-09
 */
@DS("slave")
public interface ScvTdcsUserBehaviorMapper extends BaseMapper<ScvTdcsUserBehavior> {

    /**
     * 动作列表查询开账户
     *
     * @param actionsDto
     *            行动dto
     * @return {@link List<ScvTdcsUserBehaviorVo>}
     */
    List<ScvTdcsUserBehaviorVo> queryOpenAccountActionList(ActionsDto actionsDto);

    /**
     * 动作列表查询开账户（微信）
     *
     * @param actionsDto
     *            行动dto
     * @return {@link List<ScvWxUserBehaviorVo>}
     */
    List<ScvWxTdcsUserBehaviorVo> queryWxOpenAccountActionList(ActionsDto actionsDto);

    /**
     * 动作列表查询开账户
     *
     * @param actionsDto
     *            行动dto
     * @return {@link List<ScvTdcsUserBehaviorVo>}
     */
    List<ScvTdcsUserBehaviorVo> queryUserOpenAccountActionList(ActionsDto actionsDto);
    
    List<ScvTdcsUserBehaviorVo> queryWxUserOpenAccountActionList(ActionsDto actionsDto);

    /**
     * 根据用户id获取企业id一览
     *
     * @param userId  用户id
     * @return {@link List < ScvTdcsUserBehaviorVo >}
     */
    List<Integer>   queryQyIdByUserId(String userId);

}
