package com.jds.link4sales.model.mapper.portray;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jds.link4sales.model.entity.portray.EmployeeAddition;

/**
 * <p>
 * 员工补充的客户信息 Mapper 接口
 * </p>
 *
 * @author JACKSON G
 * @since 2020-12-10
 */
public interface EmployeeAdditionMapper extends BaseMapper<EmployeeAddition> {

}
