package com.jds.link4sales.model.entity.portray;

import java.util.Date;
import java.io.Serializable;


import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 微信群信息详情表
 * </p>
 *
 * @author JACKSON G
 * @since 2020-12-08
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class WechatGroup extends Model<WechatGroup> {

    private static final long serialVersionUID = 1L;

    private Long groupId;
    private String roomId;
    private String roomOwnerId;
    private String roomOwnerName;
    private String roomOwner;
    private String roomName;
    private String roomHeadUrl;
    private Date groupTime;
    private Date createTime;
    private Date updateTime;


    @Override
    protected Serializable pkVal() {
        return this.groupId;
    }

}
