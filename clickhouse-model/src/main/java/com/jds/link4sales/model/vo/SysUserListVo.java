package com.jds.link4sales.model.vo;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author wzy
 * @since 2020-11-03
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SysUserListVo {

    private Integer userId;
    /**
     * 昵称
     */
    private String userNicename;
    /**
     * 邮箱
     */
    private String userEmail;
    /**
     * 注册时间
     */
    private Date userRegistered;
    /**
     * 手机号
     */
    private String mobile;
    /**
     * 头像
     */
    private String headPortrait;
    /**
     * 身份证号
     */
    private String idCard;
    /**
     * 姓名
     */
    private String realname;
    /**
     * 工号
     */
    private String userLogin;
    /**
     * 部门名称
     */
    private String deptName;
    /**
     * 角色名称
     */
    private String roleName;
    /**
     * 用户状态：1正常，0删除
     */
    private Integer userStatus;
    /**
     * 开通人
     */
    private String createdBy;
    /**
     * 开通时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createdTime;
    /**
     * 微信昵称
     */
    private String wxNickname;
    /**
     * 微信号
     */
    private String wxNumber;
    /**
     * 用户来源
     */
    private String source;
    /**
     * 是否绑定mfa 0未绑定 1 绑定
     */
    private Integer mfaBind;
    /**
     * 是否打开mfa 0关闭 1 打开
     */
    private Integer mfaOpen;
    private Integer userType;
    /**
     * 部门Id
     */
    private Integer deptId;
    private Integer roleId;

    /**
     * link4salesStatus   0禁用 1 启用
     */
    private  Integer link4salesStatus;
    
}
