package com.jds.link4sales.model.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * 微信用户行为vo
 *
 * @author zhixiang.peng
 * @date 2020/12/07
 */
@Data
public class ScvWxUserLiveRoomBehaviorVo {

    /**
     * oneId
     */
    private String oneId;

    /**
     * 事件时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date operateTime;
    /**
     * 头像
     */
    private String avatar;
    /**
     * 昵称
     */
    private String nickName;

    /**
     * 员工工号
     */
    private String employeeId;


    /**
     * 员工姓名
     */
    private String employeeName;

    /**
     * 客户微信号
     */
    private String customerWxid;

    /**
     * 公司微信号
     */
    private String companyWxid;

    /**
     * 所属微信号
     */
    private String belongWxid;


    /**
     * 客户状态  ,//1:在通讯录 2:在群 3:通讯录和群都在
     */
    private String relationStatus;

    /**
     * 添加时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date addTime;

    /**
     * 客户备注
     */
    private String  conRemark;

    /**
     * 类型，1-进入时间。2-发言
     */
    private Integer type;
    /**
     * 内容类型：1-视频(日常直播)；2-文字(微课)；3视频加文字（报告会）
     */
    private Integer contentType;

    /**
     * 消息类型 1文本、2图片、3音频、4视频
     */
    private Integer msgType;
    /**
     * 消息内容
     */
    private String messgeContent;

    /**
     * 房间名称
     */
    private String roomName;

    /**
     * 房间id
     */
    private String roomId;

}
