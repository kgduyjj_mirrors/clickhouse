package com.jds.link4sales.model.mapper.portray;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jds.link4sales.model.dto.ActionsDto;
import com.jds.link4sales.model.entity.portray.ScvSensorTenAggregationV2;
import com.jds.link4sales.model.vo.ScvSensorTenAggregationResultVo;
import com.jds.link4sales.model.vo.ScvSensorTenAggregationV2Vo;
import com.jds.link4sales.model.vo.ScvWxSensorTenAggregationV2Vo;

import java.util.List;

/**
 * <p>
 * 神策埋点10分钟聚合数据 Mapper 接口
 * </p>
 *
 * @author zhixiang.peng
 * @since 2021-01-21
 */
@DS("slave")
public interface ScvSensorTenAggregationV2Mapper extends BaseMapper<ScvSensorTenAggregationV2> {

    /**
     * 获取股拍直播行为(外部联系人)
     *
     * @param
     * @return
     */
    List<ScvSensorTenAggregationV2Vo> getWebWeChatGpLiveList(ActionsDto actionsDto);

    /**
     * 获取股拍直播行为（个人微信）
     *
     * @param
     * @return
     */
    List<ScvWxSensorTenAggregationV2Vo> getWebWxGpLiveList(ActionsDto actionsDto);

    /**
     * 获取股拍直播行为 侧边栏（查询出事件时间最靠前的直播室）
     * @param
     * @return
     */
    List<ScvSensorTenAggregationResultVo> getGpLiveList(ActionsDto actionsDto);
}
