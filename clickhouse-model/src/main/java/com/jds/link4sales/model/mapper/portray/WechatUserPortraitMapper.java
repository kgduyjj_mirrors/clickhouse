package com.jds.link4sales.model.mapper.portray;


import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jds.link4sales.model.dto.WechatUserPortraitDto;
import com.jds.link4sales.model.entity.portray.WechatUserPortrait;

import java.util.List;

/**
 * <p>
 * wechat_user_portrait Mapper 接口
 * </p>
 *
 * @author wzy
 * @since 2020-11-04
 */
public interface WechatUserPortraitMapper extends BaseMapper<WechatUserPortrait> {

    /**
     * 得到好友信息
     *
     * @param wechatUserPortraitDto 微信用户画像dto
     * @return {@link List <WechatUserPortraitDto>}
     */
    @DS("slave")
    List<WechatUserPortraitDto> getBuddyInfo(WechatUserPortraitDto wechatUserPortraitDto);

    /**
     * 得到群信息
     *
     * @param wechatUserPortraitDto 微信用户画像dto
     * @return {@link List<WechatUserPortraitDto>}
     */
    @DS(value = "slave")
    List<WechatUserPortraitDto> getGroupsInfo(WechatUserPortraitDto wechatUserPortraitDto);


}
