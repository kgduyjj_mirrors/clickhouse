package com.jds.link4sales.model.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**   新增角色菜单关系的dto
 * @author: zhixiang.peng
 * @date: 2020/11/9
 * @description: 
 */
@Data
public class RoleMenuDto {
    
    /**
     * 角色id
     */
    @NotNull(message = "角色id不能为空")
    private  Integer  roleId;
    
    /**
     * 菜单ids
     */
    @NotEmpty(message="菜单ids不能为空")
    private List<Integer> menuIds;
    
}
