package com.jds.link4sales.model.dto;

import java.util.List;

import javax.validation.constraints.NotNull;

import com.jds.link4sales.model.entity.CommonEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * scv qy dto外部用户列表
 *
 * @author zhiyuan wang
 * @date 2020/11/06
 */
@Data
@Builder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
public class ScvQyExternalUserListDto extends CommonEntity {

    /**
     * 企业号id
     */
    @NotNull(message = "企业号id不能为空")
    private Integer qyId;

    /**
     * 添加时间（开始时间，单位s）
     */
    private Long startTime;

    /**
     * 添加时间（结束时间，单位s）
     */
    private Long stopTime;

    /**
     * 客户姓名
     */
    private String externalUserName;

    /**
     * 企业标签（1：同行，2：风险，3：激活）
     */
    private Integer qyTag;

    /**
     * 流失状态（1：正常，2：流失）
     */
    private Integer loseStatus;

    /**
     * 员工工号
     */
    private String userCode;
    
    /**
     * 员工名或工号
     */
    private String userTag;

    private List<String> userLoginList;


  private String customerWxId;

}
