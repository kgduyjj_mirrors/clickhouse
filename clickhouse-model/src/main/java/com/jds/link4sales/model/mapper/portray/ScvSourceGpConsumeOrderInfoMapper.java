package com.jds.link4sales.model.mapper.portray;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jds.link4sales.model.entity.portray.ScvSourceGpConsumeOrderInfo;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author JACKSON G
 * @since 2021-01-07
 */
@DS("slave")
public interface ScvSourceGpConsumeOrderInfoMapper extends BaseMapper<ScvSourceGpConsumeOrderInfo> {

}
