package com.jds.link4sales.model.mapper.portray;

import java.util.List;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jds.link4sales.model.dto.ActionsDto;
import com.jds.link4sales.model.entity.portray.ScvFinancialOrderInfo;
import com.jds.link4sales.model.entity.portray.ScvFinancialUserInfo;
import com.jds.link4sales.model.vo.ScvFinancialOrderInfoVo;
import com.jds.link4sales.model.vo.ScvFinancialUserInfoVo;

/**
 * <p>
 * 财商用户订单信息 Mapper 接口
 * </p>
 *
 * @author wzy
 * @since 2020-12-16
 */
@DS("slave")
public interface ScvFinancialOrderInfoMapper extends BaseMapper<ScvFinancialOrderInfo> {

    List<ScvFinancialOrderInfoVo> queryUserFinancialOrderInfoList(ActionsDto actionsDto);

    List<ScvFinancialOrderInfoVo> queryWxUserFinancialOrderInfoList(ActionsDto actionsDto);

    List<ScvFinancialOrderInfoVo> queryH5FinancialOrderList(ActionsDto actionsDto);

    List<ScvFinancialUserInfoVo> queryH5FinancialUserList(ActionsDto actionsDto);

}
