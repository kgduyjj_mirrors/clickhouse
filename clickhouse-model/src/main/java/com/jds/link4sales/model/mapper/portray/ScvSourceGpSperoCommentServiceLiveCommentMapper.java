package com.jds.link4sales.model.mapper.portray;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jds.link4sales.model.entity.portray.ScvSourceGpSperoCommentServiceLiveComment;

/**
 * <p>
 * 直播评论表 Mapper 接口
 * </p>
 *
 * @author JUSTIN G
 * @since 2021-01-07
 */
@DS("slave")
public interface ScvSourceGpSperoCommentServiceLiveCommentMapper extends BaseMapper<ScvSourceGpSperoCommentServiceLiveComment> {

}
