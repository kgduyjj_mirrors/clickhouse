package com.jds.link4sales.model.mapper.portray;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jds.link4sales.model.entity.portray.CustomerWechatDetail;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 客户微信详情表 Mapper 接口
 * </p>
 *
 * @author JACKSON G
 * @since 2020-12-01
 */
@DS("slave")
public interface CustomerWechatDetailMapper extends BaseMapper<CustomerWechatDetail> {

    /**
     * 得到wx id
     *
     * @param cusId 客户id
     * @param acc   acc
     * @return {@link String}
     */
    String getWxId(@Param("cusId")String cusId,@Param("acc")String acc);

    /**
     * 得到wx id
     *
     * @param cusId 客户id
     * @param acc   acc
     * @return {@link String}
     */
    String getOneId(@Param("cusId")String cusId,@Param("acc")String acc);

}
