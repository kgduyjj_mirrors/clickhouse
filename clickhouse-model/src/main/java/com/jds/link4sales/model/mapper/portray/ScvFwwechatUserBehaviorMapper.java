package com.jds.link4sales.model.mapper.portray;


import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jds.link4sales.model.dto.ActionsDto;
import com.jds.link4sales.model.entity.portray.ScvFwwechatUserBehavior;
import com.jds.link4sales.model.vo.ScvFwwechatUserBehaviorVo;
import com.jds.link4sales.model.vo.ScvWxFwwechatUserBehaviorVo;
import com.jds.link4sales.model.vo.ScvWxUserBehaviorVo;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author JACKSON G
 * @since 2020-11-06
 */
@DS("slave")
public interface ScvFwwechatUserBehaviorMapper extends BaseMapper<ScvFwwechatUserBehavior> {

    /**
     * 弗兰克-威廉姆斯行为列表查询
     *
     * @param actionsDto 行动dto
     * @return {@link List< ScvFwwechatUserBehaviorVo >}
     */
    List<ScvFwwechatUserBehaviorVo> queryFwBehaviorList(ActionsDto actionsDto);


    /**
     * 弗兰克-威廉姆斯行为列表查询(微信)
     *
     * @param actionsDto 行动dto
     * @return {@link List< ScvWxUserBehaviorVo >}
     */
    List<ScvWxFwwechatUserBehaviorVo> queryWxFwBehaviorList(ActionsDto actionsDto);
    /**
     * 弗兰克-威廉姆斯行为列表查询
     *
     * @param actionsDto 行动dto
     * @return {@link List< ScvFwwechatUserBehaviorVo >}
     */
    List<ScvFwwechatUserBehaviorVo> queryUserFwBehaviorList(ActionsDto actionsDto);
    
    List<ScvFwwechatUserBehaviorVo> queryWxUserFwBehaviorList(ActionsDto actionsDto);

}
