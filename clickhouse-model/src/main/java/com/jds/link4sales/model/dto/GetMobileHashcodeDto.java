package com.jds.link4sales.model.dto;


import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author: zhixiang.peng
 * @date: 2021/1/14
 * @description:  获取手机号的随机字符串
 */
@Data
public class GetMobileHashcodeDto {
    
    /**
     * 手机号
     */
    @NotBlank(message = "手机号不能为空")
    private String mobile;

    /**
     * 渠道标识
     */
    private String state;

    /**
     * 企业id
     */
    @NotNull(message = "qyId不能为空")
    private Integer qyId;
}
