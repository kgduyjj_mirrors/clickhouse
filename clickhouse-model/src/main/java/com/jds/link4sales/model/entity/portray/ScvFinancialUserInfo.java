package com.jds.link4sales.model.entity.portray;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 财商课用户基本信息
 * </p>
 *
 * @author wzy
 * @since 2020-12-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ScvFinancialUserInfo extends Model<ScvFinancialUserInfo> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 唯一物理人Id
     */
    private String oneId;
    /**
     * 金策手机号加密
     */
    private String mobile;
    /**
     * 第三方平台用户id
     */
    private Integer thirdPlatformUserId;
    /**
     * 芝士律动ID
     */
    private String appId;
    /**
     * 姓名
     */
    private String realName;
    /**
     * 推广页面渠道
     */
    private String userSource;
    /**
     * 性别，1-男，2-女，0-未知
     */
    private Integer gender;
    /**
     * 年龄
     */
    private Integer age;
    /**
     * 投资经验， 1-股市小白还没有账户，2-初入股市一两年，3-深入股海三五年，4-深耕股市五年以上
     */
    private Integer investExperience;
    /**
     * 股拍用户id
     */
    private String userid;
    /**
     * 股拍openid
     */
    private String openid;
    /**
     * 股拍unionid
     */
    private String unionid;
    /**
     * 股拍授权时间
     */
    private Date authorizeTime;
    /**
     * 授权微信昵称
     */
    private String nickname;
    /**
     * 授权微信头像
     */
    private String avatar;
    /**
     * 员工工号
     */
    private String wkno;

    private String answer;

    private Date createdAt;

    private Date updatedAt;

    private String answerId;

    private String answerFlag;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
