package com.jds.link4sales.model.dto;

import java.util.Date;

import javax.validation.constraints.NotNull;

import com.jds.link4sales.model.entity.CommonEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SysUserListDto extends CommonEntity {
    
    /**
     * 节点id
     */
    @NotNull(message = "节点id不能为空")
    private Integer id;
    /**
     * 节点类型（1：部门，2：员工）
     */
    @NotNull(message = "节点类型不能为空")
    private Integer type;
    /**
     * 是否已分配角色（0：否，1：是）
     */
    private Integer hasRole;
    /**
     * 工号
     */
    private String userLogin;
    /**
     * 用户id
     */
    private Integer userId;
    /**
     * 昵称
     */
    private String userNicename;
    /**
     * 邮箱
     */
    private String userEmail;
    /**
     * 注册时间
     */
    private Date userRegistered;
    /**
     * 用户状态：1正常，0删除
     */
    private Integer userStatus;
    /**
     * 手机号
     */
    private String mobile;
    /**
     * 身份证号
     */
    private String idCard;
    /**
     * 姓名
     */
    private String realname;
    /**
     * 微信昵称
     */
    private String wxNickname;
    /**
     * 微信号
     */
    private String wxNumber;
    /**
     * 用户来源
     */
    private String source;
    /**
     * 是否绑定mfa 0未绑定 1 绑定
     */
    private Integer mfaBind;
    /**
     * 是否打开mfa 0关闭 1 打开
     */
    private Integer mfaOpen;
    private Integer userType;
    /**
     * 部门Id
     */
    private Integer deptId;
    
    private Integer roleId;


    
}
