package com.jds.link4sales.model.mapper.portray;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jds.link4sales.model.entity.portray.ComeDetail;

/**
 * <p>
 * 客户进线的渠道说明 Mapper 接口
 * </p>
 *
 * @author JACKSON G
 * @since 2020-12-01
 */
@DS("slave")
public interface ComeDetailMapper extends BaseMapper<ComeDetail> {

}
