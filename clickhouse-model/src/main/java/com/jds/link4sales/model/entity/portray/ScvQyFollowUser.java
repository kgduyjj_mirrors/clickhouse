package com.jds.link4sales.model.entity.portray;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author wzy
 * @since 2020-11-04
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ScvQyFollowUser extends Model<ScvQyFollowUser> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 企业号id
     */
    private Integer qyId;
    /**
     * 外部联系人的主键id
     */
    private Integer externalId;
    /**
     * 企业员工id
     */
    private String followUserId;
    /**
     * 企业成员对外部联系人的备注
     */
    private String remark;
    /**
     * 企业成员对此外部联系人的描述
     */
    private String description;
    /**
     * 企业成员添加此外部联系人的时间
     */
    private Integer createtime;
    /**
     * 该成员添加此外部联系人所打标签
     */
    private String tags;
    /**
     * 该成员对此客户备注的企业名称
     */
    private String remarkCorpName;
    /**
     * 该成员对此客户备注的手机号码
     */
    private String remarkMobiles;
    /**
     * 该成员添加此客户的渠道
     */
    private String state;
    /**
     * channelNames
     */
    private String channelNames;
    /**
     * 发起添加的userid
     */
    private String operUserid;
    /**
     * 成员添加此客户的来源
     */
    private Integer addWay;
    /**
     * 删除者id
     */
    private Integer deletedBy;
    /**
     * 创建时间
     */
    private Date createdAt;
    /**
     * 更新时间
     */
    private Date updatedAt;
    /**
     * 删除时间
     */
    private Date deletedAt;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
