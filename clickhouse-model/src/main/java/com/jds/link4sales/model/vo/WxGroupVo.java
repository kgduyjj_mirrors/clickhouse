package com.jds.link4sales.model.vo;

import lombok.Data;

import java.util.Date;

/**
 * wx团体签证官
 *
 * @author kun.gao
 * @date 2020/12/10
 */
@Data
public class WxGroupVo {

    private String roomName;

    private String roomHeadUrl;

    private String roomOwnerName;

    private String roomOwner;

    private Integer totalCount;

    private Date enterTime;

    private Integer tag;

    private String wxacc;


}
