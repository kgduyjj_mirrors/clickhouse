package com.jds.link4sales.model.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jds.link4sales.model.entity.sys.SysRole;

/**
 * <p>
 * sys_role  Mapper 接口
 * </p>
 *
 * @author JSCKSON G
 * @since 2020-11-03
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {

}
