package com.jds.link4sales.model.mapper.portray;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jds.link4sales.model.entity.portray.WechatGroupMember;

/**
 * <p>
 * 微信群信息详情表 Mapper 接口
 * </p>
 *
 * @author JACKSON G
 * @since 2020-12-10
 */
@DS("slave")
public interface WechatGroupMemberMapper extends BaseMapper<WechatGroupMember> {

}
