package com.jds.link4sales.model.mapper.portray;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jds.link4sales.model.entity.portray.ScvQyStaffCustommerRelation;

/**
 * <p>
 * 员工客户关联关系表 Mapper 接口
 * </p>
 *
 * @author JACKSON G
 * @since 2020-11-27
 */
@DS("slave")
public interface ScvQyStaffCustommerRelationMapper extends BaseMapper<ScvQyStaffCustommerRelation> {

}
