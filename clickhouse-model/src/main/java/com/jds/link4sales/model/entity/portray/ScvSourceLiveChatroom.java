package com.jds.link4sales.model.entity.portray;

import java.util.Date;
import java.io.Serializable;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 直播室
 * </p>
 *
 * @author JACKSON G
 * @since 2020-11-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ScvSourceLiveChatroom extends Model<ScvSourceLiveChatroom> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 名称
     */
    private String name;
    /**
     * 描述
     */
    private String description;
    /**
     * 内容类型：1视频2文字3视频加文字
     */
    private Integer contentType;
    /**
     * 日程类型：1日常直播2临时活动直播
     */
    private Integer calendarType;
    /**
     * 业务类型
     */
    private Integer bizType;
    /**
     * 老师
     */
    private String teacher;
    /**
     * 助理
     */
    private String assistant;
    /**
     * 0关闭1打开
     */
    private Integer isOpen;
    /**
     * 视频源
     */
    private Integer videoId;
    /**
     * 事业部
     */
    private Integer orgId;
    /**
     * 介绍图片
     */
    private String introImg;
    /**
     * 视频封面图片
     */
    private String videoImg;
    /**
     * 未开始图片
     */
    private String beforeImg;
    /**
     * 是否调整在线人数1是0否
     */
    private Integer isAdjustOnline;
    /**
     * 手动调整人数
     */
    private Integer manualAdjustNum;
    /**
     * 手动调整人数变化范围
     */
    private Integer manualAdjustRange;
    /**
     * 显示往期内容
     */
    private Integer showPastContent;
    /**
     * 环信roomId
     */
    private String hxRoomId;
    /**
     * 栏目标题
     */
    private String columnTitle;
    private Date createTime;
    /**
     * 是否视频直播1是0否
     */
    private Integer isLiveVideo;
    /**
     * 是否播放宣传视频1是0否
     */
    private Integer isXcVideo;
    /**
     * 是否播放录播视频1是0否
     */
    private Integer isLbVideo;
    /**
     * 宣传视频地址
     */
    private String xcVideo;
    /**
     * 录播视频地址
     */
    private String lbVideo;
    /**
     * 归属: 0其他 1博众
     */
    private Integer belong;
    /**
     * 是否是策略房间
     */
    private Integer isStrategy;
    /**
     * 主持人
     */
    private String compere;
    /**
     * 1-有效 0-归档
     */
    private Integer status;
    /**
     * 直播流状态1-推流 0-断流
     */
    private Integer flowStatus;
    /**
     * 禁止用户发言0否1是
     */
    private Integer noSpeak;
    /**
     * 先发后审1是0否
     */
    private Integer firstSend;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
