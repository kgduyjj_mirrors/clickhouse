package com.jds.link4sales.model.entity.elinks;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 资源链接表
 * </p>
 *
 * @author JACKSON G
 * @since 2021-01-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SourceLinkConfig extends Model<SourceLinkConfig> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * source_id
     */
    private String sourceId;
    /**
     * 类型：1-直播，2-支付 3自定义
     */
    private Integer sourceType;
    /**
     * 名称
     */
    private String roomName;
    /**
     * 资源链接
     */
    private String sourceUrl;
    /**
     * 服务号名称
     */
    private String fwhName;
    /**
     * 是否显示  1：显示  0：不显示
     */
    private Integer status;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 是否已经生成专属链接的配置
     */
    @TableField(exist = false)
    private transient  Integer  hasCreateExclusiveFlag;

    /**
     * 生成专属链接对应的url
     */
    @TableField(exist = false)
    private transient  String  sourceExclusiveUrl;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
