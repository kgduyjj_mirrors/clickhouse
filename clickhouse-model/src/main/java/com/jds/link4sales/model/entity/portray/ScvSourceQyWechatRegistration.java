package com.jds.link4sales.model.entity.portray;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 公司微信号登记表
 * </p>
 *
 * @author JACKSON G
 * @since 2021-01-29
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ScvSourceQyWechatRegistration extends Model<ScvSourceQyWechatRegistration> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 微信号
     */
    private String wxCode;
    /**
     * 0未知 1 老师号 2 助理号 3 互助号 4 员工工号 
     */
    private Integer wxType;
    /**
     * 设备id
     */
    private Integer deviceId;
    /**
     * 实名认证姓名
     */
    private String name;
    /**
     * 0 未封号 1 普通封号 2 永久封号
     */
    private Integer ban;
    /**
     * 员工工号
     */
    private String userId;
    /**
     * 更新时间
     */
    private Date updateTime;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 更新人
     */
    private String updateUser;
    /**
     * 0 未认证 1 已认证
     */
    private Integer certification;
    /**
     * 是否处于更换手机流程中 0-未处于 1-处于
     */
    private Integer changeDevice;
    /**
     * 手机号 字段aes加密值
     */
    private String phoneAes;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
