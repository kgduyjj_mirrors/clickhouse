package com.jds.link4sales.model.entity.portray;

import java.util.Date;
import java.io.Serializable;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author JACKSON G
 * @since 2020-11-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ScvTdcsUserBehavior extends Model<ScvTdcsUserBehavior> {

    private static final long serialVersionUID = 1L;

    /**
     * 用户id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 用户id
     */
    private String uid;
    /**
     * 唯一物理人id
     */
    private String oneId;
    /**
     * 注册时间
     */
    private Date registryTime;
    /**
     * 银行编号， 1-浦发，2-工行
     */
    private Integer accountBankType;
    /**
     * 操作类型，101-点击开户链接，102-完成短信验证，103-完成基本信息，104-完成黄金签约，105-首次入金时间，106-首次发起交易时间；201-首次申请工行开户，202-非工行卡风险测评，203-工行卡开户风险测评，204-工行卡一类户开户开黄金户，205-非工行卡开户开黄金户，206-黄金交易编码开户成功，207-绑定cas交易编码，208-工行电子账户开户申请，209工行影像上传
     */
    private Integer type;
    /**
     * 操作时间
     */
    private Date operateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
