package generator;

import java.sql.SQLException;

import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.rules.DbType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

/**
 * mybatis 自动生成器
 *
 * @author PZX
 * @date 2020/10/29
 */
public class MyBatisPlusGenerator {

    public static void main(String[] args) throws SQLException {

        //1. 全局配置
        GlobalConfig config = new GlobalConfig();
        // 是否支持AR模式
        config.setActiveRecord(true)
                .setAuthor("pzx")
                //.setOutputDir("D:\\workspace_mp\\mp03\\src\\main\\java")
                // 生成路径
                .setOutputDir("E:\\MyBatisPlusGenerator\\src\\main\\java")
                // 文件覆盖
                .setFileOverride(true)
                // 主键策略
                // 设置生成的service接口的名字的首字母是否为I
                .setServiceName("%sService")
                // IEmployeeService
                //生成基本的resultMap
                .setBaseResultMap(true)
                //生成基本的SQL片段
                .setBaseColumnList(true);

        //2. 数据源配置
        DataSourceConfig dsConfig = new DataSourceConfig();
        // 设置数据库类型
        dsConfig.setDbType(DbType.MYSQL)
                .setDriverName("com.mysql.jdbc.Driver")
                .setUrl("jdbc:localhost:3306/jince_data_platform")
                .setUsername("root")
                .setPassword("123456");

        //3. 策略配置globalConfiguration中
        StrategyConfig stConfig = new StrategyConfig();
        //全局大写命名
        stConfig.setCapitalMode(true)
                // 设置lombok模型
                .setEntityLombokModel(true)
                // 指定表名 字段名是否使用下划线
                .setDbColumnUnderline(true)
                // 数据库表映射到实体的命名策略
                .setNaming(NamingStrategy.underline_to_camel)
                //.setTablePrefix("tb_")
                // 生成的表
                .setInclude("scv_financial_order_info","scv_financial_employee_customer_qy_relation");

        //4. 包名策略配置
        PackageConfig pkConfig = new PackageConfig();
        pkConfig.setParent("com.jds.link4sales.model")
                //dao
                .setMapper("mapper.portray")
                //servcie
                .setService("service.portray")
                //controller
                .setController("controller.portray")
                .setEntity("entity.portray")
                //mapper.xml
                .setXml("mapper.portray");
        //5. 整合配置
        AutoGenerator ag = new AutoGenerator();
        ag.setGlobalConfig(config)
                .setDataSource(dsConfig)
                .setStrategy(stConfig)
                .setPackageInfo(pkConfig);
        //6. 执行
        ag.execute();
    }

}