package com.jds.clickhouse.web.service.impl.portray;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jds.clickhouse.web.service.portray.ScvSourceGpConsumeOrderInfoService;
import com.jds.link4sales.common.result.ResultJson;
import com.jds.link4sales.model.dto.ActionsDto;
import com.jds.link4sales.model.entity.portray.ScvSourceGpConsumeOrderInfo;
import com.jds.link4sales.model.mapper.portray.ScvSourceGpConsumeOrderInfoMapper;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author JACKSON G
 * @since 2021-01-07
 */
@Service
public class ScvSourceGpConsumeOrderInfoServiceImpl extends ServiceImpl<ScvSourceGpConsumeOrderInfoMapper, ScvSourceGpConsumeOrderInfo> implements ScvSourceGpConsumeOrderInfoService {

    @Override
    public ResultJson getGpOrderInfo(ActionsDto actionsDto) {
        PageHelper.startPage(actionsDto.getPageNum(), actionsDto.getPageSize());

        LambdaQueryWrapper<ScvSourceGpConsumeOrderInfo> lambdaQueryWrapper= new LambdaQueryWrapper<ScvSourceGpConsumeOrderInfo>();
        lambdaQueryWrapper.in(ScvSourceGpConsumeOrderInfo::getOneId,actionsDto.getOneIdList());

        //开始时间
        if(actionsDto.getStartTime()!=null){
            lambdaQueryWrapper.ge(ScvSourceGpConsumeOrderInfo::getPaidTime,actionsDto.getStartTime());
        }
        //结束时间
        if(actionsDto.getStopTime()!=null){
            lambdaQueryWrapper.le(ScvSourceGpConsumeOrderInfo::getPaidTime,actionsDto.getStopTime());
        }
        lambdaQueryWrapper.orderByDesc(ScvSourceGpConsumeOrderInfo::getPaidTime);
        List list= this.list(lambdaQueryWrapper);
        return ResultJson.success(new PageInfo<>(list));
    }
}
