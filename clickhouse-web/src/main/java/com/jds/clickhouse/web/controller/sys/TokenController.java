package com.jds.clickhouse.web.controller.sys;

import com.jds.link4sales.common.constant.Constants;
import com.jds.link4sales.model.vo.VerifyUpcTokenVo;
import com.jds.clickhouse.web.service.sys.SysMenuService;
import com.jds.clickhouse.web.service.sys.SysUserRoleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

/**
 * 提供给upc认证后回调
 *
 * @author jackson G
 * @date 2020/10/29
 */
@RestController
//@RequestMapping("/")
@Slf4j
public class TokenController {

    @Autowired
    private SysUserRoleService sysUserRoleService;

    @Autowired
    private SysMenuService sysMenuService;

    /**
     *  提供给upc认证后回调
     * @param 
     * @return 
     */
    @PostMapping(value = "/setToken")
    public VerifyUpcTokenVo setToken(HttpServletResponse response, @RequestParam(value = "token", required = true) String token) {
        Cookie cookie = new Cookie(Constants.COOKIE_TOKEN, token);
        cookie.setPath("/");
        log.info("ticket:{}", token);
        response.addCookie(cookie);
        return VerifyUpcTokenVo.SUCCESS;
    }

}
