package com.jds.clickhouse.web.controller.sys;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jds.link4sales.common.annotation.Login;
import com.jds.link4sales.common.result.ResultJson;
import com.jds.link4sales.model.entity.sys.SysDataAuth;
import com.jds.clickhouse.web.service.sys.SysDataAuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zhixiang.peng
 * @since 2020-11-12
 */
@RestController
@RequestMapping("/web/sysDataAuth")
@Login
public class SysDataAuthController {


    @Autowired
    private SysDataAuthService sysDataAuthService;

    /**
     * 设置功能的数据权限
     *
     * @param sysDataAuthList
     * @return ResultJson
     */
    @PostMapping("/setDataAuth")
    public ResultJson setDataAuth(@Valid @RequestBody List<SysDataAuth> sysDataAuthList) {
        sysDataAuthService.setDataAuth(sysDataAuthList);
        return ResultJson.success();
    }

    /**
     * 查询对应角色和功能的数据权限
     * @return
     */
    @PostMapping("/listByRoleAndFunctionId")
    public ResultJson list(@Valid @RequestBody List<SysDataAuth> sysDataAuthList) {
        List<SysDataAuth> resultList= new ArrayList<>();
        sysDataAuthList.forEach(item->{
            SysDataAuth  result=    sysDataAuthService.getOne(new QueryWrapper<SysDataAuth>().eq("role_id",item.getRoleId()).eq("function_id",item.getFunctionId()).last("limit 1"));
            resultList.add(result);

        });
        return ResultJson.success(resultList);
    }
}

