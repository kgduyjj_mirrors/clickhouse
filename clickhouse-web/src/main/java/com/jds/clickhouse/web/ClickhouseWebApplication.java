package com.jds.clickhouse.web;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * link4sales web应用程序
 *
 * @author kun.gao
 * @date 2020/10/29
 */
@ComponentScan({"com.jds.link4sales"})
@SpringBootApplication(exclude = DruidDataSourceAutoConfigure.class)
@MapperScan({"com.jds.link4sales.model.mapper"})
public class ClickhouseWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(ClickhouseWebApplication.class, args);
    }

}
