package com.jds.clickhouse.web.service.portray;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jds.link4sales.common.result.ResultJson;
import com.jds.link4sales.model.dto.ActionsDto;
import com.jds.link4sales.model.entity.portray.ScvTdcsUserBehavior;
import com.jds.link4sales.model.vo.ScvTdcsUserBehaviorVo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author JACKSON G
 * @since 2020-11-09
 */
public interface ScvTdcsUserBehaviorService extends IService<ScvTdcsUserBehavior> {

    /**
     * 动作列表查询开账户
     *
     * @param actionsDto 行动dto
     * @return {@link List < ScvTdcsUserBehaviorVo >}
     */
    ResultJson<Object> queryOpenAccountActionList(ActionsDto actionsDto);


    /**
     * 动作列表查询开账户（微信）
     *
     * @param actionsDto 行动dto
     * @return {@link List < ScvTdcsUserBehaviorVo >}
     */
    ResultJson<Object> queryWxOpenAccountActionList(ActionsDto actionsDto);

    /**
     * 动作列表查询开账户
     *
     * @param actionsDto 行动dto
     * @return {@link List < ScvTdcsUserBehaviorVo >}
     */
    ResultJson<Object> queryUserOpenAccountActionList(ActionsDto actionsDto);
    
    ResultJson<Object> queryWxUserOpenAccountActionList(ActionsDto actionsDto);

    /**
     * 根据用户id获取企业id一览
     *
     * @param userId  用户id
     * @return {@link List < ScvTdcsUserBehaviorVo >}
     */
    List<Integer>   queryQyIdByUserId(String userId);
}
