package com.jds.clickhouse.web.service.impl.sys;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jds.link4sales.model.entity.sys.SysRoleGroup;
import com.jds.link4sales.model.mapper.sys.SysRoleGroupMapper;
import com.jds.clickhouse.web.service.sys.SysRoleGroupService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  SysRoleGroup服务实现类
 * </p>
 *
 * @author JSCKSON G
 * @since 2020-11-03
 */
@Service
public class SysRoleGroupServiceImpl extends ServiceImpl<SysRoleGroupMapper, SysRoleGroup> implements SysRoleGroupService {

}
