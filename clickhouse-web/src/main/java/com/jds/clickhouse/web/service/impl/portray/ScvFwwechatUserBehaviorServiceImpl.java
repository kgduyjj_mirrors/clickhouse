package com.jds.clickhouse.web.service.impl.portray;

import java.util.List;
import java.util.stream.Collectors;

import com.jds.clickhouse.web.service.portray.CustomerWechatDetailService;
import com.jds.clickhouse.web.service.portray.ScvFwwechatUserBehaviorService;
import com.jds.clickhouse.web.service.portray.ScvQyExternalUserService;
import com.jds.clickhouse.web.service.portray.ScvSourceQyQyUsersService;
import com.jds.link4sales.model.entity.portray.CustomerWechatDetail;
import com.jds.link4sales.model.vo.ScvWxFwwechatUserBehaviorVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jds.link4sales.common.result.ResultJson;
import com.jds.link4sales.common.utils.AesUtils;
import com.jds.link4sales.model.dto.ActionsDto;
import com.jds.link4sales.model.entity.portray.ScvFwwechatUserBehavior;
import com.jds.link4sales.model.entity.portray.ScvQyExternalUser;
import com.jds.link4sales.model.entity.portray.ScvSourceQyQyUsers;
import com.jds.link4sales.model.entity.sys.SysUser;
import com.jds.link4sales.model.mapper.portray.ScvFwwechatUserBehaviorMapper;
import com.jds.link4sales.model.vo.ScvFwwechatUserBehaviorVo;
import com.jds.clickhouse.web.service.sys.SysUserService;

import lombok.RequiredArgsConstructor;
import org.springframework.util.CollectionUtils;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author JACKSON G
 * @since 2020-11-06
 */
@Service
@RequiredArgsConstructor
public class ScvFwwechatUserBehaviorServiceImpl extends
        ServiceImpl<ScvFwwechatUserBehaviorMapper, ScvFwwechatUserBehavior> implements ScvFwwechatUserBehaviorService {

    private final ScvFwwechatUserBehaviorMapper scvFwwechatUserBehaviorMapper;

    private final ScvQyExternalUserService scvQyExternalUserService;

    private final ScvSourceQyQyUsersService scvSourceQyQyUsersService;

    private final SysUserService sysUserService;

    private final CustomerWechatDetailService customerWechatDetailService;

    @Value("${aes.key}")
    private String secretKey;

    @Override
    public ResultJson<Object> queryFwBehaviorList(ActionsDto actionsDto) {
        PageHelper.startPage(actionsDto.getPageNum(), actionsDto.getPageSize());
        List<ScvFwwechatUserBehaviorVo> scvFwwechatUserBehaviorVos = scvFwwechatUserBehaviorMapper
                .queryFwBehaviorList(actionsDto);
        if (scvFwwechatUserBehaviorVos != null && scvFwwechatUserBehaviorVos.size() > 0) {
            List<Integer> collect = scvFwwechatUserBehaviorVos.stream().map(ScvFwwechatUserBehaviorVo::getExId)
                    .collect(Collectors.toList());
            List<ScvQyExternalUser> scvQyExternalUsers = scvQyExternalUserService.listByExternalId(collect);
            if (scvQyExternalUsers != null && scvQyExternalUsers.size() > 0) {
                scvFwwechatUserBehaviorVos.forEach(scvFwwechatUserBehaviorVo -> {
                    scvQyExternalUsers.forEach(scvQyExternalUser -> {
                        if (scvFwwechatUserBehaviorVo.getExId().equals(scvQyExternalUser.getId())) {
                            scvFwwechatUserBehaviorVo.setAvatar(scvQyExternalUser.getAvatar());
                            scvFwwechatUserBehaviorVo.setNickName(scvQyExternalUser.getName());
                        }
                    });
                });
            }
            List<String> followerIdList = scvFwwechatUserBehaviorVos.stream()
                    .map(ScvFwwechatUserBehaviorVo::getFollowUserId).collect(Collectors.toList());
            List<ScvSourceQyQyUsers> list = scvSourceQyQyUsersService.list(
                    new LambdaQueryWrapper<ScvSourceQyQyUsers>().eq(ScvSourceQyQyUsers::getQyId, actionsDto.getQyId())
                            .in(ScvSourceQyQyUsers::getUserid, followerIdList));
            scvFwwechatUserBehaviorVos.forEach(scvFwwechatUserBehaviorVo -> {
                list.forEach(scvSourceQyQyUsers -> {
                    if (scvSourceQyQyUsers.getUserid().equals(scvFwwechatUserBehaviorVo.getFollowUserId())) {
                        scvFwwechatUserBehaviorVo.setFollowUserName(scvSourceQyQyUsers.getName());
                        if (StringUtils.isNotBlank(scvSourceQyQyUsers.getMobile())) {
                            scvFwwechatUserBehaviorVo
                                    .setMobile(AesUtils.desEncryptTerminal(scvSourceQyQyUsers.getMobile()));
                        }
                    }
                });
            });
            List<String> userCodeList = scvFwwechatUserBehaviorVos.stream().map(ScvFwwechatUserBehaviorVo::getUserCode)
                    .collect(Collectors.toList());
            List<SysUser> list1 = sysUserService
                    .list(new LambdaQueryWrapper<SysUser>().in(SysUser::getUserLogin, userCodeList));
            scvFwwechatUserBehaviorVos.forEach(scvFwwechatUserBehaviorVo -> {
                list1.forEach(sysUser -> {
                    if (scvFwwechatUserBehaviorVo.getUserCode().equals(sysUser.getUserLogin())) {
                        scvFwwechatUserBehaviorVo.setName(sysUser.getRealname());
                    }
                });
            });
        }
        return ResultJson.success(new PageInfo<>(scvFwwechatUserBehaviorVos));
    }

    /**
     * 服务号行为数据 （微信）
     * @param actionsDto actionsDto
     * @return  ResultJson ResultJson
     */
    @Override
    public ResultJson<Object> queryWxFwBehaviorList(ActionsDto actionsDto) {

        PageHelper.startPage(actionsDto.getPageNum(), actionsDto.getPageSize());

        //获取服务号数据（微信）
        List<ScvWxFwwechatUserBehaviorVo> scvFwwechatUserBehaviorVos = scvFwwechatUserBehaviorMapper
                .queryWxFwBehaviorList(actionsDto);

        if (!CollectionUtils.isEmpty(scvFwwechatUserBehaviorVos)) {

            //通过customerWxid获取客户的信息赋值到列表vo
            List<String> collectCustomerWxid = scvFwwechatUserBehaviorVos.stream().map(ScvWxFwwechatUserBehaviorVo::getCustomerWxid)
                    .collect(Collectors.toList());
            //通过customerWxid集合获取customer_wechat_detail客户微信表的详情信息
            List<CustomerWechatDetail> customerWechatDetailUsers = customerWechatDetailService.listByCustomerWxid(collectCustomerWxid);

            if (!CollectionUtils.isEmpty(customerWechatDetailUsers)) {
                scvFwwechatUserBehaviorVos.forEach(scvFwwechatUserBehaviorVo -> {
                    customerWechatDetailUsers.forEach(customerWechatDetailUser -> {
                        if (scvFwwechatUserBehaviorVo.getCustomerWxid().equals(customerWechatDetailUser.getCustomerWxid())) {
                            scvFwwechatUserBehaviorVo.setAvatar(customerWechatDetailUser.getWechatHeadUrl());
                            scvFwwechatUserBehaviorVo.setNickName(customerWechatDetailUser.getNickname());
                        }
                    });
                });
            }

            //获取微信号-company_wxid在customer_wechat_detail中关联user_id然后获取wechat_wxacc
            List<String> collectCompanyWxid = scvFwwechatUserBehaviorVos.stream().map(ScvWxFwwechatUserBehaviorVo::getCompanyWxid)
                    .collect(Collectors.toList());

            //通过customerWxid集合获取customer_wechat_detail客户微信表的详情信息
            List<CustomerWechatDetail> companyCustomerWechatDetailUsers = customerWechatDetailService.listByCustomerWxid(collectCompanyWxid);
            //所属微信号赋值
            scvFwwechatUserBehaviorVos.forEach(scvFwwechatUserBehaviorVo -> {
                companyCustomerWechatDetailUsers.forEach(companyCustomerWechatDetailUser -> {
                    if (scvFwwechatUserBehaviorVo.getCompanyWxid().equals(companyCustomerWechatDetailUser.getCustomerWxid())) {
                        if (StringUtils.isNotBlank(companyCustomerWechatDetailUser.getWechatWxacc())) {
                            scvFwwechatUserBehaviorVo.setBelongWxid( AesUtils.decrypt(companyCustomerWechatDetailUser.getWechatWxacc(), secretKey));
                        }
                    }
                });
            });
//            //员工信息赋值
//            List<String> userCodeList = scvFwwechatUserBehaviorVos.stream().map(ScvWxFwwechatUserBehaviorVo::getEmployeeId)
//                    .collect(Collectors.toList());
//            List<SysUser> list1 = sysUserService
//                    .list(new LambdaQueryWrapper<SysUser>().in(SysUser::getUserLogin, userCodeList));
//            scvFwwechatUserBehaviorVos.forEach(scvFwwechatUserBehaviorVo -> {
//                list1.forEach(sysUser -> {
//                    if (scvFwwechatUserBehaviorVo.getEmployeeId().equals(sysUser.getUserLogin())) {
//                        scvFwwechatUserBehaviorVo.setName(sysUser.getRealname());
//                    }
//                });
//            });
        }
        return ResultJson.success(new PageInfo<>(scvFwwechatUserBehaviorVos));

    }


    @Override
    public ResultJson<Object> queryUserFwBehaviorList(ActionsDto actionsDto) {
        PageHelper.startPage(actionsDto.getPageNum(), actionsDto.getPageSize());
        List<ScvFwwechatUserBehaviorVo> scvFwwechatUserBehaviorVos = scvFwwechatUserBehaviorMapper
                .queryUserFwBehaviorList(actionsDto);
        return ResultJson.success(new PageInfo<>(scvFwwechatUserBehaviorVos));
    }
    
    @Override
    public ResultJson<Object> queryWxUserFwBehaviorList(ActionsDto actionsDto) {
        PageHelper.startPage(actionsDto.getPageNum(), actionsDto.getPageSize());
        List<ScvFwwechatUserBehaviorVo> scvFwwechatUserBehaviorVos = scvFwwechatUserBehaviorMapper
                .queryWxUserFwBehaviorList(actionsDto);
        return ResultJson.success(new PageInfo<>(scvFwwechatUserBehaviorVos));
    }
    
}
