package com.jds.clickhouse.web.service.impl.portray;
import com.jds.clickhouse.web.service.portray.ScvQyScopeFollowUserService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jds.link4sales.model.entity.portray.ScvQyScopeFollowUser;
import com.jds.link4sales.model.mapper.portray.ScvQyScopeFollowUserMapper;

/**
 * <p>
 * 配置了客户联系功能的企业员工表 服务实现类
 * </p>
 *
 * @author wzy
 * @since 2020-11-04
 */
@Service
public class ScvQyScopeFollowUserServiceImpl extends ServiceImpl<ScvQyScopeFollowUserMapper, ScvQyScopeFollowUser> implements ScvQyScopeFollowUserService {

}
