package com.jds.clickhouse.web.service.portray;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jds.link4sales.common.result.ResultJson;
import com.jds.link4sales.model.dto.ActionsDto;
import com.jds.link4sales.model.dto.WechatUserPortraitDto;
import com.jds.link4sales.model.entity.portray.WechatUserPortrait;
import com.jds.link4sales.model.vo.EmployeeAdditionVo;

/**
 * wx用户画像service
 *
 * @author Jackson
 * @date 2020/10/19
 */
public interface WxUserPortraitService extends IService<WechatUserPortrait> {

    /**
     * 获取wx用户动态
     *
     * @param actionsDto 微信用户画像
     * @return {@link ResultJson}
     */
    ResultJson<Object> getWebActionsList(ActionsDto actionsDto);

    /**
     * 获取wx用户动态
     *
     * @param actionsDto 微信用户画像
     * @return {@link ResultJson}
     */
    ResultJson<Object> getAppUserActions(ActionsDto actionsDto);

    /**
     * 得到wx web用户头信息
     *
     * @param customerWxId 客户wx id
     * @param companyWxId  公司wx id
     * @return {@link ResultJson<Object>}
     */
    ResultJson<Object> getWxUserHeaInfo(String companyWxId,String customerWxId);


    /**
     * 得到天气朋友
     * 得到微信好友
     *
     * @param customerWxId 客户wx id
     * @param pageNum      页面num
     * @param pageSize     页面大小
     * @return {@link ResultJson<Object>}
     */
    ResultJson<Object> getWxBuddy(String customerWxId,Integer  pageNum,Integer pageSize);


    /**
     * 得到wx组信息
     * 得到wx群信息
     *
     * @param customerWxId 客户wx id
     * @param pageNum      页面num
     * @param pageSize     页面大小
     * @param companyWxid  公司wxid
     * @param type         类型
     * @return {@link ResultJson<Object>}
     */
    ResultJson<Object> getWxGroupsInfo(String customerWxId,String companyWxid,Integer  pageNum,Integer pageSize,Integer type);


    /**
     * 更新添加
     *
     * @param employeeAdditionVo 员工除了签证官
     * @return {@link ResultJson<Object>}
     */
    ResultJson<Object> updateAddition(EmployeeAdditionVo employeeAdditionVo);


    /**
     * 获取微信ID
     *
     * @param str str
     * @return {@link String}
     */
    String getWxId(String str);


}