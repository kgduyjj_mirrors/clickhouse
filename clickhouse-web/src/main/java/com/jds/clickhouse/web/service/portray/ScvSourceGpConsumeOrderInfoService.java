package com.jds.clickhouse.web.service.portray;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jds.link4sales.common.result.ResultJson;
import com.jds.link4sales.model.dto.ActionsDto;
import com.jds.link4sales.model.entity.portray.ScvSourceGpConsumeOrderInfo;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author JACKSON G
 * @since 2021-01-07
 */
public interface ScvSourceGpConsumeOrderInfoService extends IService<ScvSourceGpConsumeOrderInfo> {


    /**
     * 获取股拍订单
     *
     * @param actionsDto
     * @return ResultJson
     */
    ResultJson getGpOrderInfo(ActionsDto actionsDto);

}
