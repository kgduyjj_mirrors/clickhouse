package com.jds.clickhouse.web.service.portray;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jds.link4sales.model.entity.portray.ScvFinancialUserInfo;

/**
 * <p>
 * 财商课用户基本信息 服务类
 * </p>
 *
 * @author wzy
 * @since 2020-12-16
 */
public interface ScvFinancialUserInfoService extends IService<ScvFinancialUserInfo> {

}
