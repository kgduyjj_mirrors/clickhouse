package com.jds.clickhouse.web.service.sys;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jds.link4sales.model.dto.SysUserListDto;
import com.jds.link4sales.model.vo.SysUserListVo;
import com.jds.link4sales.model.entity.sys.SysUser;

public interface SysUserService extends IService<SysUser> {

    List<SysUserListVo> list(SysUserListDto dto);

    /**
     *  根据员工工号获取同部门下所有员工的工号
     * @param userId  员工工号
     * @return
     */
    List<SysUser> selectSameDeptUserList(String userId);

    /**
     *  根据员工号获取员工详情
     * @param  userLogin 员工工号
     * @return 
     */
    SysUserListVo selectUserInfoByUserLogin(String  userLogin);
    
}
