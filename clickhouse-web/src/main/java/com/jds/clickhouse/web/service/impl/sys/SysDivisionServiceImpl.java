package com.jds.clickhouse.web.service.impl.sys;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jds.link4sales.model.entity.sys.SysDivision;
import com.jds.link4sales.model.mapper.sys.SysDivisionMapper;
import com.jds.clickhouse.web.service.sys.SysDivisionService;

/**
 * <p>
 * 事业部表 服务实现类
 * </p>
 *
 * @author wzy
 * @since 2020-11-03
 */
@Service
public class SysDivisionServiceImpl extends ServiceImpl<SysDivisionMapper, SysDivision> implements SysDivisionService {

}
