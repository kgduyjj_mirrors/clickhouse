package com.jds.clickhouse.web.service.portray;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jds.link4sales.model.dto.ScvQyExternalUserListDto;
import com.jds.link4sales.model.vo.ScvQyExternalUserListVo;
import com.jds.link4sales.model.entity.portray.ScvQyExternalUser;

/**
 * <p>
 * 企业外部联系人表 服务类
 * </p>
 *
 * @author wzy
 * @since 2020-11-04
 */
public interface ScvQyExternalUserService extends IService<ScvQyExternalUser> {

    /**
     * 列表
     *
     * @param scvQyExternalUserListQueryDto scv qy外部用户列表查询dto
     * @return {@link List<ScvQyExternalUserListVo>}
     */
    List<ScvQyExternalUserListVo> list(ScvQyExternalUserListDto scvQyExternalUserListQueryDto);

    /**
     * 通过外部id列表
     *
     * @param idList id列表
     * @return {@link List<ScvQyExternalUser>}
     */
    List<ScvQyExternalUser> listByExternalId(List<Integer> idList);

}
