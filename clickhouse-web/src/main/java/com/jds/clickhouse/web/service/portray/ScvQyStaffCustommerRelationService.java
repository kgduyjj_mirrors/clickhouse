package com.jds.clickhouse.web.service.portray;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jds.link4sales.model.entity.portray.ScvQyStaffCustommerRelation;

/**
 * <p>
 * 员工客户关联关系表 服务类
 * </p>
 *
 * @author JACKSON G
 * @since 2020-11-27
 */
public interface ScvQyStaffCustommerRelationService extends IService<ScvQyStaffCustommerRelation> {

}
