package com.jds.clickhouse.web.controller.portray;

import javax.servlet.http.HttpServletRequest;

import com.jds.link4sales.common.annotation.NoCheckUrlAuth;
import com.jds.link4sales.common.constant.QueryActionsConstants;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jds.link4sales.common.annotation.Login;
import com.jds.link4sales.common.exception.BusinessException;
import com.jds.link4sales.common.result.ResultJson;
import com.jds.link4sales.model.dto.ActionsDto;
import com.jds.link4sales.model.dto.WechatUserPortraitDto;
import com.jds.clickhouse.web.service.portray.WechatUserPortraitService;

import cn.hutool.core.lang.Assert;
import lombok.RequiredArgsConstructor;

/**
 * 微信用户画像控制器
 *
 * @author JACKSON G
 * @date 2020/11/04
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/web/wechat/user/portrait")
@Login
@NoCheckUrlAuth
public class WechatUserPortraitController {

    private final WechatUserPortraitService userPortraitService;

    @GetMapping("/head/info")
    public ResultJson<Object> getHeadInfo(Integer exId, Integer qyId, String followId) {
        Assert.notBlank(followId, "员工企业id不能为空");
        if (qyId == null) {
            throw new BusinessException("qyid不能为空");
        }
        if (exId == null) {
            throw new BusinessException("exId不能为空");
        }
        return userPortraitService.getWebUserHeaInfo(exId, followId, qyId);
    }

    @PostMapping("/update")
    public ResultJson<Object> saveOrUpdate(@RequestBody WechatUserPortraitDto wechatUserPortraitDto,
            HttpServletRequest request) {
        Assert.notNull(wechatUserPortraitDto, "参数不能为空");
        Assert.notNull(wechatUserPortraitDto.getExId(), "联系人id不能为空");
        Assert.notNull(wechatUserPortraitDto.getQyId(), "企业id不能为空");
        Assert.notBlank(wechatUserPortraitDto.getFollowId(), "员工企业id不能为空");
        return userPortraitService.saveOrUpdateWeb(wechatUserPortraitDto);
    }

    @PostMapping("/getBuddy")
    public ResultJson<Object> getBuddy(@RequestBody WechatUserPortraitDto wechatUserPortraitDto) {
        Assert.notNull(wechatUserPortraitDto, "参数不能为空");
        Assert.notNull(wechatUserPortraitDto.getExId(), "联系人id不能为空");
        Assert.notNull(wechatUserPortraitDto.getQyId(), "企业id不能为空");
        Assert.notBlank(wechatUserPortraitDto.getFollowId(), "员工企业id不能为空");
        return userPortraitService.getWebBuddy(wechatUserPortraitDto);
    }

    @PostMapping("/getGroups")
    public ResultJson<Object> getGroups(@RequestBody WechatUserPortraitDto wechatUserPortraitDto,
            HttpServletRequest request) {
        Assert.notNull(wechatUserPortraitDto, "参数不能为空");
        Assert.notNull(wechatUserPortraitDto.getExId(), "联系人id不能为空");
        Assert.notNull(wechatUserPortraitDto.getQyId(), "企业id不能为空");
        Assert.notBlank(wechatUserPortraitDto.getFollowId(), "员工企业id不能为空");
        return userPortraitService.getWebGroupsInfo(wechatUserPortraitDto);
    }

    @PostMapping("/getActions")
    public ResultJson<Object> getWebActionsList(@RequestBody ActionsDto actionsDto, HttpServletRequest request) {
        Assert.notNull(actionsDto, "参数不能为空");
        return userPortraitService.getWebActionsList(actionsDto);
    }

    @PostMapping("/mine/getActions")
    public ResultJson<Object> getMyWebActionsList(@RequestBody ActionsDto actionsDto, HttpServletRequest request) {
        Assert.notNull(actionsDto, "参数不能为空");
        return userPortraitService.getWebActionsList(actionsDto);
    }

    @PostMapping("/action/details")
    public ResultJson<Object> getActionDetailsList(@RequestBody ActionsDto actionsDto, HttpServletRequest request) {
        Assert.notNull(actionsDto, "参数不能为空");
        //股拍订单读取公共订单，check oneId
        if(actionsDto.getQueryType() == QueryActionsConstants.GP_ORDER){
            Assert.notNull(actionsDto.getOneId(), "oneId不能为空");
        }else{
            Assert.notNull(actionsDto.getExId(), "联系人id不能为空");
        }
        return userPortraitService.getAppUserActions(actionsDto);
    }

}
