package com.jds.clickhouse.web.service.impl.portray;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jds.clickhouse.web.service.portray.ComeDetailService;
import com.jds.link4sales.model.entity.portray.ComeDetail;
import com.jds.link4sales.model.mapper.portray.ComeDetailMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * <p>
 * 客户进线的渠道说明 服务实现类
 * </p>
 *
 * @author JACKSON G
 * @since 2020-12-01
 */
@Service
public class ComeDetailServiceImpl extends ServiceImpl<ComeDetailMapper, ComeDetail> implements ComeDetailService {

    @Autowired
    private ComeDetailMapper comeDetailMapper;


    @Override
    public List<ComeDetail> listByCustomerWxid(List<String> customerWxidList) {

        if (!CollectionUtils.isEmpty(customerWxidList)) {
            //查询客户的进线情况 单个客户可能会有多条
            List<ComeDetail> list = comeDetailMapper.selectList(new LambdaQueryWrapper<ComeDetail>().in(ComeDetail::getCustomerWxid, customerWxidList).orderByDesc(ComeDetail::getId));

            if (CollectionUtils.isEmpty(list)) {
                return list;
            }
            //客户id的set
            Set<String> customerWxidSet = new HashSet<>();

            //去重去最大id的ComeDetail
            List<ComeDetail> result = new ArrayList<>();
            list.forEach(item -> {
                if (!customerWxidSet.contains(item.getCustomerWxid())) {
                    customerWxidSet.add(item.getCustomerWxid());
                    result.add(item);
                }
            });
            return result;
        }
        return null;
    }
}
