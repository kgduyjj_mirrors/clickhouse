package com.jds.clickhouse.web.service.impl.portray;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jds.clickhouse.web.service.portray.ScvUserGraphService;
import com.jds.link4sales.model.entity.portray.ScvUserGraph;
import com.jds.link4sales.model.mapper.portray.ScvUserGraphMapper;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author JACKSON G
 * @since 2020-11-06
 */
@Service
public class ScvUserGraphServiceImpl extends ServiceImpl<ScvUserGraphMapper, ScvUserGraph> implements ScvUserGraphService {

}
