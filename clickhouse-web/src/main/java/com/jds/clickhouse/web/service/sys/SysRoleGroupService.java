package com.jds.clickhouse.web.service.sys;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jds.link4sales.model.entity.sys.SysRoleGroup;

/**
 * <p>
 *  SysRoleGroup服务类
 * </p>
 *
 * @author JSCKSON G
 * @since 2020-11-03
 */
public interface SysRoleGroupService extends IService<SysRoleGroup> {

}
