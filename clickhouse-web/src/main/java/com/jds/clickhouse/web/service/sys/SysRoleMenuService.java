package com.jds.clickhouse.web.service.sys;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jds.link4sales.model.dto.RoleMenuDto;
import com.jds.link4sales.model.dto.UserRoleDto;
import com.jds.link4sales.model.entity.sys.SysRoleMenu;
import com.jds.link4sales.model.entity.sys.SysUserRole;

import java.util.List;

/**
 * <p>
 * SysRoleMenu  服务类
 * </p>
 *
 * @author zhixiang.peng
 * @since 2020-11-03
 */
public interface SysRoleMenuService extends IService<SysRoleMenu> {

    /**
     *  保存角色菜单关系
     * @param  roleMenuDto roleMenuDto
     * @return  void
     */
   public void  saveOrUpdateRoleMenu(RoleMenuDto roleMenuDto);

}
