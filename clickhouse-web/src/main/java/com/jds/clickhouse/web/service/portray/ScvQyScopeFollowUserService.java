package com.jds.clickhouse.web.service.portray;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jds.link4sales.model.entity.portray.ScvQyScopeFollowUser;

/**
 * <p>
 * 配置了客户联系功能的企业员工表 服务类
 * </p>
 *
 * @author wzy
 * @since 2020-11-04
 */
public interface ScvQyScopeFollowUserService extends IService<ScvQyScopeFollowUser> {

}
