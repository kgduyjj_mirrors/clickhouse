package com.jds.clickhouse.web.service.impl.portray;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jds.clickhouse.web.service.portray.WxUserPortraitDetailService;
import com.jds.link4sales.common.result.ResultJson;
import com.jds.link4sales.model.dto.ScvQyExternalUserListDto;
import com.jds.link4sales.model.entity.portray.FollowWxRelation;
import com.jds.link4sales.model.entity.portray.QySecretMapping;
import com.jds.link4sales.model.mapper.portray.FollowWxRelationMapper;
import com.jds.link4sales.model.mapper.portray.QySecretMappingMapper;
import com.jds.link4sales.model.mapper.portray.ScvQyExternalUserMapper;
import com.jds.link4sales.model.mapper.sys.SysUserMapper;
import com.jds.link4sales.model.vo.ScvQyExternalUserListVo;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * wx用户画像服务impl
 *
 * @author cheng.zhang
 * @date 2020/11/26
 */
@Service
@RequiredArgsConstructor
public class WxUserPortraitDetailServiceImpl extends ServiceImpl<FollowWxRelationMapper, FollowWxRelation> implements WxUserPortraitDetailService {

    private final FollowWxRelationMapper followWxRelationMapper;
    private final ScvQyExternalUserMapper scvQyExternalUserMapper;
    private final SysUserMapper sysUserMapper;
    private final QySecretMappingMapper qySecretMappingMapper;


    /**
     * 关联外部联系人-获取客户列表
     *
     * @param dto
     * @return
     */
    @Override
    public ResultJson<?> getCustomerList(ScvQyExternalUserListDto dto) {
        String userTag = dto.getUserTag();
        if (StringUtils.isNotBlank(userTag)) {
            List<String> selectUserLoginList = sysUserMapper.selectUserLoginList(dto.getUserTag());
            dto.setUserLoginList(selectUserLoginList);
        }
        PageHelper.startPage(dto.getPageNum(), dto.getPageSize());
        List<ScvQyExternalUserListVo> list = scvQyExternalUserMapper.list(dto);
        list.forEach(scvQyExternalUserListVo -> {
            FollowWxRelation one = followWxRelationMapper.selectOne(new QueryWrapper<FollowWxRelation>()
                    .eq("external_id", scvQyExternalUserListVo.getExId())
                    .eq("wx_id", dto.getCustomerWxId()));
            if (null != one) {
                scvQyExternalUserListVo.setIsBind(1);
            } else {
                scvQyExternalUserListVo.setIsBind(0);
            }
            QySecretMapping res = qySecretMappingMapper.selectOne(new QueryWrapper<QySecretMapping>().eq("qy_id", scvQyExternalUserListVo.getQyId()));
            scvQyExternalUserListVo.setQyName(res.getQyName());
        });
        return ResultJson.success(PageInfo.of(list));
    }

    /**
     * 绑定外部联系人
     */
    @Override
    public ResultJson<?> bindExternal(String externalId, String wxId) {
        FollowWxRelation one = followWxRelationMapper.selectOne(new QueryWrapper<FollowWxRelation>().eq("wx_id", wxId));

        if (null != one) {
            return ResultJson.success(this.update(new UpdateWrapper<FollowWxRelation>()
                    .eq("wx_id", wxId).set("external_id", externalId).set("update_time", new Date())));
        } else {
            FollowWxRelation followWxRelation = new FollowWxRelation();
            followWxRelation.setExternalId(externalId);
            followWxRelation.setWxId(wxId);
            Date date = new Date();
            followWxRelation.setCreateTime(date);
            followWxRelation.setUpdateTime(date);
            return ResultJson.success(followWxRelationMapper.insert(followWxRelation));
        }
    }

    @Override
    public String getOneId(String customerWxId) {
        FollowWxRelation one = followWxRelationMapper.selectOne(new QueryWrapper<FollowWxRelation>().eq("wx_id", customerWxId));
        if (null != one) {
            String externalId = one.getExternalId();
            return scvQyExternalUserMapper.getOneId(Integer.parseInt(externalId));
        }
        return null;
    }
}