package com.jds.clickhouse.web.controller.sys;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author wzy
 * @since 2020-11-03
 */
@Controller
@RequestMapping("/sysDivision")
public class SysDivisionController {

}

