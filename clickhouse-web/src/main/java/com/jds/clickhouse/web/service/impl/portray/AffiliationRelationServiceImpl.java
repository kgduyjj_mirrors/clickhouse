package com.jds.clickhouse.web.service.impl.portray;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jds.clickhouse.web.service.portray.AffiliationRelationService;
import com.jds.link4sales.common.result.ResultJson;
import com.jds.link4sales.model.dto.WxUserPropertyQueryDto;
import com.jds.link4sales.model.entity.portray.AffiliationRelation;
import com.jds.link4sales.model.mapper.portray.AffiliationRelationMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 客户群体与对应员工的所属关系 服务实现类
 * </p>
 *
 * @author zhixiang.peng
 * @since 2020-12-01
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class AffiliationRelationServiceImpl extends ServiceImpl<AffiliationRelationMapper, AffiliationRelation> implements AffiliationRelationService {
    @Override
    public ResultJson<Object> getAllWxUserPropertyList(WxUserPropertyQueryDto actionsDto) {
        return null;
    }

    @Override
    public ResultJson<Object> getMyWxUserPropertyList(WxUserPropertyQueryDto actionsDto) {
        return null;
    }

    @Override
    public ResultJson<Object> getWxBuddyInfo(String customerWxId, Integer pageNum, Integer pageSize) {
        return null;
    }
}
