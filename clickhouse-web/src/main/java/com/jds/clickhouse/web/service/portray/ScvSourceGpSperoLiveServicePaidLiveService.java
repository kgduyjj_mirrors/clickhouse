package com.jds.clickhouse.web.service.portray;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jds.link4sales.model.entity.portray.ScvSourceGpSperoLiveServicePaidLive;

/**
 * <p>
 * 付费直播场次表 服务类
 * </p>
 *
 * @author zhixiang.peng
 * @since 2021-01-22
 */
public interface ScvSourceGpSperoLiveServicePaidLiveService extends IService<ScvSourceGpSperoLiveServicePaidLive> {

}
