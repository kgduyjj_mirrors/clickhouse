package com.jds.clickhouse.web.controller.portray;

import com.jds.link4sales.common.annotation.NoPrintLog;
import com.jds.link4sales.common.result.ResultJson;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 健康检查控制器
 *
 * @author kun.gao
 * @date 2020/11/09
 */
@RestController
public class HealthCheckController {

    @GetMapping("/health")
    @NoPrintLog
    public ResultJson<Object> get() {
        return ResultJson.success();
    }

}
