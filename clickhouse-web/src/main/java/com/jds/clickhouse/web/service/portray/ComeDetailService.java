package com.jds.clickhouse.web.service.portray;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jds.link4sales.model.entity.portray.ComeDetail;
import com.jds.link4sales.model.entity.portray.CustomerWechatDetail;

import java.util.List;

/**
 * <p>
 * 客户进线的渠道说明 服务类
 * </p>
 *
 * @author JACKSON G
 * @since 2020-12-01
 */
public interface ComeDetailService extends IService<ComeDetail> {


    /**
     * 通过customerWxid列表获取客户详情
     *
     * @param customerWxidList customerWxidList列表
     * @return {@link List < CustomerWechatDetail >}
     */
    List<ComeDetail> listByCustomerWxid(List<String> customerWxidList);

}
