package com.jds.clickhouse.web.service.portray;


import com.baomidou.mybatisplus.extension.service.IService;
import com.jds.link4sales.common.result.ResultJson;
import com.jds.link4sales.model.dto.ActionsDto;
import com.jds.link4sales.model.entity.portray.ScvLiveUserBehavior;

import java.util.List;

/**
 * <p>
 * 直播客户行为表 服务类
 * </p>
 *
 * @author JACKSON G
 * @since 2020-11-27
 */
public interface ScvLiveUserBehaviorService extends IService<ScvLiveUserBehavior> {

    /**
     * 查询直播室动作列表
     * Query the list of live room actions
     *
     * @param actionsDto 行动dto
     * @return {@link List < ScvUserLiveRoomBehaviorVo >}
     */
    ResultJson<Object> queryLiveActionList(ActionsDto actionsDto);

    /**
     * 查询直播室动作列表（微信）
     * Query the list of live room actions
     *
     * @param actionsDto 行动dto
     * @return {@link List < ScvUserLiveRoomBehaviorVo >}
     */
    ResultJson<Object> queryWxLiveActionList(ActionsDto actionsDto);

}
