package com.jds.clickhouse.web.service.impl.portray;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jds.clickhouse.web.service.portray.CustomerWechatDetailService;
import com.jds.link4sales.model.entity.portray.CustomerWechatDetail;
import com.jds.link4sales.model.mapper.portray.CustomerWechatDetailMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * <p>
 * 客户微信详情表 服务实现类
 * </p>
 *
 * @author JACKSON G
 * @since 2020-12-01
 */
@Service
public class CustomerWechatDetailServiceImpl extends ServiceImpl<CustomerWechatDetailMapper, CustomerWechatDetail> implements CustomerWechatDetailService {

    @Autowired
    private CustomerWechatDetailMapper customerWechatDetailMapper;

    @Override
    public List<CustomerWechatDetail> listByCustomerWxid(List<String> customerWxidList) {

        if(!CollectionUtils.isEmpty(customerWxidList)){
            return customerWechatDetailMapper.selectList(new LambdaQueryWrapper<CustomerWechatDetail>().in(CustomerWechatDetail::getCustomerWxid,customerWxidList));
        }
        return   null;
    }

    @Override
    public List<CustomerWechatDetail> listByCompanyWxid(List<String> companyWxidList) {
        if(!CollectionUtils.isEmpty(companyWxidList)){
            return customerWechatDetailMapper.selectList(new LambdaQueryWrapper<CustomerWechatDetail>().in(CustomerWechatDetail::getUserId,companyWxidList));
        }
        return   null;
    }

    @Override
    public String getWxId(String str,String acc) {
       return customerWechatDetailMapper.getWxId(str,acc);
    }
}
