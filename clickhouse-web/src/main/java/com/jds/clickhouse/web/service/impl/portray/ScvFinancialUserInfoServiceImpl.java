package com.jds.clickhouse.web.service.impl.portray;

import com.jds.clickhouse.web.service.portray.ScvFinancialUserInfoService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jds.link4sales.model.entity.portray.ScvFinancialUserInfo;
import com.jds.link4sales.model.mapper.portray.ScvFinancialUserInfoMapper;

/**
 * <p>
 * 财商课用户基本信息 服务实现类
 * </p>
 *
 * @author wzy
 * @since 2020-12-16
 */
@Service
public class ScvFinancialUserInfoServiceImpl extends ServiceImpl<ScvFinancialUserInfoMapper, ScvFinancialUserInfo> implements ScvFinancialUserInfoService {

}
