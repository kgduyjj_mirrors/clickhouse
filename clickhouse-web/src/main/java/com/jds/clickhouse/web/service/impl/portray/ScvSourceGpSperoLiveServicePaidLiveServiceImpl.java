package com.jds.clickhouse.web.service.impl.portray;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jds.clickhouse.web.service.portray.ScvSourceGpSperoLiveServicePaidLiveService;
import com.jds.link4sales.model.entity.portray.ScvSourceGpSperoLiveServicePaidLive;
import com.jds.link4sales.model.mapper.portray.ScvSourceGpSperoLiveServicePaidLiveMapper;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 付费直播场次表 服务实现类
 * </p>
 *
 * @author zhixiang.peng
 * @since 2021-01-22
 */
@Service
public class ScvSourceGpSperoLiveServicePaidLiveServiceImpl extends ServiceImpl<ScvSourceGpSperoLiveServicePaidLiveMapper, ScvSourceGpSperoLiveServicePaidLive> implements ScvSourceGpSperoLiveServicePaidLiveService {

}
