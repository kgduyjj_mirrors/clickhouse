package com.jds.clickhouse.web.controller.sys;

import javax.validation.Valid;

import com.jds.clickhouse.web.service.sys.SysUserService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jds.link4sales.common.annotation.Login;
import com.jds.link4sales.common.result.ResultJson;
import com.jds.link4sales.model.dto.SysUserListDto;
import com.jds.link4sales.model.entity.sys.SysUser;

import lombok.RequiredArgsConstructor;

/**
 * 系统用户控制器
 *
 * @author kun.gao
 * @date 2020/11/02
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/web/sysUser")
@Login
public class SysUserController {

    private final SysUserService sysUserService;

    @PostMapping("/list")
    public ResultJson<?> list(@RequestBody @Valid SysUserListDto dto) {
        PageHelper.startPage(dto.getPageNum(), dto.getPageSize());
        return ResultJson.success(PageInfo.of(sysUserService.list(dto)));
    }

    /**
     * 启用禁用 员工
     * 
     * @param
     * @return
     */
    @PostMapping("/updateLink4salesStatus")
    public ResultJson<?> updateLink4salesStatus(@RequestBody SysUser sysUser) {
        return ResultJson.success(sysUserService.update(new UpdateWrapper<SysUser>()
                .eq("user_login", sysUser.getUserLogin()).set("link4sales_status", sysUser.getLink4salesStatus())));
    }

}
