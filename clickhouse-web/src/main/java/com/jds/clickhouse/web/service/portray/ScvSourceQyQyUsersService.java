package com.jds.clickhouse.web.service.portray;


import com.baomidou.mybatisplus.extension.service.IService;
import com.jds.link4sales.model.entity.portray.ScvSourceQyQyUsers;

/**
 * <p>
 * 企业微信用户 服务类
 * </p>
 *
 * @author JACKSON G
 * @since 2020-11-26
 */
public interface ScvSourceQyQyUsersService extends IService<ScvSourceQyQyUsers> {

}
