package com.jds.clickhouse.web.controller.portray;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import lombok.RequiredArgsConstructor;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author wzy
 * @since 2020-11-04
 */
@Controller
@RequiredArgsConstructor
@RequestMapping("/scvQyFollowUser")
public class ScvQyFollowUserController {

}
