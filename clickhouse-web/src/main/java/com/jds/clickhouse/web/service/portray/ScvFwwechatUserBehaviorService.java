package com.jds.clickhouse.web.service.portray;


import com.baomidou.mybatisplus.extension.service.IService;
import com.jds.link4sales.common.result.ResultJson;
import com.jds.link4sales.model.dto.ActionsDto;
import com.jds.link4sales.model.entity.portray.ScvFwwechatUserBehavior;
import com.jds.link4sales.model.vo.ScvFwwechatUserBehaviorVo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author JACKSON G
 * @since 2020-11-06
 */
public interface ScvFwwechatUserBehaviorService extends IService<ScvFwwechatUserBehavior> {

    /**
     * 弗兰克-威廉姆斯行为列表查询
     *
     * @param actionsDto 行动dto
     * @return {@link List <  ScvFwwechatUserBehaviorVo  >}
     */
     ResultJson<Object> queryFwBehaviorList(ActionsDto actionsDto);

    /**
     * 弗兰克-威廉姆斯行为列表查询(微信)
     *
     * @param actionsDto 行动dto
     * @return {@link List <  ScvFwwechatUserBehaviorVo  >}
     */
     ResultJson<Object> queryWxFwBehaviorList(ActionsDto actionsDto);

    /**
     * 弗兰克-威廉姆斯行为列表查询
     *
     * @param actionsDto 行动dto
     * @return {@link List <  ScvFwwechatUserBehaviorVo  >}
     */
    ResultJson<Object> queryUserFwBehaviorList(ActionsDto actionsDto);
    
    ResultJson<Object> queryWxUserFwBehaviorList(ActionsDto actionsDto);

}
