package com.jds.clickhouse.web.controller.sys;


import cn.hutool.core.lang.Assert;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jds.link4sales.common.annotation.Login;
import com.jds.link4sales.common.result.ResultJson;
import com.jds.link4sales.model.dto.RoleMenuDto;
import com.jds.link4sales.model.entity.sys.SysRoleMenu;
import com.jds.clickhouse.web.service.sys.SysRoleMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * sys_role_menu  前端控制器
 * </p>
 *
 * @author zhixiang.peng
 * @since 2020-11-09
 */
@RestController
@RequestMapping("/web/sysRoleMenu")
@Login
public class SysRoleMenuController {

    @Autowired
    private SysRoleMenuService sysRoleMenuService;

    /**
     * 保存角色菜單关系
     *
     * @param roleMenuDto
     * @return
     */
    @PostMapping("/saveOrUpdateRoleMenu")
    public ResultJson saveUserRole(@Validated @RequestBody RoleMenuDto roleMenuDto) {
        sysRoleMenuService.saveOrUpdateRoleMenu(roleMenuDto);
        return ResultJson.success();
    }

    /**
     * 根据角色id查询菜单一览
     * @param roleId 角色id
     * @return
     */
    @GetMapping("/listByRoleId")
    public ResultJson listByUserId(String roleId) {
        Assert.notNull(roleId, "角色id不能为空");
        return ResultJson.success(sysRoleMenuService.list(new QueryWrapper<SysRoleMenu>().eq("role_id", roleId)));
    }
}

