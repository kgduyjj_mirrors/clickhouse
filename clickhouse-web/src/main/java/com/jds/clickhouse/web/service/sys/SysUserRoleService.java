package com.jds.clickhouse.web.service.sys;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jds.link4sales.model.dto.UserRoleDto;
import com.jds.link4sales.model.entity.sys.SysUserRole;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * sys_user_role  服务类
 * </p>
 *
 * @author zhixiang.peng
 * @since 2020-11-03
 */
public interface SysUserRoleService extends IService<SysUserRole> {

    /**
     *  批量保存用户角色关系
     * @param  userRoleDtoList userRoleDtoList
     * @return  void
     */
    void  saveUserRole(List<UserRoleDto> userRoleDtoList);

    /**
     *  判断某个用户是否用某个角色的权限
     * @param  userId  用户id工号
     * @param  roleCode 角色编码
     * @return  Boolean
     */
     Boolean  isHasRole(String  userId,String roleCode);

    /**
     *  判断用户是否有当前url的访问权限
     * @param userId 用户id
     * @param url url
     * @return
     */
    Integer selectAuthCountByUserId(String userId , String url);

    /**
     * 根据用户code获取用户角色一览
     * @param  userCode 用户工号
     * @return
     */
    List<SysUserRole>  selectRoleInfoByUserCode(String  userCode);

}
