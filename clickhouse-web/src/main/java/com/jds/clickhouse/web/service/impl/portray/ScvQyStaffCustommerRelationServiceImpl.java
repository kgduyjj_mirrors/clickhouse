package com.jds.clickhouse.web.service.impl.portray;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jds.clickhouse.web.service.portray.ScvQyStaffCustommerRelationService;
import com.jds.link4sales.model.entity.portray.ScvQyStaffCustommerRelation;
import com.jds.link4sales.model.mapper.portray.ScvQyStaffCustommerRelationMapper;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 员工客户关联关系表 服务实现类
 * </p>
 *
 * @author JACKSON G
 * @since 2020-11-27
 */
@Service
public class ScvQyStaffCustommerRelationServiceImpl extends ServiceImpl<ScvQyStaffCustommerRelationMapper, ScvQyStaffCustommerRelation> implements ScvQyStaffCustommerRelationService {

}
