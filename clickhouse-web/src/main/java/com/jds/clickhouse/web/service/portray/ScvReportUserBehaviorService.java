package com.jds.clickhouse.web.service.portray;


import com.baomidou.mybatisplus.extension.service.IService;
import com.jds.link4sales.common.result.ResultJson;
import com.jds.link4sales.model.dto.ActionsDto;
import com.jds.link4sales.model.entity.portray.ScvReportUserBehavior;

import java.util.List;

/**
 * <p>
 * 报告会客户行为表 服务类
 * </p>
 *
 * @author JACKSON G
 * @since 2020-11-27
 */
public interface ScvReportUserBehaviorService extends IService<ScvReportUserBehavior> {

    /**
     * 查询报告会动作列表
     * Query the list of report actions
     *
     * @param actionsDto 行动dto
     * @return {@link List < ScvUserLiveRoomBehaviorVo >}
     */
    ResultJson<Object> queryReportActionList(ActionsDto actionsDto);
    /**
     * 查询报告会动作列表(微信)
     * Query the list of report actions
     *
     * @param actionsDto 行动dto
     * @return {@link List < ScvUserLiveRoomBehaviorVo >}
     */
    ResultJson<Object> queryWxReportActionList(ActionsDto actionsDto);

}
