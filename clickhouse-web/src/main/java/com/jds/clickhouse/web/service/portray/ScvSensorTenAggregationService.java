package com.jds.clickhouse.web.service.portray;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jds.link4sales.common.result.ResultJson;
import com.jds.link4sales.model.dto.ActionsDto;
import com.jds.link4sales.model.entity.portray.ScvSensorTenAggregation;

/**
 * <p>
 * 神策埋点10分钟聚合数据 服务类
 * </p>
 *
 * @author JUSTIN G
 * @since 2021-01-07
 */
public interface ScvSensorTenAggregationService extends IService<ScvSensorTenAggregation> {


    /**
     * 获取股拍直播（侧边栏）
     *
     * @param actionsDto
     * @return ResultJson
     */
    ResultJson getGpLiveInfo(ActionsDto actionsDto);

    /**
     * 获取股拍直播（侧边栏）
     *
     * @param actionsDto
     * @return ResultJson
     */
    ResultJson getGpLiveInfoV2(ActionsDto actionsDto);

    /**
     * 得到列表
     *
     * @param actionsDto 行动dto
     * @return {@link ResultJson<Object>}
     */
    ResultJson<Object> getList(ActionsDto actionsDto);

    /**
     * 得到列表
     *
     * @param actionsDto 行动dto
     * @return {@link ResultJson<Object>}
     */
    ResultJson<Object> getCommentList(ActionsDto actionsDto);


    /**
     * 获取股拍直播（web明细-外部联系人）
     *
     * @param actionsDto
     * @return ResultJson
     */
    ResultJson getWebWeChatGpLiveInfo(ActionsDto actionsDto);

    /**
     * 获取股拍直播（web明细-个人微信）
     *
     * @param actionsDto
     * @return ResultJson
     */
    ResultJson getWebWxGpLiveInfo(ActionsDto actionsDto);
    /**
     * 获取股拍直播（web明细-外部联系人v2）
     *
     * @param actionsDto
     * @return ResultJson
     */
    ResultJson getWebWeChatGpLiveInfoV2(ActionsDto actionsDto);

    /**
     * 获取股拍直播（web明细-个人微信v2）
     *
     * @param actionsDto
     * @return ResultJson
     */
    ResultJson getWebWxGpLiveInfoV2(ActionsDto actionsDto);

    /**
     * 得到列表(行为翻页)
     *
     * @param actionsDto 行动dto
     * @return {@link ResultJson<Object>}
     */
    ResultJson<Object> getActionDataList(ActionsDto actionsDto);



}
