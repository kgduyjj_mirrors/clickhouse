package com.jds.clickhouse.web.service.impl.portray;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.jds.clickhouse.web.service.portray.ScvQyExternalUserService;
import com.jds.clickhouse.web.service.portray.ScvSourceQyQyUsersService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.jds.link4sales.common.exception.BusinessException;
import com.jds.link4sales.common.utils.AesUtils;
import com.jds.link4sales.model.dto.ScvQyExternalUserListDto;
import com.jds.link4sales.model.entity.portray.ScvQyExternalUser;
import com.jds.link4sales.model.entity.portray.ScvSourceQyQyUsers;
import com.jds.link4sales.model.entity.sys.SysUser;
import com.jds.link4sales.model.mapper.portray.ScvQyExternalUserMapper;
import com.jds.link4sales.model.mapper.sys.SysUserMapper;
import com.jds.link4sales.model.vo.ScvQyExternalUserListVo;
import com.jds.clickhouse.web.service.sys.SysUserService;

import lombok.RequiredArgsConstructor;
import org.springframework.util.CollectionUtils;

/**
 * <p>
 * 企业外部联系人表 服务实现类
 * </p>
 *
 * @author wzy
 * @since 2020-11-04
 */
@Service
@RequiredArgsConstructor
public class ScvQyExternalUserServiceImpl extends ServiceImpl<ScvQyExternalUserMapper, ScvQyExternalUser>
        implements ScvQyExternalUserService {

    private final ScvQyExternalUserMapper scvQyExternalUserMapper;

    private final SysUserService sysUserService;

    private final SysUserMapper sysUserMapper;

    private final ScvSourceQyQyUsersService scvSourceQyQyUsersService;

    @Override
    public List<ScvQyExternalUserListVo> list(ScvQyExternalUserListDto dto) {
        PageHelper.startPage(dto.getPageNum(), dto.getPageSize());
        String userTag = dto.getUserTag();
        if (StringUtils.isNotBlank(userTag)) {
            List<String> selectUserLoginList = sysUserMapper.selectUserLoginList(dto.getUserTag());
            if(!CollectionUtils.isEmpty(selectUserLoginList)){
                dto.setUserLoginList(selectUserLoginList);
            }else{
            //不存在员工的话  直接返回空数据
              return   new ArrayList<ScvQyExternalUserListVo>();
            }
        }
        List<ScvQyExternalUserListVo> list = scvQyExternalUserMapper.list(dto);
        if (list != null && list.size() > 0) {
            List<String> collect = list.stream().map(ScvQyExternalUserListVo::getUserCode).collect(Collectors.toList());
            List<SysUser> list1 = sysUserService
                    .list(new QueryWrapper<SysUser>().lambda().in(SysUser::getUserLogin, collect));
            list.forEach(scvQyExternalUserListVo -> {
                list1.forEach(sysUser -> {
                    if (scvQyExternalUserListVo.getUserCode().equals(sysUser.getUserLogin())) {
                        scvQyExternalUserListVo.setName(sysUser.getRealname());
                    }
                });
            });
            List<String> followerIdList = list.stream().map(ScvQyExternalUserListVo::getFollowUserId)
                    .collect(Collectors.toList());
            List<ScvSourceQyQyUsers> qyUserList = scvSourceQyQyUsersService
                    .list(new LambdaQueryWrapper<ScvSourceQyQyUsers>().eq(ScvSourceQyQyUsers::getQyId, dto.getQyId())
                            .in(ScvSourceQyQyUsers::getUserid, followerIdList));
            list.forEach(scvQyExternalUserListVo -> {
                qyUserList.forEach(scvSourceQyQyUsers -> {
                    if (scvSourceQyQyUsers.getUserid().equals(scvQyExternalUserListVo.getFollowUserId())) {
                        if (StringUtils.isNotBlank(scvSourceQyQyUsers.getMobile())) {
                            scvQyExternalUserListVo
                                    .setMobile(AesUtils.desEncryptTerminal(scvSourceQyQyUsers.getMobile()));
                        }
                    }
                });
            });
        }
        return list;
    }

    @Override
    public List<ScvQyExternalUser> listByExternalId(List<Integer> idList) {
        if (idList != null && idList.size() > 0) {
            return scvQyExternalUserMapper.selectBatchIds(idList);
        } else {
            throw new BusinessException("idList 不能为空");
        }
    }

}
