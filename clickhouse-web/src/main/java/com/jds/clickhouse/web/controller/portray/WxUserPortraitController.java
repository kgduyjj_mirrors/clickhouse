package com.jds.clickhouse.web.controller.portray;

import javax.servlet.http.HttpServletRequest;

import com.alibaba.fastjson.JSONObject;
import com.jds.clickhouse.web.service.portray.WxUserPortraitService;
import com.jds.link4sales.common.annotation.DataAuth;
import com.jds.link4sales.common.annotation.NoCheckUrlAuth;
import com.jds.link4sales.model.dto.ScvQyExternalUserListDto;
import com.jds.link4sales.model.dto.WxUserPropertyQueryDto;
import com.jds.link4sales.model.vo.EmployeeAdditionVo;
import com.jds.clickhouse.web.service.portray.AffiliationRelationService;
import com.jds.clickhouse.web.service.portray.WxUserPortraitDetailService;
import org.springframework.web.bind.annotation.*;

import com.jds.link4sales.common.annotation.Login;
import com.jds.link4sales.common.result.ResultJson;
import com.jds.link4sales.model.dto.ActionsDto;
import com.jds.clickhouse.web.service.portray.WechatUserPortraitService;

import cn.hutool.core.lang.Assert;
import lombok.RequiredArgsConstructor;

/**
 * 微信用户画像控制器
 *
 * @author JACKSON G
 * @date 2020/11/04
 * This class is the entry point of all WeChat related web pages
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/web/wx/user/portrait")
@Login
public class WxUserPortraitController {

    private final WechatUserPortraitService userPortraitService;

    private final WxUserPortraitService wxUserPortraitService;

    private final WxUserPortraitDetailService wxUserPortraitDetailService;

    private final AffiliationRelationService affiliationRelationService;

    /**
     * 头部信息
     */
    @GetMapping("/head/info")
    @NoCheckUrlAuth
    public ResultJson<Object> getWxWebHeadInfo(String companyWxId,String customerWxId) {
        Assert.notBlank(companyWxId, "员工微信id不能为空");
        Assert.notBlank(customerWxId, "客户微信id不能为空");
        return wxUserPortraitService.getWxUserHeaInfo(companyWxId, customerWxId);
    }

    /**
     * 好友列表
     */
    @GetMapping("/getBuddy")
    public ResultJson<Object> getWxBuddy(String customerWxId,Integer pageNum ,Integer pageSize) {
        Assert.notNull(customerWxId, "客户微信id不能为空");
        if (pageNum == null || pageNum <=0 ) {
            pageNum = 1;
        }
        if (pageSize == null || pageSize <=0 ) {
            pageSize = 10;
        }
        return wxUserPortraitService.getWxBuddy(customerWxId,pageNum,pageSize);
    }

    /**
     * 群列表
     */
    @GetMapping("/getGroups")
    public ResultJson<Object> getGroups(String customerWxId,String companyWxId ,Integer pageNum ,Integer pageSize,Integer type) {
        Assert.notNull(customerWxId, "客户微信id不能为空");
        Assert.notNull(companyWxId, "员工微信id不能为空");
        if (pageNum == null || pageNum <=0 ) {
            pageNum = 1;
        }
        if (pageSize == null || pageSize <=0 ) {
            pageSize = 10;
        }
        return wxUserPortraitService.getWxGroupsInfo(customerWxId,companyWxId,pageNum,pageSize,type);
    }

    @PostMapping("/update")
    public ResultJson<Object> saveOrUpdate(@RequestBody EmployeeAdditionVo employeeAdditionVo,
                                           HttpServletRequest request) {
        Assert.notNull(employeeAdditionVo, "参数不能为空");
        Assert.notNull(employeeAdditionVo.getCompanyWxid(), "员工微信id不能为空");
        Assert.notNull(employeeAdditionVo.getCustomerWxid(), "客户微信id不能为空");
        Assert.notBlank(employeeAdditionVo.getUserId(), "员工id不能为空");
        return wxUserPortraitService.updateAddition(employeeAdditionVo);
    }

    /**
     * 全部微信客户行为
     *
     */
    @PostMapping("/getActions")
    @DataAuth
    public ResultJson<Object> getWebActionsList(@RequestBody ActionsDto actionsDto, HttpServletRequest request) {
        Assert.notNull(actionsDto, "参数不能为空");
        return userPortraitService.getWxWebActionsList(actionsDto);
    }

    /**
     * 我的微信客户行为
     *
     * @param
     * @return
     */
    @PostMapping("/mine/getActions")
    public ResultJson<Object> getMyWxWebActionsList(@RequestBody ActionsDto actionsDto, HttpServletRequest request) {
        Assert.notNull(actionsDto, "参数不能为空");
        return userPortraitService.getWxWebActionsList(actionsDto);
    }

    /**
     * 全部微信客户属性
     *
     * @param
     * @return
     */
    @PostMapping("/getProperty")
    @DataAuth
    public ResultJson<Object> getPropertyList(@RequestBody WxUserPropertyQueryDto actionsDto, HttpServletRequest request) {
        Assert.notNull(actionsDto, "参数不能为空");
        return affiliationRelationService.getAllWxUserPropertyList(actionsDto);
    }

    /**
     * 我的微信客户属性
     */
    @PostMapping("/mine/getProperty")
    public ResultJson<Object> getMyPropertyList(@RequestBody WxUserPropertyQueryDto actionsDto, HttpServletRequest request) {
        Assert.notNull(actionsDto, "参数不能为空");
        return affiliationRelationService.getMyWxUserPropertyList(actionsDto);
    }

    @PostMapping("/action/details")
    public ResultJson<Object> getActionDetailsList(@RequestBody ActionsDto actionsDto, HttpServletRequest request) {
        Assert.notNull(actionsDto, "参数不能为空");
        Assert.notNull(actionsDto.getCustomerWxId(), "联系人id不能为空");
        return wxUserPortraitService.getAppUserActions(actionsDto);
    }

    @PostMapping("/getCustomerList")
    public ResultJson<?> getCustomerList(@RequestBody ScvQyExternalUserListDto dto) {
        Assert.notNull(dto, "参数不能为空");
        return wxUserPortraitDetailService.getCustomerList(dto);
    }

    @PostMapping("/bindExternal")
    public ResultJson<?> bindExternal(@RequestBody String req) {
        JSONObject obj = JSONObject.parseObject(req);
        String externalId = obj.getString("externalId");
        String wxId = obj.getString("customerWxId");
        Assert.notNull(externalId, "外部联系人id不能为空");
        Assert.notNull(wxId, "客户微信id不能为空");
        return wxUserPortraitDetailService.bindExternal(externalId, wxId);
    }

}
