package com.jds.clickhouse.web.service.impl.portray;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jds.clickhouse.web.service.portray.ScvSourceGpSperoCommentServiceLiveCommentService;
import com.jds.link4sales.model.entity.portray.ScvSourceGpSperoCommentServiceLiveComment;
import com.jds.link4sales.model.mapper.portray.ScvSourceGpSperoCommentServiceLiveCommentMapper;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 直播评论表 服务实现类
 * </p>
 *
 * @author JUSTIN G
 * @since 2021-01-07
 */
@Service
public class ScvSourceGpSperoCommentServiceLiveCommentServiceImpl extends ServiceImpl<ScvSourceGpSperoCommentServiceLiveCommentMapper, ScvSourceGpSperoCommentServiceLiveComment> implements ScvSourceGpSperoCommentServiceLiveCommentService {

}
