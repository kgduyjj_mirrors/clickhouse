package com.jds.clickhouse.web.service.sys;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jds.link4sales.model.entity.sys.SysDataAuth;
import com.jds.link4sales.model.entity.sys.SysMenu;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zhixiang.peng
 * @since 2020-11-12
 */
public interface SysDataAuthService extends IService<SysDataAuth> {

    /**
     * 批量设置功能权限
     * @param sysDataAuthList
     * @return 菜单列表
     */
    void setDataAuth(List<SysDataAuth> sysDataAuthList);

}
