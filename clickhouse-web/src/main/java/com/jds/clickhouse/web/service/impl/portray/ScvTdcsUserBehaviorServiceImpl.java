package com.jds.clickhouse.web.service.impl.portray;

import java.util.List;
import java.util.stream.Collectors;

import com.jds.clickhouse.web.service.portray.CustomerWechatDetailService;
import com.jds.clickhouse.web.service.portray.ScvQyExternalUserService;
import com.jds.clickhouse.web.service.portray.ScvSourceQyQyUsersService;
import com.jds.clickhouse.web.service.portray.ScvTdcsUserBehaviorService;
import com.jds.link4sales.model.entity.portray.CustomerWechatDetail;
import com.jds.link4sales.model.vo.ScvWxTdcsUserBehaviorVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jds.link4sales.common.result.ResultJson;
import com.jds.link4sales.common.utils.AesUtils;
import com.jds.link4sales.model.dto.ActionsDto;
import com.jds.link4sales.model.entity.portray.ScvQyExternalUser;
import com.jds.link4sales.model.entity.portray.ScvSourceQyQyUsers;
import com.jds.link4sales.model.entity.portray.ScvTdcsUserBehavior;
import com.jds.link4sales.model.entity.sys.SysUser;
import com.jds.link4sales.model.mapper.portray.ScvTdcsUserBehaviorMapper;
import com.jds.link4sales.model.vo.ScvTdcsUserBehaviorVo;
import com.jds.clickhouse.web.service.sys.SysUserService;

import lombok.RequiredArgsConstructor;
import org.springframework.util.CollectionUtils;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author JACKSON G
 * @since 2020-11-09
 */
@Service
@RequiredArgsConstructor
public class ScvTdcsUserBehaviorServiceImpl extends ServiceImpl<ScvTdcsUserBehaviorMapper, ScvTdcsUserBehavior>
        implements ScvTdcsUserBehaviorService {

    private final ScvTdcsUserBehaviorMapper scvTdcsUserBehaviorMapper;

    private final ScvQyExternalUserService scvQyExternalUserService;

    private final ScvSourceQyQyUsersService scvSourceQyQyUsersService;

    private final SysUserService sysUserService;

    private final CustomerWechatDetailService customerWechatDetailService;

    @Value("${aes.key}")
    private String secretKey;

    @Override
    public ResultJson<Object> queryOpenAccountActionList(ActionsDto actionsDto) {
        PageHelper.startPage(actionsDto.getPageNum(), actionsDto.getPageSize());
        List<ScvTdcsUserBehaviorVo> scvTdcsUserBehaviorVos = scvTdcsUserBehaviorMapper
                .queryOpenAccountActionList(actionsDto);
        if (scvTdcsUserBehaviorVos != null && scvTdcsUserBehaviorVos.size() > 0) {
            List<Integer> collect = scvTdcsUserBehaviorVos.stream().map(ScvTdcsUserBehaviorVo::getExId)
                    .collect(Collectors.toList());
            List<ScvQyExternalUser> scvQyExternalUsers = scvQyExternalUserService.listByExternalId(collect);
            if (scvQyExternalUsers != null && scvQyExternalUsers.size() > 0) {
                scvTdcsUserBehaviorVos.forEach(scvTdcsUserBehaviorVo -> {
                    scvQyExternalUsers.forEach(scvQyExternalUser -> {
                        if (scvTdcsUserBehaviorVo.getExId().equals(scvQyExternalUser.getId())) {
                            scvTdcsUserBehaviorVo.setAvatar(scvQyExternalUser.getAvatar());
                            scvTdcsUserBehaviorVo.setNickName(scvQyExternalUser.getName());
                        }
                    });
                });
            }
            List<String> followerIdList = scvTdcsUserBehaviorVos.stream().map(ScvTdcsUserBehaviorVo::getFollowUserId)
                    .collect(Collectors.toList());
            List<ScvSourceQyQyUsers> list = scvSourceQyQyUsersService.list(
                    new LambdaQueryWrapper<ScvSourceQyQyUsers>().eq(ScvSourceQyQyUsers::getQyId, actionsDto.getQyId())
                            .in(ScvSourceQyQyUsers::getUserid, followerIdList));
            scvTdcsUserBehaviorVos.forEach(scvTdcsUserBehaviorVo -> {
                list.forEach(scvSourceQyQyUsers -> {
                    if (scvSourceQyQyUsers.getUserid().equals(scvTdcsUserBehaviorVo.getFollowUserId())) {
                        scvTdcsUserBehaviorVo.setFollowUserName(scvSourceQyQyUsers.getName());
                        if (StringUtils.isNotBlank(scvSourceQyQyUsers.getMobile())) {
                            scvTdcsUserBehaviorVo
                                    .setMobile(AesUtils.desEncryptTerminal(scvSourceQyQyUsers.getMobile()));
                        }
                    }
                });
            });
            List<String> userCodeList = scvTdcsUserBehaviorVos.stream().map(ScvTdcsUserBehaviorVo::getUserCode)
                    .collect(Collectors.toList());
            List<SysUser> list1 = sysUserService
                    .list(new LambdaQueryWrapper<SysUser>().in(SysUser::getUserLogin, userCodeList));
            scvTdcsUserBehaviorVos.forEach(scvTdcsUserBehaviorVo -> {
                list1.forEach(sysUser -> {
                    if (scvTdcsUserBehaviorVo.getUserCode().equals(sysUser.getUserLogin())) {
                        scvTdcsUserBehaviorVo.setName(sysUser.getRealname());
                    }
                });
            });
        }
        return ResultJson.success(new PageInfo<>(scvTdcsUserBehaviorVos));
    }

    /**
     *  开户入金行为数据 （微信）
     * @param  actionsDto actionsDto
     * @return  ResultJson ResultJson
     */
    @Override
    public ResultJson<Object> queryWxOpenAccountActionList(ActionsDto actionsDto) {

        PageHelper.startPage(actionsDto.getPageNum(), actionsDto.getPageSize());
         //获取开户入金数据
        List<ScvWxTdcsUserBehaviorVo> scvTdcsUserBehaviorVos = scvTdcsUserBehaviorMapper
                .queryWxOpenAccountActionList(actionsDto);
        // 获取微信号集合
        if (!CollectionUtils.isEmpty(scvTdcsUserBehaviorVos)) {
            List<String> collectCustomerWxid = scvTdcsUserBehaviorVos.stream().map(ScvWxTdcsUserBehaviorVo::getCustomerWxid)
                    .collect(Collectors.toList());
            List<CustomerWechatDetail> customerWechatDetailUsers = customerWechatDetailService.listByCustomerWxid(collectCustomerWxid);
            //数据赋值
            if (!CollectionUtils.isEmpty(customerWechatDetailUsers)) {
                scvTdcsUserBehaviorVos.forEach(scvTdcsUserBehaviorVo -> {
                    customerWechatDetailUsers.forEach(customerWechatDetailUser -> {
                        if (scvTdcsUserBehaviorVo.getCustomerWxid().equals(customerWechatDetailUser.getCustomerWxid())) {
                            scvTdcsUserBehaviorVo.setAvatar(customerWechatDetailUser.getWechatHeadUrl());
                            scvTdcsUserBehaviorVo.setNickName(customerWechatDetailUser.getNickname());
                        }
                    });
                });
            }

            //获取微信号-company_wxid在customer_wechat_detail中关联user_id然后获取wechat_wxacc
            List<String> collectCompanyWxid = scvTdcsUserBehaviorVos.stream().map(ScvWxTdcsUserBehaviorVo::getCompanyWxid)
                    .collect(Collectors.toList());

            //通过customerWxid集合获取customer_wechat_detail客户微信表的详情信息
            List<CustomerWechatDetail> companyCustomerWechatDetailUsers = customerWechatDetailService.listByCustomerWxid(collectCompanyWxid);
            //所属微信号赋值
            scvTdcsUserBehaviorVos.forEach(scvTdcsUserBehaviorVo -> {
                companyCustomerWechatDetailUsers.forEach(companyCustomerWechatDetailUser -> {
                    if (scvTdcsUserBehaviorVo.getCompanyWxid().equals(companyCustomerWechatDetailUser.getCustomerWxid())) {
                        if (StringUtils.isNotBlank(companyCustomerWechatDetailUser.getWechatWxacc())) {
                            scvTdcsUserBehaviorVo.setBelongWxid( AesUtils.decrypt(companyCustomerWechatDetailUser.getWechatWxacc(), secretKey));
                        }
                    }
                });
            });
//             //获取员工工号
//            List<String> userCodeList = scvTdcsUserBehaviorVos.stream().map(ScvWxTdcsUserBehaviorVo::getEmployeeId)
//                    .collect(Collectors.toList());
//            List<SysUser> list1 = sysUserService
//                    .list(new LambdaQueryWrapper<SysUser>().in(SysUser::getUserLogin, userCodeList));
//            //根据工号赋值员工姓名
//            scvTdcsUserBehaviorVos.forEach(scvTdcsUserBehaviorVo -> {
//                list1.forEach(sysUser -> {
//                    if (scvTdcsUserBehaviorVo.getEmployeeId().equals(sysUser.getUserLogin())) {
//                        scvTdcsUserBehaviorVo.setName(sysUser.getRealname());
//                    }
//                });
//            });
        }
        return ResultJson.success(new PageInfo<>(scvTdcsUserBehaviorVos));

    }

    @Override
    public ResultJson<Object> queryUserOpenAccountActionList(ActionsDto actionsDto) {
        PageHelper.startPage(actionsDto.getPageNum(), actionsDto.getPageSize());
        List<ScvTdcsUserBehaviorVo> scvTdcsUserBehaviorVos = scvTdcsUserBehaviorMapper
                .queryUserOpenAccountActionList(actionsDto);
        return ResultJson.success(new PageInfo<>(scvTdcsUserBehaviorVos));
    }
    
    @Override
    public ResultJson<Object> queryWxUserOpenAccountActionList(ActionsDto actionsDto) {
        PageHelper.startPage(actionsDto.getPageNum(), actionsDto.getPageSize());
        List<ScvTdcsUserBehaviorVo> scvTdcsUserBehaviorVos = scvTdcsUserBehaviorMapper
                .queryWxUserOpenAccountActionList(actionsDto);
        return ResultJson.success(new PageInfo<>(scvTdcsUserBehaviorVos));
    }

    @Override
    public List<Integer> queryQyIdByUserId(String userId) {
        return scvTdcsUserBehaviorMapper.queryQyIdByUserId(userId);
    }

}
