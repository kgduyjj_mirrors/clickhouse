package com.jds.clickhouse.web.controller.portray;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 * 配置了客户联系功能的企业员工表 前端控制器
 * </p>
 *
 * @author wzy
 * @since 2020-11-04
 */
@Controller
@RequestMapping("/scvQyScopeFollowUser")
public class ScvQyScopeFollowUserController {

}

