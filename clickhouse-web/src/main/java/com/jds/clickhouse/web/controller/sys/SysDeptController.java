package com.jds.clickhouse.web.controller.sys;

import com.jds.clickhouse.web.service.sys.SysDeptService;
import com.jds.link4sales.common.annotation.Login;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jds.link4sales.common.result.ResultJson;

import lombok.RequiredArgsConstructor;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author wzy
 * @since 2020-11-03
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/web/sysDept")
@Login
public class SysDeptController {

    private final SysDeptService sysDeptService;

    @RequestMapping("/getTree")
    public ResultJson<?> getTree() {
        return ResultJson.success(sysDeptService.getTree());
    }

}
