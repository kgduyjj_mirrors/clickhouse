package com.jds.clickhouse.web.service.sys;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jds.link4sales.model.vo.SysDeptTreeVo;
import com.jds.link4sales.model.entity.sys.SysDept;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wzy
 * @since 2020-11-03
 */
public interface SysDeptService extends IService<SysDept> {

    SysDeptTreeVo getTree();

}
