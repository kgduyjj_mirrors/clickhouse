package com.jds.clickhouse.web.service.portray;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jds.link4sales.common.result.ResultJson;
import com.jds.link4sales.model.dto.ScvQyExternalUserListDto;
import com.jds.link4sales.model.entity.portray.FollowWxRelation;

/**
 * wx用户画像service
 *
 * @author cheng.zhang
 * @date 2020/11/26
 */
public interface WxUserPortraitDetailService extends IService<FollowWxRelation> {


    /**
     * 关联外部联系人-获取客户列表
     *
     * @param dto
     * @return
     */
    ResultJson<?> getCustomerList(ScvQyExternalUserListDto dto);


    /**
     * 绑定外部联系人
     */
    ResultJson<?> bindExternal(String externalId, String wxId);


    /**
     * 通过微信id查询oneId
     *
     * @param customerWxId
     * @return
     */
    String getOneId(String customerWxId);


}