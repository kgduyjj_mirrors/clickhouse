package com.jds.clickhouse.web.service.portray;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jds.link4sales.model.entity.portray.ScvSourceGpSperoCommentServiceLiveComment;

/**
 * <p>
 * 直播评论表 服务类
 * </p>
 *
 * @author JUSTIN G
 * @since 2021-01-07
 */
public interface ScvSourceGpSperoCommentServiceLiveCommentService extends IService<ScvSourceGpSperoCommentServiceLiveComment> {

}
