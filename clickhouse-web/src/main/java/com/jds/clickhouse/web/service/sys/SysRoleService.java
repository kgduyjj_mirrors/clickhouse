package com.jds.clickhouse.web.service.sys;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jds.link4sales.model.entity.sys.SysRole;

/**
 * <p>
 * sys_role  服务类
 * </p>
 *
 * @author JSCKSON G
 * @since 2020-11-03
 */
public interface SysRoleService extends IService<SysRole> {

}
