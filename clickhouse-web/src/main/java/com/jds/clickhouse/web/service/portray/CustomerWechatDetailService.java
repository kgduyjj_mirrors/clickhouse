package com.jds.clickhouse.web.service.portray;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jds.link4sales.model.entity.portray.CustomerWechatDetail;
import com.jds.link4sales.model.entity.portray.ScvQyExternalUser;

import java.util.List;

/**
 * <p>
 * 客户微信详情表 服务类
 * </p>
 *
 * @author JACKSON G
 * @since 2020-12-01
 */
public interface CustomerWechatDetailService extends IService<CustomerWechatDetail> {

    /**
     * 通过customerWxid列表获取客户详情
     *
     * @param customerWxidList customerWxidList列表
     * @return {@link List < CustomerWechatDetail >}
     */
    List<CustomerWechatDetail> listByCustomerWxid(List<String> customerWxidList);
    /**
     * 通过companyWxid列表获取客户详情
     *
     * @param companyWxidList oneIdList列表
     * @return {@link List < CustomerWechatDetail >}
     */
    List<CustomerWechatDetail> listByCompanyWxid(List<String> companyWxidList);


    String getWxId(String str,String acc);
}
