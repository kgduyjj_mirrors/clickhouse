package com.jds.clickhouse.web.controller.portray;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jds.link4sales.common.annotation.NoCheckUrlAuth;
import com.jds.link4sales.common.result.ResultJson;
import com.jds.link4sales.model.dto.ActionsDto;
import com.jds.link4sales.model.dto.WechatUserPortraitDto;
import com.jds.clickhouse.web.service.portray.WechatUserPortraitService;

import cn.hutool.core.lang.Assert;
import lombok.RequiredArgsConstructor;

/**
 * 微信网络用户画像控制器
 *
 * @author kun.gao
 * @date 2020/11/12
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/app/wechat/user/portrait")
@NoCheckUrlAuth
public class WechatAppUserPortraitController {

    private final WechatUserPortraitService userPortraitService;

    @GetMapping("/head/info")
    public ResultJson<Object> getHeadInfo(String externalId, Integer qyId, HttpServletRequest request) {
        Assert.notBlank(externalId, "联系人id不能为空");
        String followerId = (String) request.getAttribute("userId");
        Assert.notBlank(followerId, "员工企业id不能为空");
        return userPortraitService.getAppUserHeaInfo(externalId, followerId, qyId);
    }

    @PostMapping("/update")
    public ResultJson<Object> saveOrUpdate(@RequestBody WechatUserPortraitDto wechatUserPortraitDto,
            HttpServletRequest request) {
        Assert.notNull(wechatUserPortraitDto, "参数不能为空");
        Assert.notBlank(wechatUserPortraitDto.getExternalId(), "联系人id不能为空");
        Assert.notNull(wechatUserPortraitDto.getQyId(), "企业id不能为空");
        String followerId = (String) request.getAttribute("userId");
        Assert.notBlank(followerId, "员工企业id不能为空");
        wechatUserPortraitDto.setFollowId(followerId);
        return userPortraitService.saveOrUpdateApp(wechatUserPortraitDto);
    }

    @PostMapping("/getBuddy")
    public ResultJson<Object> getBuddy(@RequestBody WechatUserPortraitDto wechatUserPortraitDto,
            HttpServletRequest request) {
        Assert.notNull(wechatUserPortraitDto, "参数不能为空");
        Assert.notBlank(wechatUserPortraitDto.getExternalId(), "联系人id不能为空");
        Assert.notNull(wechatUserPortraitDto.getQyId(), "企业id不能为空");
        String followerId = (String) request.getAttribute("userId");
        Assert.notBlank(followerId, "员工企业id不能为空");
        wechatUserPortraitDto.setFollowId(followerId);
        return userPortraitService.getAppBuddy(wechatUserPortraitDto);
    }

    @PostMapping("/getGroups")
    public ResultJson<Object> getGroups(@RequestBody WechatUserPortraitDto wechatUserPortraitDto,
            HttpServletRequest request) {
        Assert.notNull(wechatUserPortraitDto, "参数不能为空");
        Assert.notBlank(wechatUserPortraitDto.getExternalId(), "联系人id不能为空");
        Assert.notNull(wechatUserPortraitDto.getQyId(), "企业id不能为空");
        String followerId = (String) request.getAttribute("userId");
        Assert.notBlank(followerId, "员工企业id不能为空");
        wechatUserPortraitDto.setFollowId(followerId);
        return userPortraitService.getAppGroupsInfo(wechatUserPortraitDto);
    }

    @PostMapping("/action/details")
    public ResultJson<Object> getActionDetailsList(@RequestBody ActionsDto actionsDto, HttpServletRequest request) {
        Assert.notNull(actionsDto, "参数不能为空");
        String followerId = (String) request.getAttribute("userId");
        Assert.notBlank(actionsDto.getExternalId(), "联系人id不能为空");
        Assert.notBlank(followerId, "员工企业id不能为空");
        actionsDto.setFollowId(followerId);
        return userPortraitService.getAppUserActions(actionsDto);
    }
}
