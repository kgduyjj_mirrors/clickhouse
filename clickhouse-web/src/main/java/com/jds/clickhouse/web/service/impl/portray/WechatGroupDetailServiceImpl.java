package com.jds.clickhouse.web.service.impl.portray;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jds.clickhouse.web.service.portray.WechatGroupDetailService;
import com.jds.link4sales.model.entity.portray.WechatGroupDetail;
import com.jds.link4sales.model.mapper.portray.WechatGroupDetailMapper;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 微信群信息详情表 服务实现类
 * </p>
 *
 * @author JACKSON G
 * @since 2020-12-01
 */
@Service
public class WechatGroupDetailServiceImpl extends ServiceImpl<WechatGroupDetailMapper, WechatGroupDetail> implements WechatGroupDetailService {

}
