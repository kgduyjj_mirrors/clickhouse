package com.jds.clickhouse.web.service.sys;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jds.link4sales.model.entity.sys.SysDivision;

/**
 * <p>
 * 事业部表 服务类
 * </p>
 *
 * @author wzy
 * @since 2020-11-03
 */
public interface SysDivisionService extends IService<SysDivision> {

}
