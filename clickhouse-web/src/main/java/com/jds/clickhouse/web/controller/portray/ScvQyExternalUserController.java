package com.jds.clickhouse.web.controller.portray;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.jds.link4sales.common.annotation.DataAuth;
import com.jds.link4sales.common.annotation.Login;
import com.jds.link4sales.common.result.ResultJson;
import com.jds.link4sales.model.dto.ScvQyExternalUserListDto;
import com.jds.clickhouse.web.service.portray.ScvQyExternalUserService;

import lombok.RequiredArgsConstructor;

/**
 * <p>
 * 企业外部联系人表 前端控制器
 * </p>
 *
 * @author wzy
 * @since 2020-11-04
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/web/scvQyExternalUser")
@Login
public class ScvQyExternalUserController {

    private final ScvQyExternalUserService scvQyExternalUserService;

    @PostMapping("/list")
    @DataAuth
    public ResultJson<?> list(@RequestBody @Valid ScvQyExternalUserListDto dto) {
        return ResultJson.success(PageInfo.of(scvQyExternalUserService.list(dto)));
    }

    @PostMapping("/mine/list")
    @DataAuth
    public ResultJson<?> myList(@RequestBody @Valid ScvQyExternalUserListDto dto) {
        return ResultJson.success(PageInfo.of(scvQyExternalUserService.list(dto)));
    }

}
