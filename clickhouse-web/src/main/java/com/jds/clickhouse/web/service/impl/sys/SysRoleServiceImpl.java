package com.jds.clickhouse.web.service.impl.sys;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jds.link4sales.model.entity.sys.SysRole;
import com.jds.link4sales.model.mapper.sys.SysRoleMapper;
import com.jds.clickhouse.web.service.sys.SysRoleService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * sys_role  服务实现类
 * </p>
 *
 * @author JSCKSON G
 * @since 2020-11-03
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements SysRoleService {

}
