package com.jds.clickhouse.web.service.portray;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jds.link4sales.model.entity.portray.WechatGroupDetail;

/**
 * <p>
 * 微信群信息详情表 服务类
 * </p>
 *
 * @author JACKSON G
 * @since 2020-12-01
 */
public interface WechatGroupDetailService extends IService<WechatGroupDetail> {

}
