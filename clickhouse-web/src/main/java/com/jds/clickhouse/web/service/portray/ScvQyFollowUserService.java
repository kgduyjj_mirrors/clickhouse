package com.jds.clickhouse.web.service.portray;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jds.link4sales.model.entity.portray.ScvQyFollowUser;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wzy
 * @since 2020-11-04
 */
public interface ScvQyFollowUserService extends IService<ScvQyFollowUser> {

}
