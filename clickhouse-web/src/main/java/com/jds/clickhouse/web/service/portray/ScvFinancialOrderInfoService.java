package com.jds.clickhouse.web.service.portray;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jds.link4sales.common.result.ResultJson;
import com.jds.link4sales.model.dto.ActionsDto;
import com.jds.link4sales.model.entity.portray.ScvFinancialOrderInfo;

/**
 * <p>
 * 财商用户订单信息 服务类
 * </p>
 *
 * @author wzy
 * @since 2020-12-16
 */
public interface ScvFinancialOrderInfoService extends IService<ScvFinancialOrderInfo> {

    ResultJson<Object> queryUserFinancialOrderInfoList(ActionsDto actionsDto);

    ResultJson<Object> queryWxUserFinancialOrderInfoList(ActionsDto actionsDto);

}
