package com.jds.clickhouse.web.service.portray;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jds.link4sales.common.result.ResultJson;
import com.jds.link4sales.model.dto.ActionsDto;
import com.jds.link4sales.model.dto.WechatUserPortraitDto;
import com.jds.link4sales.model.entity.portray.WechatUserPortrait;

/**
 * 用户画像service
 *
 * @author Jackson
 * @date 2020/10/19
 */
public interface WechatUserPortraitService extends IService<WechatUserPortrait> {

    /**
     * 获取用户头脑信息
     *
     * @param followId   员工id
     * @param qyId       企业d
     * @param externalId 外部id
     * @return {@link ResultJson<Object>}
     */
    ResultJson<Object> getAppUserHeaInfo(String externalId, String followId,Integer qyId);

    /**
     * 保存或更新
     *
     * @param wechatUserPortraitDto 微信用户画像
     * @return {@link ResultJson}
     */
    ResultJson<Object> saveOrUpdateApp(WechatUserPortraitDto wechatUserPortraitDto);

    /**
     * 获取好友列表
     *
     * @param wechatUserPortraitDto 微信用户画像
     * @return {@link ResultJson}
     */
    ResultJson<Object> getAppBuddy(WechatUserPortraitDto wechatUserPortraitDto);


    /**
     * 获群相关消息
     *
     *
     * @param wechatUserPortraitDto 微信用户画像
     * @return {@link ResultJson}
     */
    ResultJson<Object> getAppGroupsInfo(WechatUserPortraitDto wechatUserPortraitDto);


    /**
     * 获取用户动态
     *
     * @param actionsDto 微信用户画像
     * @return {@link ResultJson}
     */
    ResultJson<Object> getWebActionsList(ActionsDto actionsDto);

    /**
     * 获取用户动态（微信）
     *
     * @param actionsDto 微信用户画像
     * @return {@link ResultJson}
     */
    ResultJson<Object> getWxWebActionsList(ActionsDto actionsDto);

    /**
     * 获取用户动态
     *
     * @param actionsDto 微信用户画像
     * @return {@link ResultJson}
     */
    ResultJson<Object> getAppUserActions(ActionsDto actionsDto);

    /**
     * 获取用户头脑信息
     *
     * @param followId   员工id
     * @param qyId       企业d
     * @param exId 外部id
     * @return {@link ResultJson<Object>}
     */
    ResultJson<Object> getWebUserHeaInfo( Integer exId, String followId,Integer qyId);

    /**
     * 保存或更新
     *
     * @param wechatUserPortraitDto 微信用户画像
     * @return {@link ResultJson}
     */
    ResultJson<Object> saveOrUpdateWeb(WechatUserPortraitDto wechatUserPortraitDto);

    /**
     * 获取好友列表
     *
     * @param wechatUserPortraitDto 微信用户画像
     * @return {@link ResultJson}
     */
    ResultJson<Object> getWebBuddy(WechatUserPortraitDto wechatUserPortraitDto);


    /**
     * 获群相关消息
     *
     *
     * @param wechatUserPortraitDto 微信用户画像
     * @return {@link ResultJson}
     */
    ResultJson<Object> getWebGroupsInfo(WechatUserPortraitDto wechatUserPortraitDto);

}