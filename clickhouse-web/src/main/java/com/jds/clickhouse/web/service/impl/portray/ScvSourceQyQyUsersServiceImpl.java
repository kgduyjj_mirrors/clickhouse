package com.jds.clickhouse.web.service.impl.portray;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jds.clickhouse.web.service.portray.ScvSourceQyQyUsersService;
import com.jds.link4sales.model.entity.portray.ScvSourceQyQyUsers;
import com.jds.link4sales.model.mapper.portray.ScvSourceQyQyUsersMapper;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 企业微信用户 服务实现类
 * </p>
 *
 * @author JACKSON G
 * @since 2020-11-26
 */
@Service
public class ScvSourceQyQyUsersServiceImpl extends ServiceImpl<ScvSourceQyQyUsersMapper, ScvSourceQyQyUsers> implements ScvSourceQyQyUsersService {

}
