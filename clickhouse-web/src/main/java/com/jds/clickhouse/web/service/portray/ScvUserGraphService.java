package com.jds.clickhouse.web.service.portray;


import com.baomidou.mybatisplus.extension.service.IService;
import com.jds.link4sales.model.entity.portray.ScvUserGraph;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author JACKSON G
 * @since 2020-11-06
 */
public interface ScvUserGraphService extends IService<ScvUserGraph> {

}
