package com.jds.clickhouse.web.service.portray;


import com.baomidou.mybatisplus.extension.service.IService;
import com.jds.link4sales.common.result.ResultJson;
import com.jds.link4sales.model.dto.ActionsDto;
import com.jds.link4sales.model.entity.portray.ScvMicroUserBehavior;

import java.util.List;

/**
 * <p>
 * 报告会客户行为表 服务类
 * </p>
 *
 * @author JACKSON G
 * @since 2020-11-27
 */
public interface ScvMicroUserBehaviorService extends IService<ScvMicroUserBehavior> {

    /**
     * 查询微课动作列表
     * Query the list of micro-class actions
     *
     * @param actionsDto 行动dto
     * @return {@link List < ScvUserLiveRoomBehaviorVo >}
     */
    ResultJson<Object> queryMicroActionList(ActionsDto actionsDto);

    /**
     * 查询微课动作列表(微信)
     * Query the list of micro-class actions
     *
     * @param actionsDto 行动dto
     * @return {@link List < ScvUserLiveRoomBehaviorVo >}
     */
    ResultJson<Object> queryWxMicroActionList(ActionsDto actionsDto);

}
