package com.jds.clickhouse.web.service.impl.portray;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jds.clickhouse.web.service.portray.CustomerWechatDetailService;
import com.jds.clickhouse.web.service.portray.ScvLiveUserBehaviorService;
import com.jds.clickhouse.web.service.portray.ScvQyExternalUserService;
import com.jds.clickhouse.web.service.portray.ScvSourceQyQyUsersService;
import com.jds.link4sales.common.result.ResultJson;
import com.jds.link4sales.common.utils.AesUtils;
import com.jds.link4sales.model.dto.ActionsDto;
import com.jds.link4sales.model.entity.portray.CustomerWechatDetail;
import com.jds.link4sales.model.entity.portray.ScvLiveUserBehavior;
import com.jds.link4sales.model.entity.portray.ScvQyExternalUser;
import com.jds.link4sales.model.entity.portray.ScvSourceQyQyUsers;
import com.jds.link4sales.model.entity.sys.SysUser;
import com.jds.link4sales.model.mapper.portray.ScvLiveUserBehaviorMapper;
import com.jds.link4sales.model.vo.ScvUserLiveRoomBehaviorVo;
import com.jds.link4sales.model.vo.ScvWxUserLiveRoomBehaviorVo;
import com.jds.clickhouse.web.service.sys.SysUserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 直播客户行为表 服务实现类
 * </p>
 *
 * @author JACKSON G
 * @since 2020-11-27
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class ScvLiveUserBehaviorServiceImpl extends ServiceImpl<ScvLiveUserBehaviorMapper, ScvLiveUserBehavior> implements ScvLiveUserBehaviorService {

    private final ScvLiveUserBehaviorMapper liveUserBehaviorMapper;

    private final ScvQyExternalUserService scvQyExternalUserService;

    private final ScvSourceQyQyUsersService scvSourceQyQyUsersService;

    private final SysUserService sysUserService;

    private final CustomerWechatDetailService customerWechatDetailService;

    @Value("${aes.key}")
    private String secretKey;

    @Override
    public ResultJson<Object> queryLiveActionList(ActionsDto actionsDto) {
        PageHelper.startPage(actionsDto.getPageNum(), actionsDto.getPageSize());
        List<ScvUserLiveRoomBehaviorVo> scvUserLiveRoomBehaviorVos = liveUserBehaviorMapper
                .queryLiveActionList(actionsDto);
        if (scvUserLiveRoomBehaviorVos != null && scvUserLiveRoomBehaviorVos.size() > 0) {
            List<Integer> collect = scvUserLiveRoomBehaviorVos.stream().map(ScvUserLiveRoomBehaviorVo::getExId)
                    .collect(Collectors.toList());
            List<ScvQyExternalUser> scvQyExternalUsers = scvQyExternalUserService.listByExternalId(collect);
            if (scvQyExternalUsers != null && scvQyExternalUsers.size() > 0) {
                scvUserLiveRoomBehaviorVos.forEach(scvUserLiveRoomBehaviorVo -> {
                    scvQyExternalUsers.forEach(scvQyExternalUser -> {
                        if (scvUserLiveRoomBehaviorVo.getExId().equals(scvQyExternalUser.getId())) {
                            scvUserLiveRoomBehaviorVo.setAvatar(scvQyExternalUser.getAvatar());
                            scvUserLiveRoomBehaviorVo.setNickName(scvQyExternalUser.getName());
                        }
                    });
                });
            }
            List<String> followerIdList =  scvUserLiveRoomBehaviorVos.stream().map(ScvUserLiveRoomBehaviorVo::getFollowUserId)
                    .collect(Collectors.toList());
            List<ScvSourceQyQyUsers> list = scvSourceQyQyUsersService.list(new LambdaQueryWrapper<ScvSourceQyQyUsers>().eq(ScvSourceQyQyUsers::getQyId, actionsDto.getQyId()).in(ScvSourceQyQyUsers::getUserid, followerIdList));
            scvUserLiveRoomBehaviorVos.forEach(scvUserLiveRoomBehaviorVo -> {
                list.forEach(scvSourceQyQyUsers -> {
                    if (scvSourceQyQyUsers.getUserid().equals(scvUserLiveRoomBehaviorVo.getFollowUserId())) {
                        scvUserLiveRoomBehaviorVo.setFollowUserName(scvSourceQyQyUsers.getName());
                        if(StringUtils.isNotBlank(scvSourceQyQyUsers.getMobile())){
                            scvUserLiveRoomBehaviorVo.setMobile(AesUtils.desEncryptTerminal(scvSourceQyQyUsers.getMobile()));
                        }
                    }
                });
            });
            List<String> userCodeList = scvUserLiveRoomBehaviorVos.stream().map(ScvUserLiveRoomBehaviorVo::getUserCode).collect(Collectors.toList());
            List<SysUser> list1 = sysUserService.list(new LambdaQueryWrapper<SysUser>().in(SysUser::getUserLogin, userCodeList));
            scvUserLiveRoomBehaviorVos.forEach(scvUserLiveRoomBehaviorVo -> {
                list1.forEach(sysUser -> {
                    if(scvUserLiveRoomBehaviorVo.getUserCode().equals(sysUser.getUserLogin())){
                        scvUserLiveRoomBehaviorVo.setName(sysUser.getRealname());
                    }
                });
            });
        }
        return ResultJson.success(new PageInfo<>(scvUserLiveRoomBehaviorVos));
    }
    /**
     * 直播室行为数据 （微信）
     * @param actionsDto actionsDto
     * @return  ResultJson ResultJson
     */
    @Override
    public ResultJson<Object> queryWxLiveActionList(ActionsDto actionsDto) {
        PageHelper.startPage(actionsDto.getPageNum(), actionsDto.getPageSize());
        //获取直播室行为数据(微信)
        List<ScvWxUserLiveRoomBehaviorVo> scvUserLiveRoomBehaviorVos = liveUserBehaviorMapper
                .queryWxLiveActionList(actionsDto);

        if (!CollectionUtils.isEmpty(scvUserLiveRoomBehaviorVos)) {
            //通过customerWxid获取客户的信息赋值到列表vo
                List<String> collectCustomerWxid = scvUserLiveRoomBehaviorVos.stream().map(ScvWxUserLiveRoomBehaviorVo::getCustomerWxid)
                    .collect(Collectors.toList());
            //通过customerWxid集合获取customer_wechat_detail客户微信表的详情信息
            List<CustomerWechatDetail> customerWechatDetailUsers = customerWechatDetailService.listByCustomerWxid(collectCustomerWxid);

            //客户信息赋值
            if (!CollectionUtils.isEmpty(customerWechatDetailUsers)) {
                scvUserLiveRoomBehaviorVos.forEach(scvUserLiveRoomBehaviorVo -> {
                    customerWechatDetailUsers.forEach(customerWechatDetailUser -> {
                        if (scvUserLiveRoomBehaviorVo.getCustomerWxid().equals(customerWechatDetailUser.getCustomerWxid())) {
                            scvUserLiveRoomBehaviorVo.setAvatar(customerWechatDetailUser.getWechatHeadUrl());
                            scvUserLiveRoomBehaviorVo.setNickName(customerWechatDetailUser.getNickname());
                        }
                    });
                });
            }

            //获取微信号-company_wxid在customer_wechat_detail中关联user_id然后获取wechat_wxacc
            List<String> collectCompanyWxid = scvUserLiveRoomBehaviorVos.stream().map(ScvWxUserLiveRoomBehaviorVo::getCustomerWxid)
                    .collect(Collectors.toList());

            //通过customerWxid集合获取customer_wechat_detail客户微信表的详情信息
            List<CustomerWechatDetail> companyCustomerWechatDetailUsers = customerWechatDetailService.listByCustomerWxid(collectCompanyWxid);
            //所属微信号赋值
            scvUserLiveRoomBehaviorVos.forEach(scvFwwechatUserBehaviorVo -> {
                companyCustomerWechatDetailUsers.forEach(companyCustomerWechatDetailUser -> {
                    if (scvFwwechatUserBehaviorVo.getCompanyWxid().equals(companyCustomerWechatDetailUser.getCustomerWxid())) {
                        if (StringUtils.isNotBlank(companyCustomerWechatDetailUser.getWechatWxacc())) {
                            scvFwwechatUserBehaviorVo.setBelongWxid(AesUtils.decrypt(companyCustomerWechatDetailUser.getWechatWxacc(), secretKey));
                        }
                    }
                });
            });
            //员工信息赋值
//            List<String> userCodeList = scvUserLiveRoomBehaviorVos.stream().map(ScvWxUserBehaviorVo::getUserCode).collect(Collectors.toList());
//            List<SysUser> list1 = sysUserService.list(new LambdaQueryWrapper<SysUser>().in(SysUser::getUserLogin, userCodeList));
//            scvUserLiveRoomBehaviorVos.forEach(scvUserLiveRoomBehaviorVo -> {
//                list1.forEach(sysUser -> {
//                    if(scvUserLiveRoomBehaviorVo.getUserCode().equals(sysUser.getUserLogin())){
//                        scvUserLiveRoomBehaviorVo.setName(sysUser.getRealname());
//                    }
//                });
//            });
        }
        return ResultJson.success(new PageInfo<>(scvUserLiveRoomBehaviorVos));
    }


}
