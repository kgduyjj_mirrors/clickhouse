package com.jds.clickhouse.web.service.impl.portray;

import java.util.List;

import com.jds.clickhouse.web.service.portray.ScvFinancialOrderInfoService;
import com.jds.clickhouse.web.service.portray.ScvQyStaffCustommerRelationService;
import com.jds.link4sales.common.exception.BusinessException;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jds.link4sales.common.result.ResultJson;
import com.jds.link4sales.model.dto.ActionsDto;
import com.jds.link4sales.model.entity.portray.ScvFinancialOrderInfo;
import com.jds.link4sales.model.entity.portray.ScvQyStaffCustommerRelation;
import com.jds.link4sales.model.mapper.portray.ScvFinancialOrderInfoMapper;
import com.jds.link4sales.model.vo.ScvFinancialOrderInfoVo;

import lombok.RequiredArgsConstructor;

/**
 * <p>
 * 财商用户订单信息 服务实现类
 * </p>
 *
 * @author wzy
 * @since 2020-12-16
 */
@Service
@RequiredArgsConstructor
public class ScvFinancialOrderInfoServiceImpl extends ServiceImpl<ScvFinancialOrderInfoMapper, ScvFinancialOrderInfo>
        implements ScvFinancialOrderInfoService {

    private final ScvFinancialOrderInfoMapper scvFinancialOrderInfoMapper;

    private final ScvQyStaffCustommerRelationService scvQyStaffCustommerRelationService;

    @Override
    public ResultJson<Object> queryUserFinancialOrderInfoList(ActionsDto actionsDto) {
        ScvQyStaffCustommerRelation one = scvQyStaffCustommerRelationService.getOne(new LambdaQueryWrapper<ScvQyStaffCustommerRelation>()
                .eq(ScvQyStaffCustommerRelation::getExternalId, actionsDto.getExId()).last("limit 1"));
        if(one==null){
            throw  new BusinessException("未找到外部联系人信息");
        }
        actionsDto.setOneId(one.getOneId());
        PageHelper.startPage(actionsDto.getPageNum(), actionsDto.getPageSize());
        List<ScvFinancialOrderInfoVo> scvFinancialOrderInfoVos = scvFinancialOrderInfoMapper
                .queryUserFinancialOrderInfoList(actionsDto);
        return ResultJson.success(new PageInfo<>(scvFinancialOrderInfoVos));
    }

    @Override
    public ResultJson<Object> queryWxUserFinancialOrderInfoList(ActionsDto actionsDto) {
        PageHelper.startPage(actionsDto.getPageNum(), actionsDto.getPageSize());
        List<ScvFinancialOrderInfoVo> scvFinancialOrderInfoVos = scvFinancialOrderInfoMapper
                .queryWxUserFinancialOrderInfoList(actionsDto);
        return ResultJson.success(new PageInfo<>(scvFinancialOrderInfoVos));
    }

}
