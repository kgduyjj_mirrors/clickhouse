package com.jds.clickhouse.web.service.portray;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.IService;
import com.jds.link4sales.common.result.ResultJson;
import com.jds.link4sales.model.dto.QySecretMappingDto;
import com.jds.link4sales.model.entity.portray.QySecretMapping;

/**
 * qy秘密地图服务
 *
 * @author JACKSON G
 * @date 2020/11/03
 */
public interface QySecretMappingService extends IService<QySecretMapping> {

    /**
     * 得到qy令牌
     *
     * @param qyId qy id
     * @return {@link String}
     */
    String getQyToken(Integer qyId);

    /**
     * qy emp的用户id
     *
     * @param qyId    qy id
     * @param agentId 代理人身份证
     * @param corpId  公司标识
     * @param code    代码
     * @return {@link String}
     */
    String getQyEmpUserId(Integer qyId, String agentId, String corpId, String code);

    /**
     * 得到签名
     *
     * @param qyId    qy id
     * @param agentId 代理人身份证
     * @param corpId  公司标识
     * @param url     url
     * @return {@link JSONObject}
     */
    JSONObject getSignature(Integer qyId, String agentId, String corpId, String url);

    /**
     * 得到所有
     *
     * @return {@link ResultJson<Object>}
     */
    ResultJson<Object> getAll();

    /**
     * 保存qy秘密映射
     *
     * @param dto dto
     * @return int
     */
    int saveQySecretMapping(QySecretMappingDto dto);

}
