package com.jds.clickhouse.web.service.sys;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jds.link4sales.model.entity.sys.SysMenu;

import java.util.List;

/**
 * <p>
 * sys_menu  服务类
 * </p>
 *
 * @author zhixiang.peng
 * @since 2020-11-03
 */
public interface SysMenuService extends IService<SysMenu> {

    /**
     * 根据用户ID查询菜单树信息（菜单树）
     *
     * @param userCode 用户cdoe
     * @return 菜单列表
     */
     List<SysMenu> selectMenuTreeByUserCode(String userCode);

    /**
     * 根查询全部
     *
     * @return 菜单列表
     */
     List<SysMenu> selectAllMenuTree();

    /**
     * 根据用户ID查询菜单信息（非菜单树）
     *
     * @param userCode 用户code
     * @return 菜单列表
     */
     List<SysMenu> selectMenuByUserCode(String userCode);

    /**
     * 新增功能接口
     * @param sysMenu 功能
     */
     void addFunction(SysMenu sysMenu);

    /**
     * 更新功能接口
     * @param sysMenu 功能
     */
     void updateFunction(SysMenu sysMenu);

    /**
     * 根据当前请求的功能判断当前请求的功能数据权限
     *
     * @param url  请求的url
     * @param userCode  请求的userCode
     * @return 菜单列表
     */
     List<Integer> selectDataAuthByUrlAndUserCode(String url,String userCode);
    /**
     * 批量设置功能权限
     *
     * @param sysMenuList  请求的url
     * @return 菜单列表
     */
     void setDataAuth(List<SysMenu> sysMenuList);

}
