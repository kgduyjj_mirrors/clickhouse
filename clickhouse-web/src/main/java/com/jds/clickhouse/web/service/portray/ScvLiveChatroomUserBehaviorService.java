package com.jds.clickhouse.web.service.portray;


import com.baomidou.mybatisplus.extension.service.IService;
import com.jds.link4sales.common.result.ResultJson;
import com.jds.link4sales.model.dto.ActionsDto;
import com.jds.link4sales.model.entity.portray.ScvLiveChatroomUserBehavior;

import java.util.List;

/**
 * <p>
 * 报告会、直播、微课用户行为数据 服务类
 * </p>
 *
 * @author JACKSON G
 * @since 2020-11-06
 */
public interface ScvLiveChatroomUserBehaviorService extends IService<ScvLiveChatroomUserBehavior> {

    /**
     * 查询其他动作列表
     *
     * @param actionsDto 行动dto
     * @return {@link List < ScvUserLiveRoomBehaviorVo >}
     */
   ResultJson<Object> queryOtherActionList(ActionsDto actionsDto);

    /**
     * 查询其他动作列表
     *
     * @param actionsDto 行动dto
     * @return {@link List < ScvUserLiveRoomBehaviorVo >}
     */
    ResultJson<Object> queryUserOtherActionList(ActionsDto actionsDto);

    /**
     * 房间列表
     *
     * @param type 类型
     * @return {@link ResultJson<Object>}
     */
    ResultJson<Object> roomList(Integer type);


    /**
     * 查询其他动作列表
     *
     * @param actionsDto 行动dto
     * @return {@link List < ScvUserLiveRoomBehaviorVo >}
     */
    ResultJson<Object> queryUserActionOsList(ActionsDto actionsDto);

    ResultJson<Object> queryWxUserOtherActionList(ActionsDto actionsDto);
}
