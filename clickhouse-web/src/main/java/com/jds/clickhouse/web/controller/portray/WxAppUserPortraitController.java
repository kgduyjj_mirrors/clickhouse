package com.jds.clickhouse.web.controller.portray;

import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import com.jds.clickhouse.web.service.portray.WxUserPortraitService;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jds.link4sales.common.annotation.NoCheckUrlAuth;
import com.jds.link4sales.common.result.ResultJson;
import com.jds.link4sales.model.dto.ActionsDto;
import com.jds.link4sales.model.dto.ScvQyExternalUserListDto;
import com.jds.link4sales.model.entity.portray.QySecretMapping;
import com.jds.link4sales.model.vo.EmployeeAdditionVo;
import com.jds.clickhouse.web.service.portray.QySecretMappingService;
import com.jds.clickhouse.web.service.portray.ScvTdcsUserBehaviorService;
import com.jds.clickhouse.web.service.portray.WxUserPortraitDetailService;

import cn.hutool.core.lang.Assert;
import lombok.RequiredArgsConstructor;

/**
 * 微信网络用户画像控制器
 *
 * @author kun.gao
 * @date 2020/11/12
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/app/wx/user/portrait")
@NoCheckUrlAuth
public class WxAppUserPortraitController {

    private final WxUserPortraitDetailService wxUserPortraitDetailService;
    private final WxUserPortraitService wxUserPortraitService;
    private final ScvTdcsUserBehaviorService scvTdcsUserBehaviorService;
    private final QySecretMappingService qySecretMappingService;

    /**
     * 头部信息
     */
    @GetMapping("/head/info")
    public ResultJson<Object> getWxWebHeadInfo(String companyWxId, String customerWxId) {
        Assert.notBlank(companyWxId, "员工微信id不能为空");
        Assert.notBlank(customerWxId, "客户微信id不能为空");
        companyWxId = wxUserPortraitService.getWxId(companyWxId);
        customerWxId = wxUserPortraitService.getWxId(customerWxId);
        return wxUserPortraitService.getWxUserHeaInfo(companyWxId, customerWxId);
    }

    /**
     * 好友列表
     */
    @GetMapping("/getBuddy")
    public ResultJson<Object> getWxBuddy(String customerWxId, Integer pageNum, Integer pageSize) {
        Assert.notNull(customerWxId, "客户微信id不能为空");
        if (pageNum == null || pageNum <= 0) {
            pageNum = 1;
        }
        if (pageSize == null || pageSize <= 0) {
            pageSize = 10;
        }
        customerWxId = wxUserPortraitService.getWxId(customerWxId);
        return wxUserPortraitService.getWxBuddy(customerWxId, pageNum, pageSize);
    }

    /**
     * 群列表
     */
    @GetMapping("/getGroups")
    public ResultJson<Object> getGroups(String customerWxId, String companyWxId, Integer pageNum, Integer pageSize, Integer type) {
        Assert.notNull(customerWxId, "客户微信id不能为空");
        Assert.notNull(companyWxId, "员工微信id不能为空");
        if (pageNum == null || pageNum <= 0) {
            pageNum = 1;
        }
        if (pageSize == null || pageSize <= 0) {
            pageSize = 10;
        }
        companyWxId = wxUserPortraitService.getWxId(companyWxId);
        customerWxId = wxUserPortraitService.getWxId(customerWxId);
        return wxUserPortraitService.getWxGroupsInfo(customerWxId, companyWxId, pageNum, pageSize, type);
    }

    @PostMapping("/update")
    public ResultJson<Object> saveOrUpdate(@RequestBody EmployeeAdditionVo employeeAdditionVo) {
        Assert.notNull(employeeAdditionVo, "参数不能为空");
        Assert.notNull(employeeAdditionVo.getCompanyWxid(), "员工微信id不能为空");
        Assert.notNull(employeeAdditionVo.getCustomerWxid(), "客户微信id不能为空");
        Assert.notBlank(employeeAdditionVo.getUserId(), "员工id不能为空");
        String companyId = wxUserPortraitService.getWxId(employeeAdditionVo.getCompanyWxid());
        String cusId = wxUserPortraitService.getWxId(employeeAdditionVo.getCustomerWxid());
        employeeAdditionVo.setCompanyWxid(companyId);
        employeeAdditionVo.setCustomerWxid(cusId);
        return wxUserPortraitService.updateAddition(employeeAdditionVo);
    }

    @PostMapping("/action/details")
    public ResultJson<Object> getActionDetailsList(@RequestBody ActionsDto actionsDto, HttpServletRequest request) {
        Assert.notNull(actionsDto, "参数不能为空");
        Assert.notNull(actionsDto.getCustomerWxId(), "联系人id不能为空");
        String cusId = wxUserPortraitService.getWxId(actionsDto.getCustomerWxId());
        actionsDto.setCustomerWxId(cusId);
        return wxUserPortraitService.getAppUserActions(actionsDto);
    }

    @PostMapping("/getCustomerList")
    public ResultJson<?> getCustomerList(@RequestBody ScvQyExternalUserListDto dto) {
        Assert.notNull(dto, "参数不能为空");
        String cusId = wxUserPortraitService.getWxId(dto.getCustomerWxId());
        dto.setCustomerWxId(cusId);
        return wxUserPortraitDetailService.getCustomerList(dto);
    }

    @PostMapping("/bindExternal")
    public ResultJson<?> bindExternal(@RequestBody String req) {

        JSONObject obj = JSONObject.parseObject(req);
        String externalId = obj.getString("externalId");
        String wxId = obj.getString("customerWxId");
        Assert.notNull(externalId, "外部联系人id不能为空");
        Assert.notNull(wxId, "客户微信id不能为空");
        String cusId = wxUserPortraitService.getWxId(wxId);
        return wxUserPortraitDetailService.bindExternal(externalId, cusId);
    }
    
    @GetMapping("/qyList")
    public ResultJson<Object> qyList(String userCode) {
        Assert.notBlank(userCode, "员工工号不能为空");
        List<Integer> qyIdList= scvTdcsUserBehaviorService.queryQyIdByUserId(userCode);
        List<Integer> qyIdListDistinct=  qyIdList.stream().distinct().collect(Collectors.toList());
        if(CollectionUtils.isEmpty(qyIdListDistinct)){
            return  ResultJson.success();
        }
        return ResultJson.success(qySecretMappingService.list(new QueryWrapper<QySecretMapping>().in("qy_id",qyIdListDistinct)));
    }
}
