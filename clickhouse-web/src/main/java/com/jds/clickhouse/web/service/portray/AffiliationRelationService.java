package com.jds.clickhouse.web.service.portray;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jds.link4sales.common.result.ResultJson;
import com.jds.link4sales.model.dto.WxUserPropertyQueryDto;
import com.jds.link4sales.model.entity.portray.AffiliationRelation;
import com.jds.link4sales.model.vo.ScvWxUserPropertyVo;
import com.jds.link4sales.model.vo.WxVo;

import java.util.List;

/**
 * <p>
 * 客户群体与对应员工的所属关系 服务类
 * </p>
 *
 * @author zhixiang.peng
 * @since 2020-12-01
 */
public interface AffiliationRelationService extends IService<AffiliationRelation> {

    /**
     * 微信客户用户属性一览（全部）
     * @param actionsDto actionsDto
     * @return
     */
    ResultJson<Object> getAllWxUserPropertyList(WxUserPropertyQueryDto actionsDto);

    /**
     * 微信客户用户属性一览（我的）
     * @param actionsDto actionsDto
     * @return
     */
    ResultJson<Object> getMyWxUserPropertyList(WxUserPropertyQueryDto actionsDto);

    /**
     * 通用电气wx好友信息
     *
     * @param customerWxId 客户wx id
     * @param pageNum      页面num
     * @param pageSize     页面大小
     * @return {@link List<WxVo>}
     */
    ResultJson<Object> getWxBuddyInfo(String customerWxId, Integer pageNum , Integer pageSize);

}
