package com.jds.clickhouse.web.controller.portray;

import com.jds.clickhouse.web.service.portray.ScvSourceGpSperoLiveServicePaidLiveService;
import com.jds.link4sales.common.annotation.Login;
import com.jds.link4sales.common.exception.BusinessException;
import com.jds.link4sales.common.result.ResultJson;
import com.jds.link4sales.model.dto.ActionsDto;
import com.jds.clickhouse.web.service.portray.ScvSensorTenAggregationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 传感器十聚合控制器
 *
 * @author kun.gao
 * @date 2021/01/08
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("sensor/ten/aggregation")
@Login
public class SensorTenAggregationController {

    private final ScvSensorTenAggregationService sensorTenAggregationService;
    private final ScvSourceGpSperoLiveServicePaidLiveService scvSourceGpSperoLiveServicePaidLiveService;

    @GetMapping("/list")
    public ResultJson<Object> getList(ActionsDto actionsDto) {
        if(actionsDto == null){
            throw new BusinessException("参数不能为空");
        }
        if(StringUtils.isBlank(actionsDto.getOneId())){
            throw new BusinessException("oneId不能为空");
        }
        if(StringUtils.isBlank(actionsDto.getTitle())){
            throw new BusinessException("标题不能为空");
        }
        return sensorTenAggregationService.getList(actionsDto);
    }


    @GetMapping("/comment/list")
    public ResultJson<Object> getCommentDetails(ActionsDto actionsDto) {
        if(actionsDto == null){
            throw new BusinessException("参数不能为空");
        }
        if (StringUtils.isBlank(actionsDto.getOneId())) {
            throw new BusinessException("oneId不能为空");
        }
        if (actionsDto.getRoomId() == null) {
            throw new BusinessException("roomId不能为空");
        }
        return sensorTenAggregationService.getCommentList(actionsDto);
    }
    @GetMapping("/getActionDataList")
    public ResultJson<Object> getActionDataList(ActionsDto actionsDto) {
        if(actionsDto == null){
            throw new BusinessException("参数不能为空");
        }
        if (StringUtils.isBlank(actionsDto.getOneId())) {
            throw new BusinessException("oneId不能为空");
        }
        if(StringUtils.isBlank(actionsDto.getTitleId())){
            throw new BusinessException("标题id不能为空");
        }
        return sensorTenAggregationService.getActionDataList(actionsDto);

    }

    @GetMapping("/getThemeList")
    public ResultJson<Object> getThemeList(ActionsDto actionsDto) {
        return  ResultJson.success(scvSourceGpSperoLiveServicePaidLiveService.list());
    }


}
