package com.jds.clickhouse.web.service.impl.sys;

import java.util.List;

import com.jds.clickhouse.web.service.sys.SysUserService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jds.link4sales.model.dto.SysUserListDto;
import com.jds.link4sales.model.entity.sys.SysUser;
import com.jds.link4sales.model.mapper.sys.SysUserMapper;
import com.jds.link4sales.model.vo.SysUserListVo;

import lombok.RequiredArgsConstructor;

/**
 * 系统用户服务impl
 *
 * @author kun.gao
 * @date 2020/11/02
 */
@Service
@RequiredArgsConstructor
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {

    private final SysUserMapper sysUserMapper;

    @Override
    public List<SysUserListVo> list(SysUserListDto dto) {
        Integer type = dto.getType();
        if (type == 1) {
            dto.setDeptId(dto.getId());
        } else if (type == 2) {
            dto.setUserLogin(dto.getId().toString());
        }
        return sysUserMapper.selectList1(dto);
    }

    @Override
    public List<SysUser> selectSameDeptUserList(String userId) {
        return sysUserMapper.selectSameDeptUserList(userId);
    }

    @Override
    public SysUserListVo selectUserInfoByUserLogin(String userLogin) {
        return   sysUserMapper.selectUserInfoByUserLogin(userLogin);
    }

}
