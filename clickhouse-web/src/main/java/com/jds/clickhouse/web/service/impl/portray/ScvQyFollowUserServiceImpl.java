package com.jds.clickhouse.web.service.impl.portray;

import com.jds.clickhouse.web.service.portray.ScvQyFollowUserService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jds.link4sales.model.entity.portray.ScvQyFollowUser;
import com.jds.link4sales.model.mapper.portray.ScvQyFollowUserMapper;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wzy
 * @since 2020-11-04
 */
@Service
public class ScvQyFollowUserServiceImpl extends ServiceImpl<ScvQyFollowUserMapper, ScvQyFollowUser> implements ScvQyFollowUserService {

}
