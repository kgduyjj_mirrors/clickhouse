package com.jds.clickhouse.web.service.impl.portray;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jds.link4sales.common.result.ResultJson;
import com.jds.link4sales.common.utils.AesUtils;
import com.jds.link4sales.model.dto.ActionsDto;
import com.jds.link4sales.model.entity.portray.ScvLiveChatroomUserBehavior;
import com.jds.link4sales.model.entity.portray.ScvQyExternalUser;
import com.jds.link4sales.model.entity.portray.ScvQyStaffCustommerRelation;
import com.jds.link4sales.model.entity.portray.ScvSourceLiveChatroom;
import com.jds.link4sales.model.entity.portray.ScvSourceQyQyUsers;
import com.jds.link4sales.model.entity.sys.SysUser;
import com.jds.link4sales.model.mapper.portray.ScvLiveChatroomUserBehaviorMapper;
import com.jds.link4sales.model.mapper.portray.ScvSourceLiveChatroomMapper;
import com.jds.link4sales.model.vo.ScvUserLiveRoomBehaviorVo;
import com.jds.clickhouse.web.service.portray.AffiliationRelationService;
import com.jds.clickhouse.web.service.portray.ScvLiveChatroomUserBehaviorService;
import com.jds.clickhouse.web.service.portray.ScvQyExternalUserService;
import com.jds.clickhouse.web.service.portray.ScvQyStaffCustommerRelationService;
import com.jds.clickhouse.web.service.portray.ScvSourceQyQyUsersService;
import com.jds.clickhouse.web.service.sys.SysUserService;

import lombok.RequiredArgsConstructor;

/**
 * <p>
 * 报告会、直播、微课用户行为数据 服务实现类
 * </p>
 *
 * @author JACKSON G
 * @since 2020-11-06
 */
@Service
@RequiredArgsConstructor
public class ScvLiveChatroomUserBehaviorServiceImpl
        extends ServiceImpl<ScvLiveChatroomUserBehaviorMapper, ScvLiveChatroomUserBehavior>
        implements ScvLiveChatroomUserBehaviorService {

    private final ScvLiveChatroomUserBehaviorMapper scvLiveChatroomUserBehaviorMapper;

    private final ScvQyExternalUserService scvQyExternalUserService;

    private final ScvSourceQyQyUsersService scvSourceQyQyUsersService;

    private final SysUserService sysUserService;

    private final ScvSourceLiveChatroomMapper scvSourceLiveChatroomMapper;

    private final ScvQyStaffCustommerRelationService scvQyStaffCustommerRelationService;

    private final AffiliationRelationService affiliationRelationService;

    @Override
    public ResultJson<Object> queryOtherActionList(ActionsDto actionsDto) {
        PageHelper.startPage(actionsDto.getPageNum(), actionsDto.getPageSize());
        List<ScvUserLiveRoomBehaviorVo> scvUserLiveRoomBehaviorVos = scvLiveChatroomUserBehaviorMapper
                .queryOtherActionList(actionsDto);
        if (scvUserLiveRoomBehaviorVos != null && scvUserLiveRoomBehaviorVos.size() > 0) {
            List<Integer> collect = scvUserLiveRoomBehaviorVos.stream().map(ScvUserLiveRoomBehaviorVo::getExId)
                    .collect(Collectors.toList());
            List<ScvQyExternalUser> scvQyExternalUsers = scvQyExternalUserService.listByExternalId(collect);
            if (scvQyExternalUsers != null && scvQyExternalUsers.size() > 0) {
                scvUserLiveRoomBehaviorVos.forEach(scvUserLiveRoomBehaviorVo -> {
                    scvQyExternalUsers.forEach(scvQyExternalUser -> {
                        if (scvUserLiveRoomBehaviorVo.getExId().equals(scvQyExternalUser.getId())) {
                            scvUserLiveRoomBehaviorVo.setAvatar(scvQyExternalUser.getAvatar());
                            scvUserLiveRoomBehaviorVo.setNickName(scvQyExternalUser.getName());
                        }
                    });
                });
            }
            List<String> followerIdList = scvUserLiveRoomBehaviorVos.stream()
                    .map(ScvUserLiveRoomBehaviorVo::getFollowUserId).collect(Collectors.toList());
            List<ScvSourceQyQyUsers> list = scvSourceQyQyUsersService.list(
                    new LambdaQueryWrapper<ScvSourceQyQyUsers>().eq(ScvSourceQyQyUsers::getQyId, actionsDto.getQyId())
                            .in(ScvSourceQyQyUsers::getUserid, followerIdList));
            scvUserLiveRoomBehaviorVos.forEach(scvUserLiveRoomBehaviorVo -> {
                list.forEach(scvSourceQyQyUsers -> {
                    if (scvSourceQyQyUsers.getUserid().equals(scvUserLiveRoomBehaviorVo.getFollowUserId())) {
                        scvUserLiveRoomBehaviorVo.setFollowUserName(scvSourceQyQyUsers.getName());
                        if (StringUtils.isNotBlank(scvSourceQyQyUsers.getMobile())) {
                            scvUserLiveRoomBehaviorVo
                                    .setMobile(AesUtils.desEncryptTerminal(scvSourceQyQyUsers.getMobile()));
                        }
                    }
                });
            });
            List<String> userCodeList = scvUserLiveRoomBehaviorVos.stream().map(ScvUserLiveRoomBehaviorVo::getUserCode)
                    .collect(Collectors.toList());
            List<SysUser> list1 = sysUserService
                    .list(new LambdaQueryWrapper<SysUser>().in(SysUser::getUserLogin, userCodeList));
            scvUserLiveRoomBehaviorVos.forEach(scvUserLiveRoomBehaviorVo -> {
                list1.forEach(sysUser -> {
                    if (scvUserLiveRoomBehaviorVo.getUserCode().equals(sysUser.getUserLogin())) {
                        scvUserLiveRoomBehaviorVo.setName(sysUser.getRealname());
                    }
                });
            });
        }
        return ResultJson.success(new PageInfo<>(scvUserLiveRoomBehaviorVos));
    }

    @Override
    public ResultJson<Object> queryUserOtherActionList(ActionsDto actionsDto) {
        ScvQyStaffCustommerRelation one = scvQyStaffCustommerRelationService
                .getOne(new LambdaQueryWrapper<ScvQyStaffCustommerRelation>()
                        .eq(ScvQyStaffCustommerRelation::getExternalId, actionsDto.getExId()).last("limit 1"));
        List<ScvUserLiveRoomBehaviorVo> scvUserLiveRoomBehaviorVos = new ArrayList<>();
        if (one != null) {
            actionsDto.setOneId(one.getOneId());
            scvUserLiveRoomBehaviorVos = scvLiveChatroomUserBehaviorMapper.queryUserOtherActionList1(actionsDto);
            if (scvUserLiveRoomBehaviorVos != null && scvUserLiveRoomBehaviorVos.size() > 0) {
                scvUserLiveRoomBehaviorVos.forEach(scvUserLiveRoomBehaviorVo -> {
                    scvUserLiveRoomBehaviorVo.setExId(one.getExternalId());
                    scvUserLiveRoomBehaviorVo.setDeleteFlag(one.getDeleteFlag());
                    scvUserLiveRoomBehaviorVo.setOneId(one.getOneId());
                });
                List<Integer> collect = scvUserLiveRoomBehaviorVos.stream().map(ScvUserLiveRoomBehaviorVo::getRoomId)
                        .collect(Collectors.toList());
                List<ScvSourceLiveChatroom> scvSourceLiveChatrooms = scvSourceLiveChatroomMapper.selectList(
                        new LambdaQueryWrapper<ScvSourceLiveChatroom>().in(ScvSourceLiveChatroom::getId, collect));
                if (scvSourceLiveChatrooms != null && scvSourceLiveChatrooms.size() > 0) {
                    scvUserLiveRoomBehaviorVos.forEach(scvUserLiveRoomBehaviorVo -> {
                        scvSourceLiveChatrooms.forEach(scvSourceLiveChatroom -> {
                            if (scvSourceLiveChatroom.getId().equals(scvUserLiveRoomBehaviorVo.getRoomId())) {
                                scvUserLiveRoomBehaviorVo.setRoomName(scvSourceLiveChatroom.getName());
                            }
                        });
                    });
                }
            }
        }
        return ResultJson.success(new PageInfo<>(scvUserLiveRoomBehaviorVos));
    }

    @Override
    public ResultJson<Object> queryWxUserOtherActionList(ActionsDto actionsDto) {
        List<ScvUserLiveRoomBehaviorVo> scvUserLiveRoomBehaviorVos = new ArrayList<>();
        scvUserLiveRoomBehaviorVos = scvLiveChatroomUserBehaviorMapper.queryWxUserOtherActionList1(actionsDto);
        if (scvUserLiveRoomBehaviorVos != null && scvUserLiveRoomBehaviorVos.size() > 0) {
            List<Integer> collect = scvUserLiveRoomBehaviorVos.stream().map(ScvUserLiveRoomBehaviorVo::getRoomId)
                    .collect(Collectors.toList());
            List<ScvSourceLiveChatroom> scvSourceLiveChatrooms = scvSourceLiveChatroomMapper.selectList(
                    new LambdaQueryWrapper<ScvSourceLiveChatroom>().in(ScvSourceLiveChatroom::getId, collect));
            if (scvSourceLiveChatrooms != null && scvSourceLiveChatrooms.size() > 0) {
                scvUserLiveRoomBehaviorVos.forEach(scvUserLiveRoomBehaviorVo -> {
                    scvSourceLiveChatrooms.forEach(scvSourceLiveChatroom -> {
                        if (scvSourceLiveChatroom.getId().equals(scvUserLiveRoomBehaviorVo.getRoomId())) {
                            scvUserLiveRoomBehaviorVo.setRoomName(scvSourceLiveChatroom.getName());
                        }
                    });
                });
            }
        }
        return ResultJson.success(new PageInfo<>(scvUserLiveRoomBehaviorVos));
    }

    @Override
    public ResultJson<Object> roomList(Integer type) {
        return ResultJson.success(scvLiveChatroomUserBehaviorMapper.roomList(type));
    }

    @Override
    public ResultJson<Object> queryUserActionOsList(ActionsDto actionsDto) {
        return null;
    }

}
