package com.jds.clickhouse.web.controller.sys;


import cn.hutool.core.lang.Assert;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jds.link4sales.common.annotation.Login;
import com.jds.link4sales.common.result.ResultJson;
import com.jds.link4sales.model.dto.UserRoleDto;
import com.jds.link4sales.model.entity.sys.SysUserRole;
import com.jds.clickhouse.web.service.sys.SysUserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *   用户角色关系-接口
 * </p>
 *
 * @author zhixiang.peng
 * @since 2020-11-03
 */
@RestController
@RequestMapping("/web/sysUserRole")
@Login
public class SysUserRoleController {

    @Autowired
    private SysUserRoleService sysUserRoleService;

    /**
     *  保存用户角色关系
     * @param  userRoleDtoList
     * @return
     */
    @PostMapping("/saveOrUpdateUserRole")
    public ResultJson saveUserRole(@Validated @RequestBody List<UserRoleDto> userRoleDtoList) {
        sysUserRoleService.saveUserRole(userRoleDtoList);
        return ResultJson.success();
    }

    /**
     *  根据用户Code查询用户的角色
     * @param userCode  用户id(工号)
     * @return
     */
    @GetMapping("/listByUserCode")
    public ResultJson listByUserId(String userCode) {
        Assert.notNull(userCode,"用户code不能为空");
        return ResultJson.success(sysUserRoleService.list(new QueryWrapper<SysUserRole>().eq("user_code", userCode)));
    }
}

